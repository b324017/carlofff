function [qsat] = compute_rsat(T,p,iceflag,T1,T2)

    %THIS FUNCTION COMPUTES THE SATURATION MIXING RATIO, USING THE INTEGRATED
    %CLAUSIUS CLAPEYRON EQUATION (eq. 7-12 in Peters et al. 2022).
    %https://doi-org.ezaccess.libraries.psu.edu/10.1175/JAS-D-21-0118.1 

    %input arguments
    %T temperature (in K)
    %p pressure (in Pa)
    %iceflag (give mixing ratio with respect to liquid (0), combo liquid and
    %ice (2), or ice (3)
    %T1 warmest mixed-phase temperature
    %T2 coldest mixed-phase temperature
    
    %NOTE: most of my scripts and functions that use this function need
    %saturation mass fraction qs, not saturation mixing ratio rs.  To get
    %qs from rs, use the formula qs = (1 - qt)*rs, where qt is the total
    %water mass fraction

    %CONSTANTS
    Rd=287.04; %dry gas constant
    Rv=461.5; %water vapor gas constant
    epsilon=Rd./Rv;
    cp=1005; %specific heat of dry air at constant pressure
    g=9.81; %gravitational acceleration
    xlv=2501000; %reference latent heat of vaporization at the triple point temperature
    xls=2834000; %reference latent heat of sublimation at the triple point temperature
    cpv=1870; %specific heat of water vapor at constant pressure
    cpl=4190; %specific heat of liquid water
    cpi=2106; %specific heat of ice
    ttrip=273.15; %triple point temperature
    eref=611.2; %reference pressure at the triple point temperature

    %descriminator function between liquid and ice (i.e., omega defined in the
    %beginning of section 2e in Peters et al. 2022)
    omeg = ((T - T1)./(T2-T1)).*heaviside((T - T1)./(T2-T1)).*heaviside((1 - (T - T1)./(T2-T1))) + heaviside(-(1 - (T - T1)./(T2-T1)));

    switch iceflag
        case 0 %only give mixing ratio with respect to liquid
             term1=(cpv-cpl)/Rv;
             term2=(xlv-ttrip*(cpv-cpl))/Rv;
             esl=exp((T-ttrip).*term2./(T.*ttrip)).*eref.*(T./ttrip).^(term1);
             esl = min( esl , p*0.5 );
             qsat=epsilon.*esl./(p-esl);         
        case 1 %give linear combination of mixing ratio with respect to liquid and ice (eq. 20 in Peters et al. 2022)
             term1=(cpv-cpl)/Rv;
             term2=(xlv-ttrip*(cpv-cpl))/Rv;
             esl_l=exp((T-ttrip).*term2./(T.*ttrip)).*eref.*(T./ttrip).^(term1);

             qsat_l=epsilon.*esl_l./(p-esl_l);

            term1=(cpv-cpi)/Rv;
             term2=( xls-ttrip*(cpv-cpi))/Rv;
             esl_i=exp((T-ttrip).*term2./(T.*ttrip)).*eref.*(T./ttrip).^(term1);

             qsat_i=epsilon.*esl_i./(p-esl_i);

             qsat=(1-omeg).*qsat_l + (omeg).*qsat_i;
        case 2 %only give mixing ratio with respect to ice
            term1=(cpv-cpi)/Rv;
             term2=( xls-ttrip*(cpv-cpi))/Rv;
             esl=exp((T-ttrip).*term2./(T.*ttrip)).*eref.*(T./ttrip).^(term1);
             esl = min( esl , p*0.5 );
             qsat=epsilon.*esl./(p-esl);
    end


end