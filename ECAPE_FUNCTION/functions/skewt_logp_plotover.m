function [skewfig_handle]=skewt_logp_plotover(skewfig_handle,fname,Tc,Ts,Tdc,Tds,Wc,pcolor)

wrfdata=load(fname);

%range of pressures on the y axis
prange=[1050:-25:100]*100;

%range of temperatures on x axis
trange=[-40:1:40];

%now normalize so they range from 0 to 1
y_range=-(log10(prange)-max(log10(prange)))/max(-(log10(prange)-max(log10(prange))));

x_range=(trange-min(trange))/max((trange-min(trange)));

%convert potential temperature to actual temperature, establish T and P
gamma=287/1004;
T=wrfdata(:,1).*((wrfdata(:,2)./(1000*100)).^(gamma));
P=(wrfdata(:,2));

%normalize T and P so they plot onto the normalized axes
T_norm=(T-273.15-min(trange))/max((trange-min(trange)));
P_norm=-(log10(P)-max(log10(prange)))/max(-(log10(prange)-max(log10(prange))));

%transform T so that isotherms slant to the upper right with a slope of 1
T_plot=P_norm+T_norm;

%calculate relative humidity from mixing ratio
%water vapor mixing ratio to relative humidity
q=wrfdata(:,4)./(1-wrfdata(:,4));
clear RH
for iz=1:size(wrfdata,1)
    RH(iz)=100*q(iz)*P(iz)/(claus_clap(T(iz))*(.622 + q(iz)));
end
RH=RH';

%calculate dew point
a=6.112;
b=17.67;
c=243.5;
gam=log(RH./100)+b.*(T-273.15)./(c + (T-273.15));
Td=c.*gam./(b-gam);
%transform Td for plot
Td_trans=(Td-min(trange))/max((trange-min(trange)))+P_norm;

%==========================================================================
%==========================================================================
%==========================================================================
%=======CODE BELOW HERE FOR PLOTTING SKEW-T DIAGRAM========================
%==========================================================================
%==========================================================================
%==========================================================================


tmin_m=max(trange)-min(trange)


%plot wind vectors on the left side
P_norm(1)
if (P_norm(1) < .01)
    ind_range=[P_norm(1) .01 .02 .03:.04:max(P_norm)];
elseif (P_norm(1) >= .01)&(P_norm(1) < .02)
    ind_range=[P_norm(1) .02 .03:.04:max(P_norm)];
else
    ind_range=[P_norm(1) .03:.04:max(P_norm)];
end
horiz_point=-35;
horiz_loc=(horiz_point-min(trange))/max((trange-min(trange)));

%interpolate onto even vertical grid
u_oninds=interp1(P_norm,wrfdata(:,5),ind_range,'linear','extrap');
v_oninds=interp1(P_norm,wrfdata(:,6),ind_range,'linear','extrap');
u_norm=u_oninds./sqrt(u_oninds.^2+v_oninds.^2);
v_norm=v_oninds./sqrt(u_oninds.^2+v_oninds.^2);
%feather length
flen=.04;
u_norm=u_norm*flen;
v_norm=v_norm*flen;
%wind barb lengths
blen=.025;
%distance along feather between barbs
barb_space=.007;
%plot wind barbs
kconv=1.94384;
for iz=1:length(ind_range)
    %get wind speed
    wsp_kt=kconv*sqrt(u_oninds(iz)^2 + v_oninds(iz)^2);
    
    if (wsp_kt>=2.5)
        plot([horiz_loc horiz_loc-u_norm(iz)],[ind_range(iz) ind_range(iz)-v_norm(iz)],Wc)
    end
   
    %find number of 5 kt barbs
    nfive_b=round((wsp_kt-floor(wsp_kt/10)*10)/5);
    
    %find number of 10 kt barbs
    nten_b=floor(wsp_kt/10);
    if (nfive_b>1)
        nten_b=nten_b+1;
        nfive_b=0;
    end

    %find number of 50 kt barbs
    nfifty_b=floor(wsp_kt/50);
    nten_b=nten_b-nfifty_b*5;
    if (nten_b>4)
        nfifty_b=nfifty_b+1;
        nten_b=0;
    end
    
    x_on=horiz_loc-u_norm(iz);
    y_on=ind_range(iz)-v_norm(iz);
    
    ib_eff=1;
    ib_on=1;
    barb_mat=[nfifty_b nten_b nfive_b];
    for ib=1:(nfifty_b+nten_b+nfive_b)
        while (ib_eff>barb_mat(ib_on))
            ib_eff=1;
            ib_on=ib_on+1;
        end
        
        %move back a tad if first 50 kt barb
        if (ib_on==1&ib==1)
            x_on=x_on+barb_space*u_norm(iz)/(2*flen);
            y_on=y_on+barb_space*u_norm(iz)/(2*flen);
        end
        
        if (ib_on==1)
            plot([x_on-.5*barb_space*u_norm(iz)/flen x_on-blen*v_norm(iz)/(flen)],[y_on-.5*barb_space*v_norm(iz)/flen y_on+blen*u_norm(iz)/(flen)],'b:')
            plot([x_on+.5*barb_space*u_norm(iz)/flen x_on-blen*v_norm(iz)/(flen)],[y_on+.5*barb_space*v_norm(iz)/flen y_on+blen*u_norm(iz)/(flen)],'b:')
            x_on=x_on+barb_space*u_norm(iz)/flen;
            y_on=y_on+barb_space*v_norm(iz)/flen;
        elseif (ib_on==2)
            plot([x_on x_on-blen*v_norm(iz)/(flen)],[y_on y_on+blen*u_norm(iz)/(flen)],Wc)
            x_on=x_on+barb_space*u_norm(iz)/flen;
            y_on=y_on+barb_space*v_norm(iz)/flen;
        elseif (ib_on==3)
            plot([x_on x_on-blen*v_norm(iz)/(2*flen)],[y_on y_on+blen*u_norm(iz)/(2*flen)],Wc)
            x_on=x_on+barb_space*u_norm(iz)/flen;
            y_on=y_on+barb_space*v_norm(iz)/flen;
        end
        ib_eff=ib_eff+1;    
    end
end

mesh_=[T_plot Td_trans];
Td_trans_=min(mesh_,[],2);

plot(Td_trans_,P_norm,'-','color',Tdc,'linewidth',Tds)
plot(T_plot,P_norm,Tc,'linewidth',Ts,'color',pcolor)

crad=sqrt(100000/pi);

convfact=1.94384449;

low_range=true;

if ~low_range

    ukt=.05*u_oninds*convfact/25 + .85;
    vkt=.05*v_oninds*convfact/25 + .85;

    plot(ukt,vkt,Wc)

else

    ukt=.05*u_oninds*convfact/(25/2) + .85;
    vkt=.05*v_oninds*convfact/(25/2) + .85;

    plot(ukt,vkt,Wc,'linewidth',1.5)
    
end


end