function [NCAPE,MSE0_star,MSE0bar]=compute_NCAPE(T0,p0,q0,z0,T1,T2,LFC,EL)

    Rd=287.04; %DRY GAS CONSTANT
    Rv=461.5; %GAS CONSTANT FOR WATEEER VAPRR
    epsilon=Rd./Rv; %RATO OF THE TWO
    cp=1005; %HEAT CAPACITY OF DRY AIR AT CONSTANT PRESSUREE
    gamma=Rd/cp; %POTENTIAL TEMPERATURE EXPONENT
    g=9.81; %GRAVITATIONAL CONSTANT
    Gamma_d=g/cp; %DRY ADIABATIC LAPSE RATE
    xlv=2501000; %LATENT HEAT OF VAPORIZATION AT TRIPLE POINT TEMPERATURE
    xls=2834000; %LATENT HEAT OF SUBLIMATION AT TRIPLE POINT TEMPERATURE
    cpv=1870; %HEAT CAPACITY OF WATER VAPOR AT CONSTANT PRESSURE
    cpl=4190; %HEAT CAPACITY OF LIQUID WATER
    cpi=2106; %HEAT CAPACITY OF ICE
    pref=611.65; %REFERENCE VAPOR PRESSURE OF WATER VAPOR AT TRIPLE POINT TEMPERATURE
    ttrip=273.15; %TRIPLE POINT TEMPERATURE
    
    %COMPUTE THE MOIST STATIC ENERGY
    MSE0 = cp.*T0 + xlv.*q0 + g.*z0;
    
    %COMPUTE THE SATURATED MOIST STATIC ENERGY
    rsat = compute_rsat(T0,p0,0,T1,T2);
    qsat = (1 - rsat).*rsat;
    MSE0_star = cp.*T0 + xlv.*qsat + g.*z0;
    
    %COMPUTE MSE0_BAR
    MSE0bar=zeros(size(MSE0));
    for iz=1:1:length(MSE0bar)
        MSE0bar(iz)=mean(MSE0(1:iz));
    end
    
    int_arg = - ( g./(cp.*T0) ).*( MSE0bar - MSE0_star);
    [mn,ind_LFC] = (min(abs(z0-LFC)));
    [mn,ind_EL] = (min(abs(z0-EL)));
    ind_LFC=max(ind_LFC);
    ind_EL=max(ind_EL);
    
    NCAPE = sum( (0.5.*int_arg(ind_LFC:1:ind_EL-1) + 0.5.*int_arg(ind_LFC+1:1:ind_EL) ).*(...
        z0(ind_LFC+1:1:ind_EL) - z0(ind_LFC:1:ind_EL-1) ) );
    
end