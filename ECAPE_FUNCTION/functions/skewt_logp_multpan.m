%==========================================================================
%==========================================================================
%==========================================================================
%==========================================================================


function [skewfig_handle] = skewt_logp_multpan(null)

lw=.125;

wrfdata=load('sounding_base.txt');

%range of pressures on the y axis
prange=[1050:-25:100]*100;

%range of temperatures on x axis
trange=[-40:1:40];

%now normalize so they range from 0 to 1
y_range=-(log10(prange)-max(log10(prange)))/max(-(log10(prange)-max(log10(prange))));

x_range=(trange-min(trange))/max((trange-min(trange)));

%convert potential temperature to actual temperature, establish T and P
gamma=287/1004;
T=wrfdata(:,1).*((wrfdata(:,2)./(1000*100)).^(gamma));
P=(wrfdata(:,2));

%normalize T and P so they plot onto the normalized axes
T_norm=(T-273.15-min(trange))/max((trange-min(trange)));
Tv_norm=(T.*(1 + .61.*(wrfdata(:,4)))-273.15-min(trange))/max((trange-min(trange)));
P_norm=-(log10(P)-max(log10(prange)))/max(-(log10(prange)-max(log10(prange))));

%transform T so that isotherms slant to the upper right with a slope of 1
T_plot=P_norm+T_norm;
Tv_plot=P_norm+Tv_norm;

%calculate relative humidity from mixing ratio
%water vapor mixing ratio to relative humidity
q=wrfdata(:,4)./(1-wrfdata(:,4));
clear RH
for iz=1:size(wrfdata,1)
    RH(iz)=100*q(iz)*P(iz)/(claus_clap(T(iz))*(.622 + q(iz)));
end
RH=RH';

%calculate dew point
a=6.112;
b=17.67;
c=243.5;
gam=log(RH./100)+b.*(T-273.15)./(c + (T-273.15));
Td=c.*gam./(b-gam);
%transform Td for plot
Td_trans=(Td-min(trange))/max((trange-min(trange)))+P_norm;

%==========================================================================
%==========================================================================
%==========================================================================
%=======CODE BELOW HERE FOR PLOTTING SKEW-T DIAGRAM========================
%==========================================================================
%==========================================================================
%==========================================================================


scrsz = get(0,'ScreenSize');
%figure('Position',[1 scrsz(4)/1.5 scrsz(3)/1.5 scrsz(4)/1.5])
%figure('Position',[1 850 850 850])
%skewfig_handle=figure('Position',[1 scrsz(4)/1.2 scrsz(4)/1.2 scrsz(4)/1.2])
skewfig_handle=1;

%grid on
%hold on
tmin_m=max(trange)-min(trange);

%plot isotherms
for ip=min(trange)-tmin_m:10:max(trange)
    ton=(ip-min(trange))/max((trange-min(trange)));
    tpl=ton+1;
    clr=parula(10);
    clr=clr(2,:);
    %hp1=plot([ton tpl],[0 1],'linewidth',lw,'color',clr);
    if ip==(min(trange)-tmin_m)
        hold off
        %set(gca,'fontsize',14)
    end
    hp1=line([ton tpl],[0 1],'linewidth',lw,'color',clr);
    if ip==(min(trange)-tmin_m)
        hold on
        set(gca,'fontsize',22)
    end
%    hp1.Color(4) = 0.25;
    %hp1=patchline([ton tpl],[0 1],'edgecolor','b','linewidth',lw,'edgealpha',.5);
end

%plot dry adiabats
for ia=min(trange):20:max(trange)+tmin_m+max(trange)
    theta_ad=ones(size(prange))*ia+273.15;
    t_ad=theta_ad.*((prange./(1000*100)).^(gamma))-273.15;
    t_ad_plot=(t_ad-min(trange))/max((trange-min(trange)))+y_range;
    clr=[0 0 0] ;
    hp1=plot(t_ad_plot,y_range,':','linewidth',lw,'color',clr);
    %hp1=patchline(t_ad_plot,y_range,'edgecolor','k','linestyle',':','linewidth',lw,'edgealpha',.5);
end

%plot mixing ratio lines
a=6.1094*100;
b=17.625;
c=243.04;
d=273.15;
epsilon=.622;
for ia=(min(trange)+10)-tmin_m-max(trange):10:(max(trange)+10)
    %dewpoint at lowest pressure
    Td_l(1)=ia;
    es_l_star=claus_clap(Td_l(1)+d);
    q_star=epsilon*es_l_star/prange(1);
    %calculate vertical profile of es given constant q
    es_l=es_l_star + (q_star/epsilon).*(prange-prange(1));
    %now calculate Ts corresponding to es
    for iz=2:length(prange)
        Td_l(iz)=(c*log(es_l(iz)/a))/(b-log(es_l(iz)/a));
    end
    %coordinate transform Td_l 
    Td_l_plot=(Td_l-min(trange))/max((trange-min(trange)))+y_range;
    clr=[0 0 0];
    plot(Td_l_plot,y_range,'--','linewidth',lw,'color',clr)
end

%plot dew point
for ip=min(trange)-tmin_m:10:max(trange)    
    for iz=1000:25:500
        pon=iz*100;
        gam_on=log(RH./100)+b.*(T-273.15)./(c + (T-273.15));
        Td=c.*gam./(b-gam);
    end
end

%plot moist adiabats
gamma=287/1004;
Hv=2257*1000;
R=287;
cp=1.005*1000;
epsilon=.622;
for ia=(min(trange)+10)-tmin_m-max(trange):5:(max(trange)+10)
    %dewpoint at lowest pressure
    Tm_l(1)=ia;
    es_l_star=claus_clap(Tm_l(1)+d);
    r_l(1)=epsilon*es_l_star/prange(1);
   
    for iz=2:length(prange)
        Tm_l(iz)=Tm_l(iz-1)+((R*(Tm_l(iz-1)+d))/prange(iz-1))*(prange(iz)-prange(iz-1))*(1 + Hv*r_l(iz-1)/(R*(Tm_l(iz-1)+d)))/(cp + (Hv.^2)*r_l(iz-1)*epsilon/(R*((Tm_l(iz-1)+d)^2)));
        es_on=claus_clap(Tm_l(iz)+d);
        r_l(iz)=epsilon*es_on/prange(iz);
    end
    %coordinate transform Td_l 
    Tm_l_plot=(Tm_l-min(trange))/max((trange-min(trange)))+y_range;
    %clr=hsv(10);
    clr=[.5 .5 .5];
%    clr=[clr(10,:) .5];
    plot(Tm_l_plot,y_range,'--','linewidth',lw,'color',clr)
    %patchline(Tm_l_plot,y_range,'edgecolor','m','linestyle','--','linewidth',.5,'edgealpha',.5)
end

%plot wind vectors on the left side
P_norm(1)
if (P_norm(1) < .01)
    ind_range=[P_norm(1) .01 .02 .03:.04:max(P_norm)];
elseif (P_norm(1) >= .01)&(P_norm(1) < .02)
    ind_range=[P_norm(1) .02 .03:.04:max(P_norm)];
else
    ind_range=[P_norm(1) .03:.04:max(P_norm)];
end

horiz_point=-35;
horiz_loc=(horiz_point-min(trange))/max((trange-min(trange)));

%interpolate onto even vertical grid
% u_oninds=interp1(P_norm,wrfdata(:,5),ind_range,'linear','extrap');
% v_oninds=interp1(P_norm,wrfdata(:,6),ind_range,'linear','extrap');
u_oninds=interp1(P_norm,wrfdata(:,5),ind_range);
v_oninds=interp1(P_norm,wrfdata(:,6),ind_range);
u_norm=u_oninds./sqrt(u_oninds.^2+v_oninds.^2);
v_norm=v_oninds./sqrt(u_oninds.^2+v_oninds.^2);
plot([horiz_loc horiz_loc],[0.04 .95],'w','linewidth',45)
plot([horiz_loc horiz_loc],[0.02 .99],'k:','linewidth',1)
%feather length
flen=.04;
u_norm=u_norm*flen;
v_norm=v_norm*flen;
%wind barb lengths
blen=.025;
%distance along feather between barbs
barb_space=.007;
%plot wind barbs
kconv=1.94384;
for iz=1:length(ind_range)
    %get wind speed
    wsp_kt=kconv*sqrt(u_oninds(iz)^2 + v_oninds(iz)^2);
    
    if (wsp_kt>=2.5)
        plot([horiz_loc horiz_loc-u_norm(iz)],[ind_range(iz) ind_range(iz)-v_norm(iz)],'k')
    end
   
    %find number of 5 kt barbs
    nfive_b=round((wsp_kt-floor(wsp_kt/10)*10)/5);
    
    %find number of 10 kt barbs
    nten_b=floor(wsp_kt/10);
    if (nfive_b>1)
        nten_b=nten_b+1;
        nfive_b=0;
    end

    %find number of 50 kt barbs
    nfifty_b=floor(wsp_kt/50);
    nten_b=nten_b-nfifty_b*5;
    if (nten_b>4)
        nfifty_b=nfifty_b+1;
        nten_b=0;
    end
    
    x_on=horiz_loc-u_norm(iz);
    y_on=ind_range(iz)-v_norm(iz);
    
    ib_eff=1;
    ib_on=1;
    barb_mat=[nfifty_b nten_b nfive_b];
    for ib=1:(nfifty_b+nten_b+nfive_b)
        while (ib_eff>barb_mat(ib_on))
            ib_eff=1;
            ib_on=ib_on+1;
        end
        
        %move back a tad if first 50 kt barb
        if (ib_on==1&ib==1)
            x_on=x_on+barb_space*u_norm(iz)/(2*flen);
            y_on=y_on+barb_space*u_norm(iz)/(2*flen);
        end
        
        if (ib_on==1)
            plot([x_on-.5*barb_space*u_norm(iz)/flen x_on-blen*v_norm(iz)/(flen)],[y_on-.5*barb_space*v_norm(iz)/flen y_on+blen*u_norm(iz)/(flen)],'k')
            plot([x_on+.5*barb_space*u_norm(iz)/flen x_on-blen*v_norm(iz)/(flen)],[y_on+.5*barb_space*v_norm(iz)/flen y_on+blen*u_norm(iz)/(flen)],'k')
            x_on=x_on+barb_space*u_norm(iz)/flen;
            y_on=y_on+barb_space*v_norm(iz)/flen;
        elseif (ib_on==2)
            plot([x_on x_on-blen*v_norm(iz)/(flen)],[y_on y_on+blen*u_norm(iz)/(flen)],'k')
            x_on=x_on+barb_space*u_norm(iz)/flen;
            y_on=y_on+barb_space*v_norm(iz)/flen;
        elseif (ib_on==3)
            plot([x_on x_on-blen*v_norm(iz)/(2*flen)],[y_on y_on+blen*u_norm(iz)/(2*flen)],'k')
            x_on=x_on+barb_space*u_norm(iz)/flen;
            y_on=y_on+barb_space*v_norm(iz)/flen;
        end
        ib_eff=ib_eff+1;    
    end
end

mesh_=[T_plot Td_trans];
Td_trans_=min(mesh_,[],2);

clrs=summer(10);

plot(Td_trans_,P_norm,'-','linewidth',3,'color',clrs(2,:))
plot(T_plot,P_norm,'-','linewidth',3,'color','r')
plot(Tv_plot,P_norm,'-','linewidth',1,'color','r')
xlim([0 1])
ylim([-0.03 1])
x_label=[min(trange):10:max(trange)];
for ix=1:length(x_label)
    xCell(ix)=cellstr(num2str(x_label(ix)));
end
x_range_star=(x_label-min(trange))/max((trange-min(trange)));
%set(gca,'XTick',x_range,'YTick',y_range,'YTickLabel',zCell,'XTickLabel',xCell);
set(gca,'XTick',x_range_star,'XTickLabel',xCell);
y_label=[1000:-100:100];
for iy=1:length(y_label)
    yCell(iy)=cellstr(num2str(y_label(iy)));
end
y_range_star=-(log10(y_label*100)-max(log10(prange)))/max(-(log10(prange)-max(log10(prange))));
set(gca,'YTick',y_range_star,'YTickLabel',yCell,'YGrid','on');
xlabel('Temperature (C)')
ylabel('Pressure (hPa)')
%axis square

%==========================================================================
%==========================================================================
%==========================================================================
%=======CODE BELOW HERE FOR PLOTTING HODOGRAPH=============================
%==========================================================================
%==========================================================================
%==========================================================================

% low_range=false;
% 
% if ~low_range
% 
%     scatter(.85,.85,110000,'w.')
%     scatter(.85,.85,6500,'ko')
%     scatter(.85,.85,1700,'ko')
%     scatter(.75,.85,1,'k.')
%     scatter(.8,.85,1,'k.')
%     scatter(.85,.85,1,'k.')
%     scatter(.85,.75,1000,'w.')
%     scatter(.85,.8,1000,'w.')
%     text(.84,.8,'25')
%     text(.84,.75,'50')
%     crad=sqrt(100000/pi);
% 
%     convfact=1.94384449;
% 
%     ukt=.05*u_oninds*convfact/25 + .85;
%     vkt=.05*v_oninds*convfact/25 + .85;
% 
%     plot(ukt,vkt,'k')
% 
% else
% 
%     scatter(.85,.85,110000,'w.')
%     scatter(.85,.85,6500,'ko')
%     scatter(.85,.85,1700,'ko')
%     scatter(.75,.85,1,'k.')
%     scatter(.8,.85,1,'k.')
%     scatter(.85,.85,1,'k.')
%     scatter(.85,.75,1000,'w.')
%     scatter(.85,.8,1000,'w.')
%     text(.84,.8,'12')
%     text(.84,.75,'25')
%     crad=sqrt(100000/pi);
% 
%     convfact=1.94384449;
% 
%     ukt=.05*u_oninds*convfact/(25/2) + .85;
%     vkt=.05*v_oninds*convfact/(25/2) + .85;
% 
%     plot(ukt,vkt,'k','linewidth',1.5)
%     
% end

end








%==========================================================================
%==========================================================================
%==========================================================================
%==========================================================================



