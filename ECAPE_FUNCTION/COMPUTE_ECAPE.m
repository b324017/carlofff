
clear all
close all

cd /opt/src/ECAPE_FUNCTION
pkg load statistics

%SET DEFAILT TEXT TO LATEX FOR PLOTTING
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
%set(groot, 'defaultLegendInterpreter','latex'); 
set(groot, 'defaultTextInterpreter','latex');


%get current directory
cdir=pwd;
fndir = [cdir '/functions/'];
path(path,fndir); %add path to functions folder

%==========================================================================
%==========================================================================
%==========================================================================
%CONSTANTS
global Rd Rv epsilon cp gamma g Gamma_d xlv xls cpv cpl cpi pref ttrip Pr ksq
%
Rd=287.04; %DRY GAS CONSTANT
Rv=461.5; %GAS CONSTANT FOR WATEEER VAPRR
epsilon=Rd./Rv; %RATO OF THE TWO
cp=1005; %HEAT CAPACITY OF DRY AIR AT CONSTANT PRESSUREE
gamma=Rd/cp; %POTENTIAL TEMPERATURE EXPONENT
g=9.81; %GRAVITATIONAL CONSTANT
Gamma_d=g/cp; %DRY ADIABATIC LAPSE RATE
xlv=2501000; %LATENT HEAT OF VAPORIZATION AT TRIPLE POINT TEMPERATURE
xls=2834000; %LATENT HEAT OF SUBLIMATION AT TRIPLE POINT TEMPERATURE
cpv=1870; %HEAT CAPACITY OF WATER VAPOR AT CONSTANT PRESSURE
cpl=4190; %HEAT CAPACITY OF LIQUID WATER
cpi=2106; %HEAT CAPACITY OF ICE
pref=611.65; %REFERENCE VAPOR PRESSURE OF WATER VAPOR AT TRIPLE POINT TEMPERATURE
ttrip=273.15; %TRIPLE POINT TEMPERATURE
Pr=1/3; %PRANDTL NUMBER
ksq=0.18; %VON KARMAN CONSTANT
%==========================================================================
%==========================================================================
%==========================================================================


%LOAD SAMPLE SOUNDING
data=load('SOUNDING.txt');
z0 = data(:,1); %height (in m)
p0 = data(:,2); %pressure (in Pa)
T0 = data(:,3); %temperature (in K)
q0 = data(:,4); %water vapor mass fraction (specific humidity), in kg/kg
u0 = data(:,5); %u wind component, in m/s
v0 = data(:,6); %v wind component, in m/s

%COMPUTE THE MOIST STATIC ENERGY
MSE0 = cp.*T0 + xlv.*q0 + g.*z0;

%ASSUME MOST UNSTABLE PARCEL CORRESPONDS TO THE LARGEST MSE IN THE LOWEST 5
%KM OF THE ATMOSPHERE
f5km = find(z0<=5000);
[MSE_max,parcel_start] = max(MSE0(f5km));

%GET THE UNDILUTED MUCAPE AND MUCIN
[MUCAPE,MUCIN,MULFC,MUEL]=compute_CAPE_AND_CIN(T0,p0,q0,parcel_start,0,0,z0,273.15,253.15); %GOING TO ASSUME A 20 C MIXED PHASE REGION

%MLCAPE AND MLCIN: JUST AVERAGE OVER THE LOWEST 1 KM OF THE ATMOSPHERE
f1km = find(z0<=1000);
CAPE_z=zeros(size(f1km));
CIN_z=zeros(size(f1km));
LFC_z=zeros(size(f1km));
EL_z=zeros(size(f1km));
for izz=1:length(f1km)
    iz = f1km(izz);
    [CAPE_z(iz),CIN_z(iz),LFC_z(iz),EL_z(iz)]=compute_CAPE_AND_CIN(T0,p0,q0,iz,0,0,z0,273.15,253.15);
end
MLCAPE=mean(CAPE_z);
MLCIN=mean(CIN_z);

%COMPUTE NCAPE
[NCAPE,MSE0_star,MSE0_bar]=compute_NCAPE(T0,p0,q0,z0,273.15,253.15,MULFC,MUEL);

%COMPUTE V_SR
V_SR = compute_VSR(z0,u0,v0);

%THESE ARE A BUNCH OF CONSTANT PARAMTERS SET FOR THE ECAPE CALCULATION
L=120;
H=MUEL;
l=L./H;
sigma = 1.1;
alpha=0.8;
pitchfork=ksq*(alpha^2)*(pi^2)*L/(4.*Pr*(sigma^2)*H);
vsr_tilde = V_SR./sqrt(2*MUCAPE);
N_tilde = NCAPE./MUCAPE;

%EQUATION SOLVES FOR THE NONDIMENSIONAL ECAPE (E_TILDE_A IN THE PAPER)
E_tilde = vsr_tilde.^2 + ...
    ( -1 - pitchfork - (pitchfork/(vsr_tilde^2 ))*N_tilde + sqrt(...
    (1 + pitchfork + (pitchfork/(vsr_tilde^2 ))*N_tilde)^2 + ...
    (4*(pitchfork/(vsr_tilde^2 ))*(1 - pitchfork*N_tilde) ) ) )/( 2*pitchfork/(vsr_tilde^2) );

%RE-DIMENSIONALIZE BY MULTIPLYING BY UNDILUTED MUCAPE
ECAPE = E_tilde*MUCAPE;

%THE LAST BIT OF CODE HERE COMPUTES A LIFTED PARCEL DENSITY TEMPERATURE
%THAT CORRESPONDS TO THE ECAPE, VIA 2 METHODS

%FIRST, WE JUST COMPUTE THE FRACTIONAL ENTRAINMENT RATE FROM THE ECAPE
%eps = 2.5.*2*ksq*L./(MUEL.*Pr);
eps = 2*ksq*L./(MUEL.*Pr);
varepsilon = .65.*eps.*(alpha.^2).*(pi.^2).*E_tilde./(4.*(sigma.^2).*MUEL.*(vsr_tilde.^2 ) ); %THIS IS THE FRACTIONAL ENTRAINMENT RATE
[T_lif,Qv_lif,Qt_lif,B_lif]=lift_parcel_adiabatic(T0,p0,q0./(1 + q0),parcel_start,varepsilon,0,z0,273.15,253.15); %LIFT A NEW PARCEL WITH THE FRACTIONAL ENTRAINMENT RATE CONSISTENT WITH ECAPE
T_rho_lif = T_lif.*( 1 + (Rv/Rd).*Qv_lif - Qt_lif); %COMPUTE THE DENSITY TEMPERATURE OF THE LIFTED PARCEL
Th_rho_lif = T_rho_lif.*(1000.*100./p0).^(Rd/cp); %COMPUTE THE DENSITY POTENTIAL TEMPERATURE OF THE LIFTED PARCEL
csvwrite(['sounding_lifted.txt'],[Th_rho_lif p0 z0 .000000000001.*ones(size(q0)) .000000000001.*ones(size(q0)) .000000000001.*ones(size(q0))]); %WRITE OUT TO SOUNDING FOR PLOTTING


[T_lif,Qv_lif,Qt_lif,B_lif2]=lift_parcel_adiabatic(T0,p0,q0./(1 + q0),parcel_start,0,0,z0,273.15,253.15); %FOR REFERENCE, ALSO LIFT (AND PLOT) AN UNDILUTED PARCEL
T_rho_lif = T_lif.*( 1 + (Rv/Rd).*Qv_lif - Qt_lif); %DENSITY TEMPERATURE OF UNDILUTED PARCEL
Th_rho_lif = T_rho_lif.*(1000.*100./p0).^(Rd/cp); %DENSITY POTENTIAL TEMPERATURE OF UNDILUTED PARCEL
csvwrite(['sounding_liftedE.txt'],[Th_rho_lif p0 z0 .000000000001.*ones(size(q0)) .000000000001.*ones(size(q0)) .000000000001.*ones(size(q0))]); %WRITE OUT TO SOUNDING FOR PLOTTING

Th0=T0.*(1000.*100./p0).^(Rd/cp);
csvwrite(['sounding_base.txt'],[Th0 p0 z0 q0./(1 + q0) u0 v0]); %WRITE OUT THE ENVIRONMENTAL SOUNDING FOR PLOTTING

%==============================================================================
%==============================================================================
%==============================================================================
%SECOND METHOD, USE EQ. 18 FROM THE ECAPE PAPER
%THINGS YOU WILL NEED:
%1) ALL THE CONSTANTS THAT ARE USED TO CALCULATE ECAPE
%2) VERTICAL PROFILES OF H_HAT AND H_0^* (SHOULD HAVE THESE THINGS ALREADY FROM ECAPE CALCULATION)
%3) PROFILE OF THE BUOYANCY OF AN UNDILUTED LIFTED AIR PARCEL (THIS CAN COME FROM WHATEVER ROUTINE YOU TYPICALLY USE TO COMPUTE CAPE)
%GET PROFILES OF MSE_STAR AND MSE_BAR
%
eps = 2*ksq*L./(MUEL.*Pr);
varepsilon = .65.*eps.*(alpha.^2).*(pi.^2).*E_tilde./(4.*(sigma.^2).*MUEL.*(vsr_tilde.^2 ) ); %THIS IS THE FRACTIONAL ENTRAINMENT RATE
[NCAPE,MSE0_star,MSE0_bar]=compute_NCAPE(T0,p0,q0,z0,273.15,253.15,MULFC,MUEL);
Buoy_UD = B_lif2; %THIS IS THE BUOYANCY PROFILE OF AN UNDILUTED PARCEL
B_ent = Buoy_UD.*exp(-varepsilon.*z0) + (g./(cp.*T0)).*(1 - exp(-varepsilon.*z0) ).*(MSE0_bar-MSE0_star); %GET THE BUOYANCY OF THE DILUTED PARCEL FROM EQ 18 IN THE PAPER
T_rho = T0.*(1 + (Rv/Rd-1).*q0 ).*B_ent./g + T0.*(1 + (Rv/Rd-1).*q0 ); %GET THE DENSITY TEMPERATURE OF THE PARCEL
Th_rho = T_rho.*(1000.*100./p0).^(Rd/cp); %GET THE DENSITY POTENTIAL TEMPERATURE OF THE PARCEL
csvwrite(['sounding_lifted_APPROX.txt'],[Th_rho p0 z0 .000000000001.*ones(size(q0)) .000000000001.*ones(size(q0)) .000000000001.*ones(size(q0))]); %WRITE OUT TO SOUNDING FOR PLOTTING


%==============================================================================
%==============================================================================
%==============================================================================


reset(0) ;
scrsz = get(0,'ScreenSize');
%%h=figure('Position',[1 scrsz(4)/1.5 scrsz(3)/2 scrsz(4)/2])
h=figure();
set(h,'renderer', 'painters')
hold on
set(gca,'fontsize',25)
%%%

hp=skewt_logp_multpan; %PLOT ENVIRONMENTAL SOUNDING USING MY PROPRIETARY SKEW-T SCRIPT
[skewfig_handle]=skewt_logp_plotover(hp,['sounding_lifted.txt'],'k-',1,'k',1,'k','k') %PLOT OVER THE LIFTED ENTRAINING PARCEL IN BLACK
[skewfig_handle]=skewt_logp_plotover(hp,['sounding_liftedE.txt'],'k-',1,'k',1,'k','b') %PLOT OVER THE LIFTED UNDILUTED PARCEL IN BLUE
[skewfig_handle]=skewt_logp_plotover(hp,['sounding_lifted_APPROX.txt'],'k-',1,'k',1,'k','m') %PLOT OVER THE LIFTED ENTRAINING PARCEL IN BLACK

E_tilde
nansum(max(B_lif,0))./nansum(max(B_lif2,0))
