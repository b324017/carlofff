
addpath ~/carlofff/ECAPE_FUNCTION/functions ~/carlofff/Fortran ~/carlofff/fun
[~] = mkdir("~/carlofff/data/tmp") ;
pkg load netcdf
global CX = 1000 PARLOOP = ~true VERBOSE = ~true BLD  MOIST ;
NCPU = 0 ; MOIST = "pw" ;
source ~/carlofff/conf.m

function res = heaviside(x)
  res = double(x > 0) ;
  res(x == 0) = 0.5 ;
endfunction

##if exist(ofile = "data/era5/ptr.ob", "file") == 2

##   load(ofile) ;

##else

##   load(ofile) ;
##   era5 = read_era5("data/era5") ;
##   save(ofile, "era5")
##   exit ;

##endif

if 0
   plot(datenum(era5.id), mean(mean(era5.cin, 2), 3)) ;

   d = [2014 7 28] ;
   I = sdate(era5.id, d) ;
   xm = squeeze(mean(era5.cape(I,:,:))) ;
   clf ; hold on ;
   imagesc(era5.lon, era5.lat, xm') ;
   set(gca, "ydir", "normal") ;
   xlabel("longitude") ; ylabel("latitude") ;
   colormap(brewermap(9, "Blues"))
   hc = colorbar ;
   pos = get(get(hc, "label"), "extent")(2) ;
   set(get(hc, "label"), "string", "cape  [J/kg]", "position", [0.5 2.1*pos], "rotation", 0) ;

   hb = borders("germany", "color", "black") ;
endif

## ESGF
scn = {"historical" "ssp126" "ssp370" "ssp585"}{1} ;
options = weboptions("Timeout", 30) ;

## cape, cin
if length(argv) > 0 && ~strcmp(argv{1}, "-i") && ismember(argv{1}, {"prc" "prw"})
   dstr = "Day" ;
else
   dstr = "HighRes" ;
endif
[~] = mkdir(sprintf("data/cmip6/%s", dstr)) ;
[~] = mkdir(sprintf("url/%s", dstr)) ;

expid = "historical,ssp126,ssp585" ;
varlbl = "r1i1p1f1,r2i1p1f1,r3i1p1f1,r4i1p1f1,r5i1p1f1" ;
facets = "experiment_id%2Cvariant_label%2Cnominal_resolution%2Cfrequency%2Cvariable_id%2Cdata_node" ;

for v = {"hus" "ua" "prc" "prw"}

   v = v{:} ;
   if nargin > 0 && ~strcmp(argv{1}, "-i") && ~ismember(v, argv) continue ; endif

   switch v
      case "hus"
	 url = ["https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=" facets "&latest=true&query=*&data_node=aims3.llnl.gov,cmip.bcc.cma.cn,crd-esgf-drc.ec.gc.ca,dpesgf03.nccs.nasa.gov,esg-cccr.tropmet.res.in,esg-dn1.nsc.liu.se,esg-dn2.nsc.liu.se,esg-dn3.nsc.liu.se,esg.lasg.ac.cn,esg1.umr-cnrm.fr,esgdata.gfdl.noaa.gov,esgf-data.ucar.edu,esgf-data02.diasjp.net,esgf-data03.diasjp.net,esgf-data1.llnl.gov,esgf-nimscmip6.apcc21.org,esgf.bsc.es,esgf.ceda.ac.uk,esgf.dwd.de,esgf.nci.org.au,esgf.rcec.sinica.edu.tw,noresg.nird.sigma2.no,vesg.ipsl.upmc.fr&experiment_id=" expid "&variant_label=" varlbl "&frequency=3hrPt,6hr,6hrPt&variable_id=" v] ;
	 if strcmp(MOIST, "pw")
	    sfx = "pw" ;
	    V = {"pw"} ;
	 else
	    sfx = "cnvenv" ;
	    V = {"cape" "cin"} ;
	 endif
      case "ua"
	 url = ["https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=" facets "&latest=true&query=*&data_node=cmip.bcc.cma.cn,esg-dn3.nsc.liu.se,esg.lasg.ac.cn,esgf-data.ucar.edu,esgf-data03.diasjp.net,esgf.dwd.de,esgf.nci.org.au,esgf.rcec.sinica.edu.tw,noresg.nird.sigma2.no&experiment_id=" expid "&variant_label=" varlbl "&nominal_resolution=100%20km&table_id=6hrLev&variable_id=" v] ;
	 url = ["https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=" facets "&latest=true&query=*&experiment_id=" expid "&variant_label=" varlbl "&frequency=6hrPt,day&nominal_resolution=100%20km&variable_id=" v] ;
	 sfx = "vort" ;
	 V = {"div" "vort"} ;
      case "prc"
	 url = ["https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=activity_id%2C+data_node%2C+source_id%2C+institution_id%2C+source_type%2C+experiment_id%2C+sub_experiment_id%2C+nominal_resolution%2C+variant_label%2C+grid_label%2C+table_id%2C+frequency%2C+realm%2C+variable_id%2C+cf_standard_name&latest=true&query=*&experiment_id=historical,ssp126,ssp585&frequency=day&variable_id=" v] ;
##	 url = ["https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=" facets "&latest=true&query=*&experiment_id=" expid "&variant_label=" varlbl "&frequency=6hr,6hrPt,day&nominal_resolution=100%20km&variable_id=" v] ;
	 sfx = v ;
	 V = {v} ;
      case "prw"
	 url = ["https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=" facets "&latest=true&query=*&data_node=aims3.llnl.gov,esg-dn2.nsc.liu.se,esgf-data.ucar.edu,esgf-data04.diasjp.net,esgf-data1.llnl.gov,esgf.bsc.es,esgf.ceda.ac.uk,esgf.dwd.de,esgf.nci.org.au,esgf.rcec.sinica.edu.tw,noresg.nird.sigma2.no&experiment_id=" expid "&variant_label=" varlbl "&frequency=3hr,day&nominal_resolution=100%20km&variable_id=" v] ;
	 url = ["https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=" facets "&latest=true&query=*&experiment_id=" expid "&variant_label=" varlbl "&frequency=3hr,day&nominal_resolution=100%20km&variable_id=" v] ;
##	 url = "https://esgf-node.llnl.gov/esg-search/search?project=CMIP6&offset=0&limit=10000&type=Dataset&format=application%2Fsolr%2Bjson&facets=activity_id%2C+data_node%2C+source_id%2C+institution_id%2C+source_type%2C+experiment_id%2C+sub_experiment_id%2C+nominal_resolution%2C+variant_label%2C+grid_label%2C+table_id%2C+frequency%2C+realm%2C+variable_id%2C+cf_standard_name&latest=true&query=*&experiment_id=historical,ssp126,ssp585&frequency=3hr,day&nominal_resolution=100%20km&source_id=AWI-CM-1-1-MR&variable_id=prw&variant_label=r1i1p1f1,r2i1p1f1,r3i1p1f1,r4i1p1f1" ;
	 sfx = v ;
	 V = {v} ;
      otherwise
	 url = ["http://esgf-data.dkrz.de/esg-search/search/?offset=0&limit=10000&type=Dataset&replica=false&latest=true&source_type=AOGCM&nominal_resolution=250+km&experiment_id=historical%2CpiControl%2Cssp126%2Cssp370%2Cssp585&variant_label=r1i1p1f1&frequency=6hrPt&variable_id=" v "%2Cta&mip_era=CMIP6&activity_id%21=input4MIPs&facets=mip_era%2Cactivity_id%2Cproduct%2Csource_id%2Cinstitution_id%2Csource_type%2Cnominal_resolution%2Cexperiment_id%2Csub_experiment_id%2Cvariant_label%2Cgrid_label%2Ctable_id%2Cfrequency%2Crealm%2Cvariable_id%2Ccf_standard_name%2Cdata_node&format=application%2Fsolr%2Bjson"] ;
   endswitch

   for i = 1 : 5
      S = webread(url, options) ;
      if ~isempty(S)
	 S = jsondecode(S) ;
	 break ;
      endif
   endfor
   if isempty(S) continue ; endif

   if nargin > 1 && ~strcmp(argv(){1}, "-i")
      k = str2num(argv{2}) - 1 ;
   else
      k = 0 ;
   endif
   for r = S.response.docs'(k+1:end)

      k++ ;
      if iscell(r) r = r{:} ; endif
      if ~isfield(r, "url") continue ; endif

      Cout = {r.experiment_id{:} r.source_id{:} r.variant_label{:} sfx r.frequency{:}} ;
      if isnewer(ofile = sprintf("data/cmip6/%s/%s.%s.%s.%s.%s.ob", dstr, Cout{:}))

	 printf("%d: <-- %s\n", k, ofile) ;
##	 load(ofile) ;

      else

	 if exist(Ffile = sprintf("url/%s/%s.%s.%s.%s.%s.lst", dstr, Cout{:}), "file") == 2

	    F = strsplit(fileread(Ffile))(1:end-1) ;

	 else

	    try
	       u = r.url{1} ; u = strsplit(u, "#"){1} ;
	       lbl = strsplit(u, ".") ; j = find(strcmp(lbl, "prc")) - 2 ; lbl = lbl{j} ;
	       lbl = nthargout(5, @regexp, lbl, "r([0-9]*)i"){:}{:} ;
	       if str2num(lbl) > 5 continue ; endif
	       printf("%d: trying %s...\n", k, u) ;
	       XT = xmltree(webread(u, options)) ;
	       fXT = find(XT, "name", "access") ;
	       J = strcmp(arrayfun(@(i) attributes(XT, "get", i){2}.val, fXT, "UniformOutput", false), "OpenDAPServer") ;
	       X = arrayfun(@(i) get(XT, i), fXT(J), "UniformOutput", false) ;
	       TC = ~isempty(J) ;
	    catch
	       TC = false ;
	    end_try_catch
	    if ~TC
	       warning("no OPeNDAP: %s\n", u) ;
	       continue ;
	    endif
	    F = parfun(@(x) sprintf("http://%s/thredds/dodsC/%s", r.data_node, x.attributes{1}.val), X, "UniformOutput", false) ;

	 endif
##	 C = parfun(@(f) getcmip(:, f, GLON, GLAT, GLEV, CX), F) ;
	 pkg load parallel
	 C = parcellfun(nproc, @(f) getcmip(:, f, GLON, GLAT, GLEV, CX), F, "ErrorHandler", @errfun) ;
	 if ~any(I = parfun(@(c) ~isequaln(c.(V{1}), NaN), C)) continue ; endif
	 C = C(I) ;
	 if iscell(C) C = cell2mat(C) ; endif
	 [~,I] = sort(parfun(@(c) datenum(c.id(1,:)), C)) ; C = C(I) ;

	 s = C(1) ;
	 s.nc.Filename = parfun(@(c) c.nc.Filename, C, "UniformOutput", false) ;
	 s.id = cat(1, parfun(@(c) c.id, C, "UniformOutput", false){:}) ;
	 for vn = V
	    if 0
	       s.(vn{:}) = cat(1, parfun(@(c) c.(vn{:}), C, "UniformOutput", false){:}) ;
	    else
	       s.(vn{:}) = [] ;
	       for i = 1 : length(C)
		  s.(vn{:}) = cat(1, s.(vn{:}), C(i).(vn{:})) ;
		  C(i).(vn{:}) = [] ;
	       endfor
	    endif
	 endfor
	 printf("%d: --> %s\n", k, ofile) ;
	 save(ofile, "s") ;

	 if 0
	    parfun(@(x) getcmip("del", x, GLON, GLAT, GLEV, v, CX), X) ;
	 endif

      endif

   endfor
endfor
exit

F = glob(sprintf("data/cmip6/%s/*.hus.*ob", dstr))' ;
for j = 1 : length(F)

   f = F{j} ;
   g = strrep(f, ".hus.", ".ta.") ;
   load(f) ; hus = s ; clear s
   load(g) ; ta = s ; clear s

   if 1
      N = size(ta.x) ;
      Q = mat2cell(hus.x, ones(N(1),1), N(2), ones(N(3),1), ones(N(4),1)) ;
      T = mat2cell(ta.x, ones(N(1),1), N(2), ones(N(3),1), ones(N(4),1)) ;
      [cape cin] = arrayfun(@(t, q) getcape(2, hus.lev/100, t-273.15, q), ta.x(1:20,:,1:2,1:3), hus.x(1:20,:,1:2,1:3), "UniformOutput", false) ;
   else
      [mucape, mucin] = compute_CAPE_AND_CIN(t=flip(ta.x(1,:,1,1)),ta.lev,hus.x(1,:,1,1),1,0,0,z,273.15,253.15) ;
      [mucape, mucin] = parfun(@(t,q,z) compute_CAPE_AND_CIN(t,LEVEL,q,1,0,0,z,273.15,253.15), T, Q, Z, "UniformOutput", false) ;
   endif

endfor
