Stations.Network={'GSN'};
Stations.Stations={'Spain.stn'};
Stations.Variable={'Precip'};
[data,Stations]=loadStations(Stations,'ascfile',1);

%Validacion de la Persistencia
umbral=10;

P=data(1:end-1,:);
O=data(2:end,:);

i=find(~isnan(P));
P(i)=P(i)>umbral;

i=find(~isnan(O));
O(i)=O(i)>umbral;

Validation=makeValidation(O,P);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Example with INM network

dmn=readDomain('Iberia');
Stations.Network={'GSN'};
Stations.Stations={'Spain.stn'};
Stations.Variable={'Precip'};

%Training data
dates={'1-Jan-1960','31-Dec-1998'};
[EOF,CP]=getEOF(dmn,'ncp',50,'dates',dates);
[dataE,Stations]=loadStations(Stations,'dates',dates,'ascfile',1);

%Test data
dates={'1-Jan-1999','31-Dec-1999'};
[EOF,CPT]=getEOF(dmn,'ncp',50,'dates',dates);
[dataT,Stations]=loadStations(Stations,'dates',dates,'ascfile',1);

umbral=5;
i=find(~isnan(dataE));
dataE(i)=dataE(i)>umbral;
i=find(~isnan(dataT));
dataT(i)=dataT(i)>umbral;

O=[];P=[];
for j=1:1:180
   [AnalogPat,Neig,NeigDist]=getAnalogous(CPT(j,:),CP,1,'knn',[]);
   %[AnalogPat,Neig,NeigDist]=getAnalogous(CPT(j,:),CP,50,'knn',[]);
   %[AnalogPat,Neig,NeigDist]=getAnalogous(CPT(j,:),CP,200,'knn',[]);
   O=[O;dataT(j,:)];
   P=[P;nanmean(dataE(Neig,:))];
end
Validation=makeValidation(O(:,1),P(:,1));


%para ver estacion por estaci�n
warning off
for i=1:size(O,2)
   Stations.Info.Name(i,:)
   Validation=makeValidation(O(:,i),P(:,i));
   drawnow
   pause
end

   
