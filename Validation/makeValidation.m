function VAL=makeValidation(O,P)
%
%VAL=makeValidation(O,P)
%
%   Realiza y dibuja la validacion de la prediccion P frente a la 
%   observacion O. Se realiza la validacion por filas y se promedia 
%   por columnas. 
%
%Como entrada se indican la prediccion y la observacion, ambas deben 
%ser matrices del mismo tamaño, y pueden contener lagunas (NaNs)
%son de tamaño mxn donde m es el numero de dias y n el numero de 
%estaciones como la validacion se realiza por filas, si se quiere validar
%por dias,se debe llamar VAL=makeValidation(O,P)
%pero si lo que se quiere es validar por estaciones se debe hacer
%VAL=makeValidation(O',P')
%
%La estructura devuelta contiene los siguientes campos.
%   VAL = 
%
%         PC: Probabilidad Climatologica
%        PRB: Probabilidad considerada para la resolucion y fiabilidad
%        RES: Resolucion para cada una de las probabilidades (PRB)
%        REL: Fiabilidad para cada una de las probabilidades (PRB)
%        BSP: Brier Score de la prediccion
%        BSC: BrierScore de la Climatologia (Pc)
%        BSS: Brier Skill Score
%        THR: Umbrales considerados par la categorizacion dde la prediccion
%        HIR: Tasa de Aciertos par la curva ROC
%        FAR: Tasa de "Falsas Alarmas" para la curva ROC
%        RSA: ROC Skill Area
%        CLR: Relacion Coste perdidas consideradas para calcular el Valor
%               Economico (EV)
%         EV: Valor Economico para cada uno de los Umbrales considerados
%        EVx: Valor Economico Maximo de todos los umbrales (THR) 


VAL=struct('PC',[],'PRB',[],'RES',[],'REL',[],'BSP',[],'BSC',[],'BSS',[],...
    'THR',[],'HIR',[],'FAR',[],'RSA',[],'CLR',[],'EV',[],'EVx',[]);

prob=[-0.1 (0.1:0.1:1)]';

%Llevamos a cabo la contabilidad de los eventos
[hi,ob,fa,nob,fbo,fbp]=count(O,P,prob);


j=1;
%Y derivamos las distintas medidas de calidad.
VP(j).Pc=nanmean(ob./(nob+ob),2);
VP(j).FAR=nanmean(fa./repmat(nob,[size(fa,1) 1 1]),2);
VP(j).HIR=nanmean(hi./repmat(ob,[size(hi,1) 1 1]),2);
VP(j).PRB=0.05:0.1:0.95;
VP(j).FBO=nanmean(fbo,2);
VP(j).FBP=nanmean(fbp,2);

drawValgen(VP)
VAL.hdl(1) = hdl2struct(1);
VAL.hdl(2) = hdl2struct(2);

%Probabilidad climatologica a partir de la Observacion del Evento 
VAL.PC=VP(j).Pc;
%Probabilidades para la Fiabilidad y la Resolucion (PRoBability)
VAL.PRB=VP(j).PRB';
%Fiabilidad (RELiability)
VAL.REL=VP(j).FBO./VP(j).FBP;
%Resolucion (RESsolution)
N=sum(VP(j).FBP);
VAL.RES=VP(j).FBP/N;


%Brier Scores de Obsercacion y Prediccion y Brier Skill Score
[VAL.BSS,VAL.BSP,VAL.BSC]=bss(O,P,VAL.PC);
VAL.BSP=nanmean(VAL.BSP,2);
VAL.BSC=nanmean(VAL.BSC,2);
VAL.BSS=nanmean(VAL.BSS,2);

%Curva ROC
%Umbrales de categorizacion de la probabilidad (THReshold)
VAL.THR=[0:0.1:1]';
%Tasa de aciertos (HIt Rate)
VAL.HIR=VP(j).HIR;
%Tasa de "falsas alarmas" (False Alarm Rate)
VAL.FAR=VP(j).FAR;

%ROC Skill Area
x=cat(1,VP(j).FAR,ones([1 size(VP(j).FAR,2) size(VP(j).FAR,3)]));
y=cat(1,VP(j).HIR,zeros([1 size(VP(j).HIR,2) size(VP(j).HIR,3)]));
VAL.RSA=ones([1 size(x,2) size(x,3)])+NaN;
VAL.RSA(:,:,:)=polyarea(x,y,1)*2-1;

%Relacion Costes Perdidas (Cost Losses Ratio)
VAL.CLR=linspace(0,1,1000);
%Valor Economico para cada uno de los umbrales de categorizacion
%   de la probabilidad y el maximo de estos (EV,EVx)
[VAL.EV,VAL.EVx]=econVal(VP(j).HIR,VP(j).FAR,VAL.PC,VAL.CLR);

