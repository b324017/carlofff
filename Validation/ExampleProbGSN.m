dmn=readDomain('Nao');
Stations.Network={'GSN'};
Stations.Stations={'Europe.stn'};
Stations.Variable={'Precip'};
umbral=1;
NCPs=1;

%conjunto de entrenamiento
dates={'1-Jan-1960','31-Dec-1994'};
fieldE=getFieldfromEOF(dmn,'ncp',NCPs,'var',151,'time',12,'level',0,...
   'startdate',dates{1},'enddate',dates{2});
[dataE,Stations]=loadStations(Stations,'dates',dates,'ascfile',1);
i=find(~isnan(dataE));
dataE(i)=dataE(i)>umbral;

%conjunto de test
dates={'1-Jan-1995','31-Dec-1995'};
fieldT=getFieldfromEOF(dmn,'ncp',NCPs,'var',151,'time',12,'level',0,...
   'startdate',dates{1},'enddate',dates{2});
[dataT,Stations]=loadStations(Stations,'dates',dates,'ascfile',1);

i=find(~isnan(dataT));
dataT(i)=dataT(i)>umbral;

O=[];P=[];
for j=1:1:size(fieldT.dat,1)
   j
   %Obtener analogos usando K-vecinos
   [AnalogPat,Neig,NeigDist]=getAnalogous(fieldT.dat(j,:),fieldE.dat,25,'knn',[]);
   O=[O;dataT(j,:)];
   P=[P;nanmean(dataE(Neig,:))];
end

Validation=makeValidation(O,P);
break
%para ver estacion por estaci�n
pause
warning off
for i=1:1:size(O,2)
   close all
   Validation=makeValidation(O(:,i),P(:,i));
   [Stations.Info.Name(i,:) '  ' num2str(Validation.RSA)]
   drawnow
   pause
end


   
