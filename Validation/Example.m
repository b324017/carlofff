Stations.Network={'GSN'};
Stations.Stations={'Spain.stn'};
Stations.Variable={'Precip'};
[data,Stations]=loadStations(Stations,'ascfile',1);

%Validacion de la Persistencia
umbral=10;

P=data(1:end-1,:);
O=data(2:end,:);

i=find(~isnan(P));
P(i)=P(i)>umbral;

i=find(~isnan(O));
O(i)=O(i)>umbral;

Validation=makeValidation(O,P);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Example with INM network

Stations.Network={'INM'};
Stations.Stations={'completas.stn'};
Stations.Variable={'Precip0707'};
dates={'1-Jan-1999','31-Dec-1999'};
[data,Stations]=loadStations(Stations,'dates',dates,'zipfile',1);

%Validacion de la Persistencia
umbral=1;

%predecimos con los fenomenos de hoy lo que ocurrir� ma�ana
P=data(1:end-1,:);
O=data(2:end,:);

i=find(~isnan(P));
P(i)=P(i)>umbral;

i=find(~isnan(O));
O(i)=O(i)>umbral;

Validation=makeValidation(O,P);

pause

%Para validar una �nica estaci�n
Validation=makeValidation(O(:,2),P(:,2));

brierscore=nanmean((O(:,2)-P(:,2)).^2)
pcli=nanmean(O(:,2))
bsc=nanmean((O(:,2)-pcli).^2)

bss=1-brierscore/bsc
alfa=sum(O(:,2)==1 & P(:,2)==1);
beta=sum(O(:,2)==0 & P(:,2)==1);
gamma=sum(O(:,2)==1 & P(:,2)==0);
delta=sum(O(:,2)==0 & P(:,2)==0);

h=alfa/(alfa+gamma)
f=beta/(beta+delta)
Validation.HIR
Validation.FAR

%el m�ximo de la curva de valor economico es HK=HIR-FAR
max(Validation.EVx)
h-f

