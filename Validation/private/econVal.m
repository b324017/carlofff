function [EV,EVx]=econVal(HIR,FAR,Pc,Valor)
Valorm=min(Valor,Pc);
EV=(ones(size(HIR))*Valorm-HIR*Pc*Valor-FAR*(1-Pc)*Valor-(1-HIR)*Pc*ones(size(Valor)))./(ones(size(HIR))*(Valorm-Pc*Valor));
EVx=max(EV,[],1);