function [HIR,FAR]=roc(Obsr,Prdc,P)
nP=size(P,1);
HIR(nP,size(Prdc,2),size(Prdc,3))=0;
FAR(nP,size(Prdc,2),size(Prdc,3))=0;

indn=~isnan(Obsr) & ~isnan(Prdc);
ind=find(indn);
i=find(sum(indn,1)==0);
o(size(Obsr,1),size(Obsr,2),size(Obsr,3))=0;
h(size(Prdc,1),size(Prdc,2),size(Prdc,3))=0;

for l=1:nP
   %�Se ha producido el evento con una probabilidad > P para
   % la observacion y la prediccion?
   o(ind)=Obsr(ind)>0;
   h(ind)=Prdc(ind)>=P(l) & o(ind);
   count=sum(o,1);
   %count(find(count==0))=1;
   hit=sum(h,1);
   hit(i)=NaN;
   HIR(l,:,:)=hit./count;
   
   o(ind)=~o(ind);
   h(ind)=Prdc(ind)>=P(l) & o(ind);
   count=sum(o,1);
   
   %count(find(count==0))=1;
   hit=sum(h,1);
   FAR(l,:,:)=hit./count;
end
