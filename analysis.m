
addpath ~/carlofff/fun
pkg load dataframe io
[~, ~] = mkdir("data") ;
cd ~/carlofff
A = argv ;
global isoctave JVAR EXP REG NH MON IMB CNVDUR BLD VERBOSE PARALLEL MAXX CALENDAR RCNT
source conf.m
m2s = @(mdl, k) strrep(strrep(mdl, "-", "_"), "_", [repmat("\\", 1, k), "_"]) ;
m2si = @(mdl, k) strrep(strrep(mdl, "_", "-"), "-", [repmat("\\", 1, k), "-"]) ;
JMDL = 1 : length(MDL) ; JNET = 1 : length(NET) ;
clear skl ;
for jMDL = JMDL
   mdl = MDL{jMDL} ;
   if exist(f = sprintf("data/%s.%02d/Shallow.%s.%s.%s.ob", EXP, NH, [mdl sIMB], IND, LNAME), "file") ~= 2
      skl.(m2s(mdl,0)) = -Inf ;
   else
      load(f) ;
      skl.(m2s(mdl,0)) = shallow.rpss.VAL ;
   endif
endfor
for jNET = JNET
   mdl = NET{jNET} ;
   if exist(f = sprintf("data/%s.%02d/%dx%d/Deep.%s.%s.%s.ob", EXP, NH, RES{jNET}, [mdl sIMB], IND, LNAME), "file") ~= 2
      skl.(m2s(mdl,0)) = -Inf ;
   else
      load(f) ;
      skl.(m2s(mdl,0)) = deep.rpss.VAL ;
   endif
endfor


GN = {"SCEN" "DS" "GCM" "REA" "RGN" "YEAR"} ;
G = GT = "" ; X = Y = [] ; clear s ; I = ":" ; JSIM = [1 : 4] ;
load(sprintf("data/%s.%02d/%s.ob", EXP, NH, LNAME)) ;
p = reorder(pdd.uc, REG.name) ; pdd.oc = pdd.uc(p) ;
skl0 = 0 ; ny = 10 ;
clear b T sd ; wrn = warning() ;
for jSIM = JSIM

   sim = SIM{jSIM} ;

   ## load atmospheric variables
   sfile = sprintf("data/%s.%02d/%s.%s.%s.prob.ob", EXP, NH, sim, IND, LNAME) ;
   printf("<-- %s\n", sfile) ;
   load(sfile) ;

   for [mdl smdl] = prob
      if skl.(smdl) < skl0
	 warning(sprintf("skl:%s", smdl), "%s: skill %.2f < %.2f\n", smdl, skl.(smdl), skl0) ;
	 warning("off", sprintf("skl:%s", smdl)) ;
	 continue ;
      endif

      if strcmp(sim, "ana")
	 if VERBOSE
	    printf(["%12s%10s\n"], sim, smdl) ;
	 endif
	 [s.id s.x] = annstat(mdl.id, mdl.x, @nanmean, ny) ;
	 ## trends
	 for jC = 1 : length(pdd.uc) ;
	    T.(pdd.uc{jC}).(sim).(smdl) = [ones(rows(s.x),1) s.id(:,1)] ;
	    b.(pdd.uc{jC}).(sim).(smdl) = regress(s.x(:,jC), [ones(rows(s.x),1) s.id(:,1)]) ;
	 endfor

      else

	 for [gcm sgcm] = mdl
	    for [rea srea] = gcm
	       if VERBOSE
		  printf(["%12s%10s%15s%10s\n"], sim, smdl, sgcm, srea) ;
	       endif
	       [s.id s.x] = annstat(rea.id, rea.x, @nanmean, ny) ;
	       if any(isnan(s.x(:))) break ; endif

	       ## trends
	       for jC = 1 : length(pdd.uc) ;
		  T.(pdd.uc{jC}).(sim).(smdl).(sgcm).(srea) = [ones(rows(s.x),1) s.id(:,1)] ;
		  b.(pdd.uc{jC}).(sim).(smdl).(sgcm).(srea) = regress(s.x(:,jC), [ones(rows(s.x),1) s.id(:,1)]) ;
		  sd.(pdd.uc{jC}).(sim).(smdl).(sgcm).(srea) = std(s.x(:,jC)) ;
		  ##		  continue ;
		  ##		  return
		  I = ~any(isnan(s.x), 2) ;
		  g1 = repmat({sprintf("%s", sim)}, sum(I), 1) ;
		  g2 = repmat({sprintf("%s", smdl)}, sum(I), 1) ;
		  g3 = repmat({sprintf("%s", sgcm)}, sum(I), 1) ;
		  g4 = repmat({sprintf("%s", srea)}, sum(I), 1) ;
		  g5 = repmat({sprintf("%s", pdd.uc{jC})}, sum(I), 1) ;
		  g6 = arrayfun(@(yy) sprintf("%4d", yy), s.id(I,1), "UniformOutput", false) ;
		  G = [G ; [g1 g2 g3 g4 g5 g6]] ;
		  X = [X ; s.x(I,jC)] ;
		  GT = [GT ; {sim smdl sgcm srea pdd.uc{jC}}] ;
		  Y = [Y ; [b.(pdd.uc{jC}).(sim).(smdl).(sgcm).(srea)(2) sd.(pdd.uc{jC}).(sim).(smdl).(sgcm).(srea)]] ;
	       endfor
	    endfor
	 endfor
      endif
      [~, msgid] = lastwarn() ;
   endfor
endfor
warning(wrn) ;

JG = logical([1 1 1 1 1 1]) ; IG = sprintf("%d", JG) ;
if exist(f = sprintf("data/%s.%02d/anova.%s.%s.%s.ob", EXP, NH, IND, IG, LNAME), "file") == 2
   load(f) ;
else
   M = union(MDL, NET, "stable") ;
   M = M(~cellfun(@(smdl) skl.(m2s(smdl, 0)) < skl0, M)) ;
   I = @(g) ismember(g(:,2), M) ;
   save -mat foo.mat M X G JG GN GT Y
   [P, ATAB, STATS] = anovan (X(I(G),:), G(I(G),JG), "model", [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1], "varnames", GN(JG), "display", "on") ;
   [P, ATAB, STATS] = anovan (Y(I(GT),1)./Y(I(GT),2), GT(I(GT),:), "model", [1 0 0 0 0; 0 1 0 0 0; 0 0 1 0 0; 0 0 0 1 0; 0 0 0 0 1], "varnames", GN(JG(1:end-1)), "display", "on", "sstype", 1) ;
   printf("--> %s\n", f) ;
   save(f, "P", "ATAB", "STATS") ;
endif
xlswrite("nc/anova.xlsx", ATAB) ;
DF = dataframe(ATAB) ;
clf ;
bar(cell2mat(ATAB(2:end-2,6)), "facecolor", 0.3*[1 1 1]) ;
set(gca, "xticklabel", ATAB(2:end-2,:), "box", "off")
ylabel("F ratio") ;
title("ANOVA centennial CatRaRE trends") ;
set(findobj("-property", "fontsize"), "fontsize", 20) ;
hgsave(sprintf("nc/%s.%02d/anova.%s.og", EXP, NH, IND)) ;
print(sprintf("nc/%s.%02d/anova.%s.svg", EXP, NH, IND)) ;


JG = [2 3] ; j = 0 ; clear P ATAB STATS
for y = unique(G(:,5))'
   I = cell2mat(cellfun(@(yy) strcmp(yy, y), G(:,5), "UniformOutput", false)) ;
   if ~any(I) continue ; endif
   j++ ;
   [P(j,:), ATAB(j,:,:), STATS(j)] = anovan (X(I,1), G(I,JG), "model", "linear", "varnames", GN(JG), "display", "off") ;
endfor

reset(0) ;
set(0, "defaultfigureunits", "normalized", "defaultfigurepapertype", "A4") ;
set(0, "defaultfigurepaperunits", "normalized", "defaultfigurepaperpositionmode", "auto") ;
set(0, "defaultfigureposition", [0.7 0.7 0.3 0.3]) ;
set(0, "defaultaxesgridalpha", 0.3)
set(0, "defaultaxesfontname", "Libertinus Sans", "defaulttextfontname", "Libertinus Sans") ;

set(0, "defaultaxesfontsize", 18, "defaulttextfontsize", 18, "defaultlinelinewidth", 2) ;


addpath ~/oct/nc/maxdistcolor
JMDL = 1 : 4 ;
colS = maxdistcolor(length(JMDL), @(m) sRGB_to_OSAUCS (m, true, true)) ;
colD = maxdistcolor(length(NET), @(m) sRGB_to_OSAUCS (m, true, true)) ;
m2s = @(mdl, k) strrep(strrep(mdl, "-", "_"), "_", [repmat("\\", 1, k), "_"]) ;
##col = col(randperm(ncol),:) ;

pfx = @(net) sprintf("models/%s/%s.%02d/%s.%s.%s", net, EXP, NH, net, ind, LNAME) ;

alpha = 0.05 ;
RCOL = [0.2 0.2 0.7 ; 0.2 0.7 0.2 ; 0.7 0.2 0.2 ; 0.7*[1 1 1] ; 0.3*[1 1 1]] ;
SCOL = [0*[1 1 1] ; 0.2 0.2 0.7 ; 0.2 0.7 0.2 ; 1 0.7 0 ; [0.8 0.1 0.1]] ;
jC = 2 ; RGN = pdd.uc(jC) ;
pdd.lc = c2l(pdd.c) ; qEta = mean(pdd.lc) ;

## FAR
clf ; j = 0 ; yn = Inf ; yx = -Inf ;
for jC = [4 3 6 5]
   ax(++j) = subplot(2, 2, j) ; hold on
   set(gca, "ColorOrder", SCOL(2:5,:)) ;
   for jSIM = 4
      sim = SIM{jSIM} ;
      bm = mean(cell2mat(structflat(b.(pdd.uc{jC}).(sim)))(2,:)) ;
      far = 1 - qEta(jC) ./ (qEta(jC) + (0:10:80) * bm) ;
      plot(2015:10:2095, 100 * far) ;
      xlabel("year") ; ylabel("FAR  [%]") ;
      title(sprintf("%s", pdd.uc{jC})) ;
   endfor
   yn = min(yn, ylim()(1)) ;
   yx = max(yx, ylim()(2)) ;
endfor
set(ax, "xlim", [2015 2095], "ylim", [yn yx], "ygrid", "on", "ytick", [0 4 8 12])
set(findobj("-property", "fontsize"), "fontsize", 16) ;
hgsave(sprintf("nc/%s.%02d/far_rgn.%s.og", EXP, NH, IND)) ;
print(sprintf("nc/%s.%02d/far_rgn.%s.svg", EXP, NH, IND)) ;

## time series plot
clf ;
plot(randn(10, 1))
xlabel("time") ; ylabel("spatially mean prob") ;
print(sprintf("nc/ts.svg", EXP, NH, IND)) ;

clf ; jp = 0 ; yx = -Inf ;
for jj = [5 2 4 3]
   ax(++jp) = subplot(2, 2, jp) ;
   I = @(g) ismember(g(:,2), M) & ismember(g(:,5), pdd.oc{jj}) ;
   [P, ATAB, STATS] = anovan (Y(I(GT)), GT(I(GT),1:end-1), "model", [1 0 0 0; 0 1 0 0; 0 0 1 0; 0 0 0 1], "varnames", GN(JG(1:end-2)), "display", "off", "sstype", 1) ;
   bar(cell2mat(ATAB(2:end-2,6)), "facecolor", 0.3*[1 1 1]) ;
   set(gca, "xticklabel", ATAB(2:end-2,:), "box", "off")
   ylabel("F ratio") ;
   title(sprintf("%s", pdd.oc{jj})) ;
   yx = max([cell2mat(ATAB(2:end-2,6)) ; yx]) ;
endfor
set(ax, "ylim", [0 yx]) ;
set(findobj("-property", "fontsize"), "fontsize", 16) ;
hgsave(sprintf("nc/%s.%02d/anova_rgn.%s.og", EXP, NH, IND)) ;
print(sprintf("nc/%s.%02d/anova_rgn.%s.svg", EXP, NH, IND)) ;


## trend distribution
TSIM = SIM ; TSIM(1) = "ERA5" ;
clf ; hold on ; j = 0 ;
for jC = [1]
   j++ ;
   hold on ;
   set(gca, "ColorOrder", SCOL(2:5,:)) ;
   sim = "ana" ;
   [nn.(sim) xx.(sim)] = hist(cell2mat(structflat(b.(pdd.oc{jC}).(sim)))(2,:), 50) ;
   I = nn.(sim) > 0 ;
   w = mean(xx.(sim)(I)) * 100 * 100 ; # centennial trend in percentage
   plot([w w], [0 1]) ;
   for jSIM = JSIM(2:end)
      sim = SIM{jSIM} ;
      [nn.(sim) xx.(sim)] = hist(cell2mat(structflat(b.(pdd.oc{jC}).(sim)))(2,:), 50) ;
      x = xx.(sim) * 100 * 100 ;
      n = cumsum(nn.(sim) / sum(nn.(sim))) ;
      plot(x, n) ;
   endfor
   grid on
   xlabel("centennial trend CatRaRE probability  [%]") ; ylabel("CMIP6/DS range") ;
   if jC == 1
      xlim([-20 20]) ;
   else
      xlim([-10 10]) ;
   endif
   ylim([0 1]) ;
   legend(toupper(TSIM), "location", "southeast", "box", "off") ;
   xt = mean(xlim) - 6 ;
   text(xt, 0.9, sprintf("base rate p_0 = %.0f %%", 100*pdd.qc(p)(jC))) ;
   title(sprintf("class: %s", pdd.oc{jC})) ;
endfor
hgsave(sprintf("nc/%s.%02d/trends_%s.%02d.%s.og", EXP, NH, IND, 100*Q0, pdd.oc{jC})) ;
print(sprintf("nc/%s.%02d/trends_%s.%02d.%s.svg", EXP, NH, IND, 100*Q0, pdd.oc{jC})) ;

clf ; hold on ; j = 0 ;
for jC = [5 2 4 3]
   j++ ;
   ax(j) = subplot(2, 2, j) ; hold on ;
   set(gca, "ColorOrder", SCOL(2:5,:)) ;
   sim = "ana" ;
   [nn.(sim) xx.(sim)] = hist(cell2mat(structflat(b.(pdd.oc{jC}).(sim)))(2,:), 50) ;
   I = nn.(sim) > 0 ;
   w = mean(xx.(sim)(I)) * 100 * 100 ; # centennial trend in percentage
   plot([w w], [0 1]) ;
   for jSIM = JSIM(2:end)
      sim = SIM{jSIM} ;
      [nn.(sim) xx.(sim)] = hist(cell2mat(structflat(b.(pdd.oc{jC}).(sim)))(2,:), 50) ;
      x = xx.(sim) * 100 * 100 ;
      n = cumsum(nn.(sim) / sum(nn.(sim))) ;
      plot(x, n) ;
   endfor
   grid on
   xlim([-5 5]) ; ylim([0 1]) ;
   text(0.9*xlim()(1), 0.9, sprintf("p_0 = %.0f %%", 100*pdd.qc(p)(jC))) ;
   title(sprintf("class: %s", pdd.oc{jC})) ;
endfor
hgsave(sprintf("nc/%s.%02d/trends_%s.4R..og", EXP, NH, IND)) ;
print(sprintf("nc/%s.%02d/trends_%s.4R.svg", EXP, NH, IND)) ;
