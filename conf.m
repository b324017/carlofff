
[glat glon] = borders("germany") ;
BLD = ~true ; MAXX = 200 ; PARALLEL = true ;
EXP = {"1R" "NS" "EW" "4R" "SW" "DE"}{4} ;
QRES = 1.5 ;
if QRES > 1.5
   SQRES = sprintf(".%.1f", QRES) ;
else
   SQRES = "" ;
endif
if strcmp(EXP, "DE")
   GLON = [5.75 15.25] ; GLAT = [47.25 55.25] ;
   NLON = 39 ; NLAT = 33 ;
   JVAR = [1 5 6] ; # read these atm. indices
   NH = 6 ; # relevant hours
   ID = [2000 5 1 0 ; 2021 8 31 23] ;
else
   GLON = [min(glon(:)) max(glon(:))] ; GLAT = [min(glat(:)) max(glat(:))] ;
   NLON = 10 ; NLAT = 10 ; NLEV = 1 ;
   JVAR = [1 2 5] ; # read these atm. indices
   NH = 6 ; # relevant hours
   ID = [1979 5 1 0 ; 2022 8 31 23] ;
endif
GLEV = [0.9 1.0] ;
REG = reg(EXP, GLON, GLAT) ;
MON = 5 : 8 ;
VAR = trl_atm("atmvar.lst", 1) ;
IND = sprintf("%d", ind2log(JVAR, numel(VAR))) ;
VAR = VAR(JVAR) ;
FILL = true ;
if isempty(CNVDUR = getenv("CNVDUR"))
   CNVDUR = 9 ;
else
   CNVDUR = str2num(CNVDUR) ;
endif
SOLV = getenv("SOLV") ;
Q0 = 0.9 ;
IMB = {"SMOTE" "NONE" ""}{3} ;
if isempty(IMB)
   sIMB = "" ;
else
   sIMB = "_ovs" ;
endif
SKL = {"HSS" "ETS" "BSS"} ; jSKL = 2 ; # ETS

jPDD = 6 ;
PDD = {"WEI" "xWEI" "cape" "cp" "regnie" "CatRaRE"}{jPDD} ;
LNAME = sprintf("%s_%02d_%02.0f", PDD, CNVDUR, 100 * Q0) ;
jVAR = {1 2 0 0 0 3}{jPDD} ;

[~] = mkdir(sprintf("data/%s.%02d", EXP, NH)) ;

MDL = {"lasso" "tree" "nnet" "nls"} ;
NET = {"Simple" "ResNet" "LeNet-5" "CIFAR-10" "ALL-CNN" "DenseNet" "GoogLeNet" "AlexNet"} ;
RES = {[32 32] [32 32] [28 28] [32 32] [32 32] [32 32] [224 224] [227 227]} ;

JSIM = 1:4 ;
SIM = {"ana" "historical" "ssp126" "ssp585"}(JSIM) ;
REF = {"ana" "historical" "historical" "historical"}(JSIM) ;
NSIM = {"ANA" "HIST" "SSP126" "SSP585"}(JSIM) ;
DS = {[1979 5 1 0] [1960 5 1 0] [2015 5 1 0] [2015 5 1 0]} ;
DE = {[2022 8 31 23] [2014 8 31 23] [2100 8 31 23] [2100 8 31 23]} ;
CS = [1981 5 1 0] ; CE = [2010 8 31 23] ;
