
addpath ~/carlofff/fun

[~, ~] = mkdir("data") ;
cd ~/carlofff
A = argv ;

global isoctave JVAR EXP REG NH MON IMB CNVDUR BLD VERBOSE PARALLEL MAXX CALENDAR RCNT
if ~isempty(A) && isnewer(cfile = sprintf("tmp/tmp.%s/conf.m", A{1}))
   source(cfile) ;
else
   source conf.m
endif
glon = linspace(GLON(1), GLON(2), NLON) ;
glat = linspace(GLAT(1), GLAT(2), NLAT) ;
if isempty(GLOG_log_dir = getenv("GLOG_log_dir"))
   mkdir(GLOG_log_dir = sprintf("/tmp/GLOG.%d", getpid)) ;
   setenv("GLOG_log_dir", GLOG_log_dir) ;
endif

##{
isoctave = @() exist("OCTAVE_VERSION","builtin") ~= 0 ;
if isoctave()
   pkg load statistics
   source("caffe_loc.m") ; # LOC = "/path/to/caffe"
   addpath(sprintf("%s/matlab", LOC)) ;
   addpath(sprintf("%s/matlab/+caffe/private", LOC)) ;
   [st out] = system(sprintf("%s/bin/caffe device_query -gpu 0 2>&1", LOC)) ;
   if st == 0
      printf("+++\t\tCaffe using GPU\t\t+++\n") ;
      if exist("gpu_init", "var") == 0
	 caffe.set_mode_gpu() ;
	 gpu_init = true ;
      endif
      caffe.set_device(0) ;
   else
      printf("+++\t\tCaffe using CPU\t\t+++\n") ;
      caffe.set_mode_cpu() ;
   endif
else
   addpath /opt/caffeML/matlab
endif
[~, ~] = mkdir(sprintf("data/%s.%02d/prob", EXP, NH)) ; [~, ~] = mkdir(sprintf("nc/%s.%02d", EXP, NH)) ;

if isnewer(ofile = sprintf("data/era5/%02dx%02d/ptr_%02d.ob", NLON, NLAT, NH), glob("data/era5/ptr.*.ob"))
   printf("<-- %s\n", ofile) ;
   load(ofile) ;
else
   era5 = read_ana("data/era5", glon, glat, ID(1,1):ID(end,1)) ;
   printf("--> %s\n", ofile) ;
   save(ofile, "era5")
endif

if isnewer(pdfile = sprintf("data/%s.%02d/%s.ob", EXP, NH, LNAME))
   printf("<-- %s\n", pdfile) ;
   load(pdfile) ;
else
   ## select predictand
   jV = find(strcmp(PDD, VAR)) ; if isempty(jV) jV = 1 ; endif
   str = sprintf("%s,", VAR{:}) ; str = str(1:end-1) ;
   pdd = selpdd(PDD, glon, glat, jVAR, Q0) ;
   ## aggregate
   pdd = agg(pdd, NH, @nanmax) ;
   ## select classes
   [pdd.c pdd.uc pdd.qc] = classes(pdd, jVAR) ;
   pdd.lname = LNAME ;

   printf("--> %s\n", pdfile) ;
   save(pdfile, "pdd") ;
endif

if ~isnewer(cfile = sprintf("canon_corr.%02dx%02d.txt", NLON, NLAT), ofile, pdfile)
   cc = canon_corr(era5, pdd, jVAR) ;
   IC = cell2mat(cellfun(@(c) strcmp(fieldnames (cc)', c), fieldnames (cc), "UniformOutput", false)) ;
   cc = structflat(cc) ;
   dlmwrite(cfile, [diag(IC) cc]) ;
endif

if isnewer(ptfile = sprintf("data/ptr_%02d.%s.ob", NH, IND), ofile)

   printf("<-- %s\n", ptfile) ;
   load(ptfile) ;
   ptr.ofile = ofile ;
   if ~isfield(ptr, "ofile")
      ptr.ofile = ofile ;
      printf("--> %s\n", ptfile) ;
      save(ptfile, "ptr") ;
   endif

else

   ## select predictors
   str = sprintf("era5.%s,", VAR{:}) ; str = str(1:end-1) ;
   eval(sprintf("ptr = selptr(:, FILL, %s) ;", str))
   ## aggregate
   ptr = agg(ptr, NH, @nanmax) ;

   ptr.ind = IND ;
   ptr.name = "ptr" ;
   ptr.ofile = ofile ;

   ## normalize with mean and std
   ptr = nrm_ptr(ptr) ;

   printf("--> %s\n", ptfile) ;
   save(ptfile, "ptr") ;
   
endif

%% write train (CAL) & test (VAL) data
ptr.YCAL = [2001 5 1 0 ; 2011 8 31 18] ;
ptr.YVAL = [2012 5 1 0 ; 2022 8 31 18] ;
##ptr.YCAL = ptr.YVAL = [2001 5 1 0 ; 2020 8 31 23] ;

if 0
   ## test with reduced major class
   [rptr rpdd] = redclass(ptr, pdd, 0.8) ;
endif

profile off

## Shallow
clear skl ;
for jMDL = 1 : length(MDL)
   mdl = MDL{jMDL} ;
   if isnewer(sfile = sprintf("data/%s.%02d/Shallow.%s.%s.%s.ob", EXP, NH, [mdl sIMB], ptr.ind, LNAME), ptfile, pdfile)
      printf("<-- %s\n", sfile) ;
      load(sfile) ;
      if 0
	 strucdisp(shallow.skl) ;
	 plot_fit(mdl, shallow.fit) ;
	 set(findall("-property", "fontname"), "fontname", "Libertinus Sans") ;
	 set(findall("type", "axes"), "fontsize", 24) ;
	 set(findall("type", "text"), "fontsize", 22) ;
      endif
   else
      shallow = Shallow(ptr, pdd, :, "CVE", mdl, SKL) ;
      printf("--> %s\n", sfile) ;
      save(sfile, "shallow") ;
   endif
   skl.CAL(jMDL,:) = [shallow.pc.CAL shallow.rpss.CAL cellfun(@(s) mean(arrayfun(@(v) v.(s), shallow.moc.CAL)), SKL)] ;
   skl.VAL(jMDL,:) = [shallow.pc.VAL shallow.rpss.VAL cellfun(@(s) mean(arrayfun(@(v) v.(s), shallow.moc.VAL)), SKL)] ;
endfor
printf("--> %s\n", mfile = sprintf("nc/%s.%02d/skl.Shallow%s.%s.%s.ot", EXP, NH, sIMB, ptr.ind, LNAME)) ;
save("-text", mfile, "skl") ;

## Deep
for jNET = 1 : length(NET)

   net = NET{jNET} ; res = RES{jNET} ;
   [~, ~] = mkdir(sfx = sprintf("data/%s.%02d/%dx%d", EXP, NH, res)) ;
   dfile = sprintf("%s/Deep.%s.%s.%s.ob", sfx, [net sIMB], IND, LNAME) ;
   mfile = sprintf("nc/%s.%02d/skl.%s.%s.%s.ot", EXP, NH, [net sIMB], IND, LNAME) ;

   if isnewer(dfile, ptfile, pdfile, glob(sprintf("models/%s/*.tpl", net)){:})

      printf("<-- %s\n", dfile) ;
      load(dfile) ;
      if exist(mfile, "file") ~= 2 continue ; endif
      printf("<-- %s\n", mfile) ;
      load(mfile) ;
      
   else

      init_rnd() ;
      pfx = sprintf("%s/%s.%s.%s", sfx, [net sIMB], ptr.ind, LNAME) ;
      switch SOLV
	 case ""
	    solverstate = sprintf("%s_iter_0.solverstate", pfx) ;
	 case "netonly"
	    solverstate = sprintf("%s.netonly", pfx) ;
	    [deep weights] = Deep(ptr, pdd, res, solverstate, SKL) ;
	    continue ;
	 case "cont"
	    siter = table_pick(sprintf("%s_solver.prototxt", pfx), "max_iter") ;
	    solverstate = sprintf("%s_iter_%s.solverstate", pfx, siter) ;
	 otherwise
	    if ~strcmp(strsplit(SOLV, "/"){2}, net)
	       warning("SOLV not matching %s, continuing\n", net) ;
	       continue ;
	    endif
	    solverstate = upd_solver(SOLV, ptr.ind, LNAME) ;
      endswitch
      clear skl deep ; i = 0 ;
      while i < 20   ## UGLY
	 if isnewer(sfile = sprintf("%s/skl.%s.%s.%s.ot", sfx, [net sIMB], IND, LNAME)) && i < 2
	    load(sfile) ;
	    i = rows(skl.CAL) ;
	    printf("%d: <-- %s\n", i, sfile) ;
	 endif

	 kfail = 0 ; wskl = 0 ;
	 while ++kfail <= 10 && wskl <= 0
	    [deep weights] = Deep(ptr, pdd, res, solverstate, SKL) ;
	    wskl = deep.pc.VAL ;
	 endwhile
	 if kfail > 10
	    warning("no convergence for %s: PC = %.2f\n", net, wskl) ;
	    break ;
	 endif

	 i++ ;
	 deep.prob.name = net ;
	 save(sprintf("%s.%02d", dfile, i), "deep") ;
	 printf("%s/caffe.INFO --> %s/%s.%s.%s.log.%02d\n", GLOG_log_dir, sfx, [net sIMB], IND, LNAME, i) ;
	 system(sprintf("cp -L %s/caffe.INFO %s/%s.%s.%s.log.%02d", GLOG_log_dir, sfx, [net sIMB], IND, LNAME, i)) ;
	 system(sprintf("cp /dev/null %s/caffe.INFO", GLOG_log_dir)) ;
	 state = strrep(weights, ".caffemodel", ".solverstate") ;
	 rename(weights, sprintf("%s.%02d", weights, i)) ;
	 rename(state, sprintf("%s.%02d", state, i)) ;
	 skl.CAL(i,:) = [deep.pc.CAL deep.rpss.CAL cellfun(@(s) mean(arrayfun(@(v) v.(s), deep.moc.CAL)), SKL)] ;
	 skl.VAL(i,:) = [deep.pc.VAL deep.rpss.VAL cellfun(@(s) mean(arrayfun(@(v) v.(s), deep.moc.VAL)), SKL)] ;
	 save("-text", sfile, "skl") ;
##	 system(sprintf("nvidia-smi -f nvidia.%d.log", i)) ;
      endwhile
      if kfail > 10
	 state = strrep(weights, ".caffemodel", ".solverstate") ;
	 delete(weights, state) ;
	 continue ;
      endif

      printf("--> %s\n", mfile) ;
      copyfile(sfile, mfile) ; unlink(sfile) ;
      pause(1) ;    # for timestamp comparison
##      plot_log("/tmp/caffe.INFO", :, iter = 0, pse = 30, plog = 0) ;
##      cmd = sprintf("python /opt/src/caffe/python/draw_net.py models/%s/%s.prototxt nc/%s.svg", net, LNAME, net) ;
##      system(cmd) ;

   endif

   ## find best model and apply
   [~, i] = max(skl.VAL(:,2+jSKL)) ;

   pfx = sprintf("%s/%s.%s.%s", sfx, [net sIMB], IND, LNAME) ;
   difile = sprintf("%s.%02d", dfile, i) ;
   if isnewer(difile) && ~isnewer(dfile, difile)
      printf("%s --> %s\n", difile, dfile) ;
      rename(difile, dfile) ;
      delete(ls("-1t", sprintf("%s.*", dfile))) ;
      wfile = strtrim(ls("-1t", sprintf("%s_iter_*.caffemodel.%02d", pfx, i))(1,:)) ;
      rename(wfile, wfile(1:end-3)) ;
      delete(ls("-1t", sprintf("%s_iter_*.caffemodel.*", pfx))) ;
      wfile = strtrim(ls("-1t", sprintf("%s_iter_*.solverstate.%02d", pfx, i))(1,:)) ;
      rename(wfile, wfile(1:end-3)) ;
      delete(ls("-1t", sprintf("%s_iter_*.solverstate.*", pfx))) ;
      wfile = sprintf("%s.log.%02d", pfx, i) ;
      rename(wfile, wfile(1:end-3)) ;
      delete(ls("-1t", sprintf("%s.log.*", pfx))) ;
   endif

endfor

profshow ;
profile off ;
T = profile("info") ;
save -text prof.ot T

for jSIM = 1 : length(SIM)

   sim = SIM{jSIM} ;

   if strcmp(sim, "ana")
      prfile = sprintf("data/%s.%02d/prob/%s.%s.%s.ob", EXP, NH, sim, IND, LNAME) ;
      pfile = sprintf("data/ptr.%s.ob", sim) ;
      glb_gcm = {} ;
   else
      prfile = sprintf("data/%s.%02d/prob/%s.%s.%s%s.ob", EXP, NH, sim, IND, LNAME, SQRES) ;
      pfile = sprintf("data/ptr.%s%s.ob", sim, SQRES) ;
      glb_gcm = sprintf("data/cmip6/*/%s*.ob", sim) ;
   endif
   pnfile = sprintf("data/ptr.%s.%s%s.ob", sim, IND, SQRES) ;

   glb = glob(sprintf("data/%s.%02d/Shallow.*.%s.%s.ob", EXP, NH, IND, LNAME)) ;
   glb = union(glb, glob(sprintf("models/*/%s.%02d/*.%s.%s_iter_*.caffemodel", EXP, NH, IND, LNAME))) ;
   glb = union(glb, glob(glb_gcm)) ;

   glb0 = glob(sprintf("data/%s.%02d/prob/%s.*.%s.%s%s.ob", EXP, NH, sim, IND, LNAME, SQRES)) ;

   if isnewer(prfile, glb{:}, glb0{:}, pnfile)
      continue ;
   endif

   ## load atmospheric variables

   if isnewer(pfile, glob(glb_gcm))

      printf("<-- %s\n", pfile) ;
      load(pfile) ;

   else

      [~, FLD] = trl_atm("atmvar.lst", [1 2 2 2](jSIM)) ;
      clear("ptr") ;
      for fld = unique(FLD, "stable")
	 fld = fld{:} ; jres = find(strcmp(fld, FLD)) ;
	 if strcmp(sim, "ana") ## FIXME using all fld at once
	    s = read_ana("data/era5", glon, glat, ID(1,1):ID(end,1), fld) ;
	 else
	    s = read_sim("data/cmip6", sim, fld, glon, glat, QRES) ;
	 endif
	 if isempty(s) continue ; endif
	 for [gcm sgcm] = s
	    if strcmp(sim, "ana")
	       eval(sprintf("ptr.%s = agg(s.%s, NH) ;", sgcm, sgcm)) ;
	    else
	       for [rea srea] = gcm
		  for [v sv] = rea
		     ## select time period and aggregate
		     CALENDAR = getcal(v.nc) ;
		     sp = selper(v, DS{jSIM}, DE{jSIM}) ;
		     printf("%15s, %s, %s (%d):\t%6d%6d%6d%6d\t%6d%6d%6d%6d\n", sgcm, srea, sv, length(sp.id), sp.id([1 end],:)') ;
		     sp = agg(sp, NH) ;
		     if nanmean(sp.x(:)) == 0
			s.(sgcm).(srea) = rmfield(s.(sgcm).(srea), sv) ;
			warning("%s, %s, %s, %s: no values\n", sim, sgcm, srea, sv) ;
			continue ;
		     endif
		     if datenum(sp.id(1,:)) > datenum(DS{jSIM}) + 1 || datenum(sp.id(end,:)) + 1 < datenum(DS{jSIM})
			error("incompatible dates for: %s\n\t%d %d %d %d\t%d %d %d %d\n", sv, sp.id([1 end],:)') ;
		     endif
		     [sp.id sp.x] = selmon(sp.id, sp.x) ;
		     if VERBOSE
			printf("%15s, %s, %s (%d):\t%6d%6d%6d%6d\t%6d%6d%6d%6d\n", sgcm, srea, sv, length(sp.id), sp.id([1 end],:)') ;
		     endif
		     ptr.(sgcm).(srea).(sv) = sp ;
		  endfor
	       endfor
	    endif
	 endfor
      endfor

      printf("--> %s\n", pfile) ;
      save(pfile, "ptr") ;

   endif

   ## select predictors
   LR = exist(rfile = sprintf("data/ref.%s.%s%s.ob", REF{jSIM}, IND, SQRES), "file") == 2 ;

   if isnewer(pnfile, pfile) & LR

      printf("<-- %s\n", pnfile) ;
      load(pnfile) ;

   else

      if strcmp(sim, "ana")
	 str = sprintf("ptr.%s,", VAR{:}) ; str = str(1:end-1) ;
	 eval(sprintf("ptr = selptr(:, FILL, %s) ;", str)) ;
	 eval(sprintf("ptr.name = \"%s\" ;", sim)) ;
      else
	 for [mdl smdl] = ptr
	    for [rea srea] = mdl
	       str = {} ;
	       for [v sv] = rea
		  if VERBOSE
		     printf("%15s, %s, %s (%d):\t%6d%6d%6d%6d\t%6d%6d%6d%6d\n", smdl, srea, sv, length(v.id), v.id([1 end],:)') ;
		  endif
		  str = {str{:} sprintf("ptr.%s.%s.%s,", smdl, srea, sv)} ;
	       endfor
	       str = strcat(str{:})(1:end-1) ;
	       eval(sprintf("ptr.%s.%s = selptr(:, FILL, %s) ;", smdl, srea, str)) ;
	       eval(sprintf("ptr.%s.%s.name = \"%s\" ;", smdl, srea, sim)) ;
	    endfor
	 endfor
      endif
      SVAR = trl_atm("atmvar.lst", [1 2 2 2](jSIM)) ;
      ptr = selfields(ptr, SVAR, IND) ;
      if isempty(ptr)
	 warning("%s: not enough fields for %s\n", sim, IND) ;
	 continue ;
      endif

      ## reference climate
      if strcmp(sim, REF{jSIM})
	 if isnewer(rfile, pfile)
	    printf("<-- %s\n", rfile) ;
	    load(rfile) ;
	 else
	    w = rstructfun(@ref_clim, ptr, CS, CE) ;
	    if isfield(w, "xm")
	       ref = [w.xm w.xs] ;
	    else
	       ref = structfun(@(s) mean(reshape(structflat(s), 3, 2, []), 3)(:), w, "UniformOutput", false) ;
	    endif
	    printf("--> %s\n", rfile) ;
	    save(rfile, "ref") ;
	 endif
      else
	 printf("<-- %s\n", rfile) ;
	 load(rfile) ;
      endif

      ## normalize ptr
      for [gcm sgcm] = ptr
	 if isstruct(ref) && ~isfield(ref, sgcm)
	    ptr = rmfield(ptr, sgcm) ;
	    warning("missing ref for %s\n", sgcm) ;
	    continue ;
	 endif
      endfor
      ptr = rstructfun(@nrm_ptr, ptr, ref) ;

      printf("--> %s\n", pnfile) ;
      save(pnfile, "ptr") ;

   endif

   clear prob ;

   for jMDL = 1 : length(MDL)
      mdl = MDL{jMDL} ;
      tfile = sprintf("data/%s.%02d/prob/%s.%s.%s.%s%s.ob", EXP, NH, sim, [mdl sIMB], IND, LNAME, SQRES) ;
      mfile = sprintf("data/%s.%02d/Shallow.%s.%s.%s.ob", EXP, NH, [mdl sIMB], IND, LNAME) ;
      if isnewer(tfile, pnfile, mfile)
	 printf("<-- %s\n", tfile) ;
	 load(tfile) ; prob.(mdl) = tprob ;
      else
	 load(mfile) ;
	 fun = @(s) struct("id", s.id, "x", Shallow(s, shallow, [], [], mdl)) ;
	 tprob = rstructfun(fun, ptr) ;
	 prob.(mdl) = tprob ;
	 printf("--> %s\n", tfile) ;
	 save(tfile, "tprob") ;
      endif
   endfor

   for jNET = 1 : length(NET)
      net = NET{jNET} ; snet = strrep(net, "-", "_") ; res = RES{jNET} ;
      pfx = sprintf("models/%s/%s.%02d/%s.%s.%s", net, EXP, NH, net, IND, LNAME) ;
      model = sprintf("%s_deploy.prototxt", pfx) ;
      if exist(model, "file") ~= 2
	 error("model not found: %s", model) ;
      endif
      siter = table_pick(sprintf("%s_solver.prototxt", pfx), "max_iter") ;
      weights = sprintf("%s_iter_%s.caffemodel", pfx, siter) ;
      if exist(weights, "file") ~= 2
	 warning("weights not found: %s\n", weights) ;
	 continue ;
      endif
      tfile = sprintf("data/%s.%02d/prob/%s.%s.%s.%s%s.ob", EXP, NH, sim, [net sIMB], IND, LNAME, SQRES) ;
      if isnewer(tfile, pnfile, model, weights)
	 printf("<-- %s\n", tfile) ;
	 load(tfile) ; prob.(snet) = tprob ;
      else
	 deploy = caffe.Net(model, weights, 'test') ;
	 fun = @(s) struct("id", s.id, "x", apply_net(s, deploy, res)) ;
	 prob.(snet) = tprob = rstructfun(fun, ptr) ;
	 printf("--> %s\n", tfile) ;
	 save(tfile, "tprob") ;
      endif
   endfor

   printf("--> %s\n", prfile) ;
   save(prfile, "prob") ;

endfor

if ~isempty(graphics_toolkit) && ~strcmp(graphics_toolkit, "gnuplot")
   source plots.m ;
endif


## recent events
RCNT.lbl = LBL = "South24" ;
MON = 5 : 6 ;
RCNT.D = D = [2024 5 1 ; 2024 6 7] ;
JMDL = 2 ; JNET = 4 ;
sim = "ana" ;
[~, ~] = mkdir(sprintf("nc/%s", LBL)) ;
[~, ~] = mkdir(sprintf("data/%s/pl/cnvenv", LBL)) ;
[~, ~] = mkdir(sprintf("data/%s/pl/vort", LBL)) ;
[~, ~] = mkdir(sprintf("data/%s/sl/water", LBL)) ;

if isnewer(ofile = sprintf("data/%s/ptr.ob", LBL), sprintf("data/%s", LBL))

   printf("<-- %s\n", ofile) ;
   load(ofile) ;

else

   SVAR = trl_atm("atmvar.lst", 1, JVAR) ;
   clear(SVAR{:}) ;
   for svar = SVAR
      svar = svar{:} ;
      glon = linspace(GLON(1), GLON(2), NLON) ;
      glat = linspace(GLAT(1), GLAT(2), NLAT) ;
      s = read_ana(sprintf("data/%s", LBL), glon, glat, D(1,1):D(end,1), svar) ;
      for [v sv] = s
	 ## aggregate
	 eval(sprintf("ana.%s = agg(s.%s, NH) ;", sv, sv)) ;
      endfor
   endfor

   printf("--> %s\n", ofile) ;
   save(ofile, "ana") ;

endif

## select predictors
str = {} ;
for [v k] = eval(sim)
   if strcmp(k, "prob") continue ; endif
   str = {str{:} sprintf("%s.%s,", sim, k)} ;
endfor
str = strcat(str{:})(1:end-1) ;
eval(sprintf("%s.prob = selptr(:, FILL, %s) ;", sim, str)) ;
eval(sprintf("%s.prob.name = \"%s\" ;", sim, sim)) ;

if isnewer(prfile = sprintf("data/%s/%s.%02d/%s.%s.%s.ob", LBL, EXP, NH, sim, IND, LNAME))

   printf("<-- %s\n", prfile) ;
   load(prfile) ;

else

   ## normalize with mean and std
   prob = nrm_ptr(prob) ;

   for jMDL = JMDL
      mdl = MDL{jMDL} ;
      sfile = sprintf("data/%s.%02d/Shallow.%s.%s.%s.ob", EXP, NH, [mdl sIMB], IND, LNAME) ;
      printf("<-- %s\n", sfile) ;
      load(sfile) ;
      ##	 shallow.fit.model = @(par, x, u) clprob(@m5ppredict_new, par, x, u) ; fmod(sfile, shallow) ;
      if ~isfield(shallow, "uc")
	 shallow.uc = shallow.fit.uc ; shallow.fit = rmfield(shallow.fit, "uc") ; fmod(sfile, shallow) ;
      endif
      eval(sprintf("%s.prob.Shallow.%s = Shallow(%s.prob, shallow, :, [], mdl) ;", sim, mdl, sim)) ;
   endfor
   figure(1, "position", [0.4 0.7 0.6 0.3])
   I = sdate(ana.prob.id, double(D(1,:)), double(D(2,:))) ;
   J = find(strcmp(cellstr(datestr(datenum(ana.prob.id(I,:)), "dddHH")), "Mon00")) ;
   d0 = datenum(2023, 12, 31, 23, 59, 59) ;
   xt = datenum(ana.prob.id(J,:)) - d0 ;
   xtl = datestr(datenum(ana.prob.id(I,:))(J), "mmm dd") ;
   clf ;
   h = bar(datenum(ana.prob.id(I,:)) - d0, ana.prob.Shallow.(mdl)(I,:), "stacked") ;
   set(gca, "xtick", xt, "xticklabel", xtl) ;
##   set (h, "edgecolor", 1 * [1 1 1])
   set (h(1), "facecolor", 0.7 * [1 1 1])
   axis tight
##   set(h, "barwidth", 0.7)
   xlabel("May/June 2024") ; ylabel("probability") ;
   title(sprintf("regional class probability (0,1,N,S) of convective CatRaRE-type events ≤ 9h\nMay/June 2024 Germany from shallow learning classifier %s", toupper([mdl sIMB])), "interpreter", "none") ;
   legend(pdd.uc, "location", "eastoutside", "box", "off")
   set(findobj("-property", "fontsize"), "fontsize", 16) ;
   hgsave(["nc/" LBL "/May_June_2024.og"]) ;
   print(["nc/" LBL "/May_June_2024.svg"]) ;
   print(["nc/" LBL "/May_June_2024.png"]) ;

   for jNET = JNET
      net = NET{jNET} ; res = RES{jNET} ;
      pfx = sprintf("models/%s/%s.%02d/%s.%s.%s", net, EXP, NH, net, IND, LNAME) ;
      model = sprintf("%s_deploy.prototxt", pfx) ;
      if ~isnewer(model)
	 error("model not found: %s", model) ;
      endif
      siter = table_pick(sprintf("%s_solver.prototxt", pfx), "max_iter") ;
      if 1
	 weights = sprintf("%s_iter_%s.caffemodel", pfx, siter) ;
      else
	 weights = sprintf("%s_iter_%s.caffemodel.17", pfx, siter) ;
      endif
      if ~isnewer(weights)
	 warning("weights not found: %s\n", weights) ;
	 continue ;
      endif
      deploy = caffe.Net(model, weights, 'test') ;
      out = sprintf("%s.prob.Deep.%s = apply_net(%s.prob, deploy, res) ;", sim, strrep(net, "-", "_"), sim) ;
      printf("%s\n", out) ;
      eval(out) ;
   endfor
   figure(1, "position", [0.4 0.7 0.6 0.3])
   I = sdate(ana.prob.id, double(D(1,:)), double(D(2,:))) ;
   J = find(strcmp(cellstr(datestr(datenum(ana.prob.id(I,:)), "dddHH")), "Mon00")) ;
   d0 = datenum(2023, 12, 31, 23, 59, 59) ;
   xt = datenum(ana.prob.id(J,:)) - d0 ;
   xtl = datestr(datenum(ana.prob.id(I,:))(J), "mmm dd") ;
   clf ;
   jNET = 7 ; net = NET{jNET} ;
   h = bar(datenum(ana.prob.id(I,:)) - d0, ana.prob.Deep.(strrep(net, "-", "_"))(I,:), "stacked") ;
   set(gca, "xtick", xt, "xticklabel", xtl) ;
##   set (h, "edgecolor", 1 * [1 1 1])
   set (h(1), "facecolor", 0.7 * [1 1 1])
   axis tight
##   set(h, "barwidth", 0.7)
   xlabel("May/June 2024") ; ylabel("probability") ;
   title(sprintf("regional class probability (0,1,N,S) of convective CatRaRE-type events ≤ 9h\nMay/June 2024 Germany from deep learning classifier %s", [net sIMB]), "interpreter", "none") ;
   legend(pdd.uc, "location", "eastoutside", "box", "off")
   set(findobj("-property", "fontsize"), "fontsize", 16) ;
   hgsave(["nc/" LBL "/May_June_2024.og"]) ;
   print(["nc/" LBL "/May_June_2024.svg"]) ;
   print("-r600", ["nc/" LBL "/May_June_2024.png"]) ;

   printf("--> %s\n", prfile) ;
   save(prfile, sim) ;

endif

load(["nc/" LBL "/May_June_2024.og"]) ;

## load atmospheric variables
if isnewer(sfile = sprintf("data/%s/%s.%s.ob", LBL, sim, ptr.ind))

   printf("<-- %s\n", sfile) ;
   load(sfile) ;

   else

      SVAR = trl_atm("atmvar.lst", [1 2 2 2](jSIM), JVAR) ;
      clear(SVAR{:}) ;
      for svar = SVAR
	 svar = svar{:} ;
	 glon = linspace(GLON(1), GLON(2), NLON) ;
	 glat = linspace(GLAT(1), GLAT(2), NLAT) ;
	 s = read_ana("data/era5", glon, glat, ID(1,1):ID(end,1), svar) ;
	 for [v sv] = s
	    for [w m] = v
	       for [u r] = w
		  ## aggregate
		  eval(sprintf("%s.%s.%s.%s = agg(s.%s.%s.%s, NH) ;", sim, m, r, sv, sv, m, r)) ;
	       endfor
	    endfor
	 endfor
      endfor

      ## select predictors
      for [v k] = eval(sim)
	 for [w l] = v
	    str = {} ;
	    for [u m] = w
	       str = {str{:} sprintf("%s.%s.%s.%s,", sim, k, l, m)} ;
	    endfor
	    str = strcat(str{:})(1:end-1) ;
	    printf("%s\n", str), continue
	    eval(sprintf("%s.prob = selptr(:, FILL, %s) ;", sim, str)) ;
	    eval(sprintf("%s.prob.name = \"%s\" ;", sim, sim)) ;
	 endfor
      endfor

   endif
