load("data/IDX.ot") ;

if 0
   F = glob("data/cmip6/**/*.ob")' ;
   F = cellfun(@(c) strsplit(c, "/"){4}, F, "UniformOutput", false) ;
   GCM = cellfun(@(c) strsplit(c, "."){2}, F, "UniformOutput", false) ;
   REA = cellfun(@(c) strsplit(c, "."){3}, F, "UniformOutput", false) ;
   I = ~ismember(REA, {"ua" "va"}) ;
   GCM = unique(GCM(I)) ;
   REA = unique(REA(I)) ;
endif

Cidx = [1 1 0 0 0 0
	1 1 0 0 0 1
	1 1 0 0 1 0
	1 1 0 1 0 0
	1 1 1 0 0 0
	1 1 0 0 1 1
	1 1 0 1 0 1
	1 1 0 1 1 0
	1 1 1 0 0 1
	1 1 1 0 1 0
	1 1 1 1 0 0
	1 1 0 1 1 1
	1 1 1 0 1 1
	1 1 1 1 0 1
	1 1 1 1 1 0
	1 1 1 1 1 1
       ] ;

for i = 1 : size(Cidx, 1)
   for j = 1 : 3
      s(i,j) = sum(all(Cidx(i,:) <= IDX(:,:,j), 2)) ;
   endfor
endfor
