import os
from datapi import ApiClient
client = ApiClient()

client.check_authentication()

from datetime import date
from dateutil.rrule import rrule, DAILY
import os.path

def era5_cnvenv (lbl, y1, m1, d1, y2, m2, d2):

    a = date(y1, m1, d1)
    b = date(y2, m2, d2)

    for dt in rrule(DAILY, dtstart=a, until=b):
        y = dt.year
        m = dt.month
        d = dt.day

        odir = 'data/%s/pl/cnvenv' % (lbl)
        if not os.path.exists(odir):
            os.mkdir(odir)
        ofile = '%s/%04d-%02d-%02d.nc' % (odir,y,m,d)
        if not os.path.exists(ofile):

            collection_id = "reanalysis-era5-pressure-levels"
            collection = client.get_collection(collection_id)
            request = {
                'product_type': 'reanalysis',
                'variable': [
                    'specific_humidity', 'temperature',
                ],
                'pressure_level': [
                    '50', '70', '100',
                    '125', '150', '175',
                    '200', '225', '250',
                    '300', '350', '400',
                    '450', '500', '550',
                    '600', '650', '700',
                    '750', '775', '800',
                    '825', '850', '875',
                    '900', '925', '950',
                    '975', '1000',
                ],
                'year': '%d' % y,
                'month': '%02d' % m,
                'day': '%02d' % d,
                'time': [
                    '00:00', '01:00', '02:00',
                    '03:00', '04:00', '05:00',
                    '06:00', '07:00', '08:00',
                    '09:00', '10:00', '11:00',
                    '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00',
                    '18:00', '19:00', '20:00',
                    '21:00', '22:00', '23:00',
                ],
                'area': [
                    55.25, 5.75, 47.25,
                    15.25,
                ],
                "data_format": "netcdf",
            }
            collection.process.apply_constraints(**request)
            client.retrieve(collection_id, target=ofile, **request)  # blocks
            remote = client.submit(collection_id, **request)  # doesn't block
            remote.request_uid
            remote.status
            remote.download(ofile)  # blocks


def era5_rh (lbl, y1, m1, d1, y2, m2, d2):

    a = date(y1, m1, d1)
    b = date(y2, m2, d2)

    for dt in rrule(DAILY, dtstart=a, until=b):
        y = dt.year
        m = dt.month
        d = dt.day

        odir = 'data/%s/pl/rh' % (lbl)
        if not os.path.exists(odir):
            os.mkdir(odir)
        ofile = '%s/%04d-%02d-%02d.nc' % (odir,y,m,d)
        if not os.path.exists(ofile):

            collection_id = "reanalysis-era5-pressure-levels"
            collection = client.get_collection(collection_id)
#             collection.end_datetime
            request = {
                'product_type': 'reanalysis',
                'variable': 'relative_humidity',
                'pressure_level': [
                    '50', '70', '100',
                    '125', '150', '175',
                    '200', '225', '250',
                    '300', '350', '400',
                    '450', '500', '550',
                    '600', '650', '700',
                    '750', '775', '800',
                    '825', '850', '875',
                    '900', '925', '950',
                    '975', '1000',
                ],
                'year': '%d' % y,
                'month': '%02d' % m,
                'day': '%02d' % d,
                'time': [
                    '00:00', '01:00', '02:00',
                    '03:00', '04:00', '05:00',
                    '06:00', '07:00', '08:00',
                    '09:00', '10:00', '11:00',
                    '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00',
                    '18:00', '19:00', '20:00',
                    '21:00', '22:00', '23:00',
                ],
                'area': [
                    55.25, 5.75, 47.25,
                    15.25,
                ],
                "data_format": "netcdf",
            }
            collection.process.apply_constraints(**request)
            client.retrieve(collection_id, target=ofile, **request)  # blocks
            remote = client.submit(collection_id, **request)  # doesn't block
            remote.request_uid
            remote.status
            remote.download(ofile)  # blocks


def era5_wind (lbl, y1, m1, d1, y2, m2, d2):

    a = date(y1, m1, d1)
    b = date(y2, m2, d2)


    for dt in rrule(DAILY, dtstart=a, until=b):
        y = dt.year
        m = dt.month
        d = dt.day

        odir = 'data/%s/pl/wind' % (lbl)
        if not os.path.exists(odir):
            os.mkdir(odir)
        ofile = '%s/%04d-%02d-%02d.nc' % (odir,y,m,d)
        if not os.path.exists(ofile):

            collection_id = "reanalysis-era5-pressure-levels"
            collection = client.get_collection(collection_id)
            request = {
                'product_type': 'reanalysis',
                'variable': ['u_component_of_wind', 'v_component_of_wind'],
                'pressure_level': [
                    '50', '70', '100',
                    '125', '150', '175',
                    '200', '225', '250',
                    '300', '350', '400',
                    '450', '500', '550',
                    '600', '650', '700',
                    '750', '775', '800',
                    '825', '850', '875',
                    '900', '925', '950',
                    '975', '1000',
                ],
                'year': '%d' % y,
                'month': '%02d' % m,
                'day': '%02d' % d,
                'time': [
                    '00:00', '01:00', '02:00',
                    '03:00', '04:00', '05:00',
                    '06:00', '07:00', '08:00',
                    '09:00', '10:00', '11:00',
                    '12:00', '13:00', '14:00',
                    '15:00', '16:00', '17:00',
                    '18:00', '19:00', '20:00',
                    '21:00', '22:00', '23:00',
                ],
                'area': [
                    55.25, 5.75, 47.25,
                    15.25,
                ],
                'data_format': 'netcdf',
            }
            collection.process.apply_constraints(**request)
            client.retrieve(collection_id, target=ofile, **request)  # blocks
            remote = client.submit(collection_id, **request)  # doesn't block
            remote.request_uid
            remote.status
            remote.download(ofile)  # blocks


def era5_vort (lbl, y1, m1, d1, y2, m2, d2):

    a = date(y1, m1, d1)
    b = date(y2, m2, d2)

    c = cdsapi.Client()

    for dt in rrule(DAILY, dtstart=a, until=b):
        y = dt.year
        m = dt.month
        d = dt.day

        odir = 'data/%s/pl/vort' % (lbl)
        if not os.path.exists(odir):
            os.mkdir(odir)
        ofile = '%s/%04d-%02d-%02d.nc' % (odir,y,m,d)
        if not os.path.exists(ofile):

            c.retrieve(
                'reanalysis-era5-pressure-levels',
                {
                    'product_type': 'reanalysis',
                    'variable': ['divergence', 'vorticity'],
                    'pressure_level': ['500', '850', '1000'],
                    'year': '%d' % y,
                    'month': '%02d' % m,
                    'day': '%02d' % d,
                    'time': [
                        '00:00', '01:00', '02:00',
                        '03:00', '04:00', '05:00',
                        '06:00', '07:00', '08:00',
                        '09:00', '10:00', '11:00',
                        '12:00', '13:00', '14:00',
                        '15:00', '16:00', '17:00',
                        '18:00', '19:00', '20:00',
                        '21:00', '22:00', '23:00',
                    ],
                    'area': [55.25, 5.75, 47.25,15.25],
                    'format': 'netcdf',
                },
                ofile
            )


def era5_water (lbl, y1, m1, d1, y2, m2, d2):

    a = date(y1, m1, d1)
    b = date(y2, m2, d2)

    c = cdsapi.Client()

    for dt in rrule(DAILY, dtstart=a, until=b):
        y = dt.year
        m = dt.month
        d = dt.day

        odir = 'data/%s/pl/water' % (lbl)
        if not os.path.exists(odir):
            os.mkdir(odir)
        ofile = '%s/%04d-%02d-%02d.nc' % (odir,y,m,d)
        if not os.path.exists(ofile):

            c.retrieve(
                'reanalysis-era5-single-levels',
                {
                    'product_type': 'reanalysis',
                    'format': 'netcdf',
                    'variable': ['convective_precipitation', 'total_column_rain_water'],
                    'year': '%d' % y,
                    'month': '%02d' % m,
                    'day': '%02d' % d,
                    'time': [
                        '00:00', '01:00', '02:00',
                        '03:00', '04:00', '05:00',
                        '06:00', '07:00', '08:00',
                        '09:00', '10:00', '11:00',
                        '12:00', '13:00', '14:00',
                        '15:00', '16:00', '17:00',
                        '18:00', '19:00', '20:00',
                        '21:00', '22:00', '23:00',
                    ],
                    'area': [
                        55.25, 5.75, 47.25,
                        15.25,
                    ],
                },
                ofile
            )
