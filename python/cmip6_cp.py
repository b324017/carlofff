import sys
import cdsapi

c = cdsapi.Client()

out = 'data/cmip6/' + sys.argv[1] + '_cp.nc'

c.retrieve(
    'reanalysis-era5-single-levels',
    {
        'product_type': 'reanalysis',
        'format': 'netcdf',
        'variable': 'convective_precipitation',
        'year': sys.argv[1],
        'month': [
            '05', '06', '07',
            '08',
        ],
        'day': [
            '01', '02', '03',
            '04', '05', '06',
            '07', '08', '09',
            '10', '11', '12',
            '13', '14', '15',
            '16', '17', '18',
            '19', '20', '21',
            '22', '23', '24',
            '25', '26', '27',
            '28', '29', '30',
            '31',
        ],
        'time': [
            '12:00', '13:00', '14:00',
            '15:00', '16:00', '17:00',
            '18:00',
        ],
        'area': [
            55.25, 5.75, 47.25,
            15.25,
        ],
    },
    out)
