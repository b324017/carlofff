#include <octave/oct.h>
#include <octave/f77-fcn.h>

extern "C"
{
  F77_RET_T
  F77_FUNC (getcape, FORTSUB)
    (const F77_DBLE*, const F77_INT&, F77_DBLE*, F77_DBLE*, F77_DBLE*, F77_DBLE&, F77_DBLE&, F77_DBLE&, F77_DBLE&, F77_DBLE&, F77_DBLE&, F77_DBLE&, F77_DBLE&);
}

DEFUN_DLD (getcape, args, , "getcape")
{
  if (args.length () != 4)
    print_usage ();

  NDArray source_in = args(0).array_value ();
  NDArray p_in = args(1).array_value ();
  NDArray t_in = args(2).array_value ();
  NDArray q_in = args(3).array_value ();

  double *source_inv = source_in.fortran_vec ();
  double *p_inv = p_in.fortran_vec ();
  double *t_inv = t_in.fortran_vec ();
  double *q_inv = q_in.fortran_vec ();

  double cape;
  double cin;
  double zlcl;
  double zlfc;
  double zel;
  double psource;
  double tsource;
  double qvsource;

  octave_idx_type n = p_in.numel ();

  F77_XFCN (getcape, FORTSUB,
    (source_inv, n, p_inv, t_inv, q_inv, cape, cin, zlcl, zlfc, zel, psource, tsource, qvsource));

  return ovl (cape, cin);
}
