
if exist("EXP", "var") ~= 1
   addpath ~/carlofff/fun
   source conf.m ;
endif
global COL

### plots

close all ; reset(0) ;
set(0, "defaultfigureunits", "normalized", "defaultfigurepapertype", "A4") ;
set(0, "defaultfigurepaperunits", "normalized", "defaultfigurepaperpositionmode", "auto") ;
set(0, "defaultfigureposition", [0.7 0.7 0.3 0.3]) ;
set(0, "defaultaxesgridalpha", 0.3)
set(0, "defaultaxesfontname", "Libertinus Sans", "defaulttextfontname", "Libertinus Sans") ;
set(0, "defaultaxesfontsize", 18, "defaulttextfontsize", 18, "defaultlinelinewidth", 2) ;

addpath ~/oct/nc/maxdistcolor
JMDL = 1 : 4 ;
colS = maxdistcolor(length(JMDL), @(m) sRGB_to_OSAUCS (m, true, true)) ;
colD = maxdistcolor(length(NET), @(m) sRGB_to_OSAUCS (m, true, true)) ;
m2s = @(mdl, k) strrep(strrep(mdl, "-", "_"), "_", [repmat("\\", 1, k), "_"]) ;
m2si = @(mdl, k) strrep(strrep(mdl, "_", "-"), "-", [repmat("\\", 1, k), "-"]) ;
##col = col(randperm(ncol),:) ;

pfx = @(net) sprintf("models/%s/%s.%02d/%s.%s.%s", net, EXP, NH, net, ind, LNAME) ;

alpha = 0.05 ;
load(sprintf("data/%s.%02d/%s.ob", EXP, NH, LNAME)) ;
jC = 3 ; pdd.lc = c2l(pdd.c)(:,jC) ; RGN = pdd.uc{jC} ;
qEta = mean(pdd.lc) ;

## predictor correlations
FLD = trl_atm("atmvar.lst", 1) ;
r0 = dlmread("canon_corr.39x33.txt") ;
r1 = dlmread("canon_corr.10x10.txt") ;
s = textscan(fid = fopen("canon_corr.txt"), "%s %f %s %f") ; fclose(fid) ;
ind0 = s{1} ; r0 = s{2} ; ind0 = s{3} ; r1 = s{4} ;
hb = bar([r1 r0]) ;
set(hb(1), "facecolor", 0.4*[1 1 1], "edgecolor", 0.4*[1 1 1]) ;
set(hb(2), "facecolor", 0.7*[1 1 1], "edgecolor", 0.7*[1 1 1]) ;
set(gca, "XTicklabel", FLD, "ygrid", "on") ;
ylabel("canon. correl.") ;
legend({"39x33" "10x10"}) ;
title(sprintf("single predictor quality\n(rel. to grid size)")) ;
hgsave(sprintf("nc/cancorr.og")) ;
print(sprintf("nc/cancorr.svg")) ;


## region geometry
figure(1, "position", [0.6 0.3 0.4 0.7]) ; clf
hb = borders("germany", "color", "black") ;
l1 = line([REG.geo.NW(1,1) REG.geo.NE(1,2)], [REG.geo.NE(2,[1 1])]) ;
l2 = line(REG.geo.SW(1,[2 2]), [REG.geo.SW(2,1) REG.geo.NE(2,2)]) ;
hgsave(sprintf("nc/4R-classes.og")) ;
print(sprintf("nc/4R-classes.svg")) ;
### cases
jNET = 7 ; IND = "110000" ; NH = 24 ;
net = NET{jNET} ; sfx = sprintf("data/%s.%02d/%dx%d", EXP, NH, RES{jNET}) ;
load("data/ind/ind.ob") ;
cape_r = regrid(cape, glon, glat) ;
load(sprintf("models/%s/%s.%02d/Deep.%s.%s.%s.ob", net, "1R", NH, net, IND, LNAME)) ;

## July 2014
D = [2014 7 15 ; 2014 8 15] ; d = [2014 7 28] ;
ds = datestr(datenum(d), "yyyy-mm") ;
clf ; jVAR = 3 ; cx = 1.5 ;
figure(1, "position", [0.6 0.7 0.4 0.3]) ;

ax = subplot(1, 2, 1) ;
[h hc hb] = plot_fld(cape, d, [0 600]) ;
I = sdate(pdd.ts.id, d) ;
if any(I)
   x = pdd.ts.x(I,jVAR) ;
   h = scatter(pdd.ts.lon(I), pdd.ts.lat(I), x, "r", "o", "filled") ;
   [xn in] = min(x) ; [xx ix] = max(x) ;
   hn = scatter(pdd.ts.lon(I)(in), pdd.ts.lat(I)(in), cx*x(in), "r", "o", "filled") ;
   hx = scatter(pdd.ts.lon(I)(ix), pdd.ts.lat(I)(ix), cx*x(ix), "r", "o", "filled") ;
   sn = sprintf("{\\it{E_{T,A}}} = %.0f", xn) ; sx = sprintf("{\\it{E_{T,A}}} = %.0f", xx) ;
   legend([hx hn], {sx sn}, "box", "off", "location", "northeast") ;
endif
posa = get(ax, "position") ; posa(2) = 0.16 ;  set(ax, "position", posa) ;
posc = get(hc, "position") ; posc(2) *= 1.1 ; posc(4) *= 0.95 ; set(hc, "position", posc) ;
ax = subplot(1, 2, 2) ;
[h hc hb] = plot_fld(cape_r, d, [0 600]) ;
I = sdate(pdd.ts.id, d) ;
if any(I)
   x = pdd.ts.x(I,jVAR) ;
   h = scatter(pdd.ts.lon(I), pdd.ts.lat(I), x, "r", "o", "filled") ;
   [xn in] = min(x) ; [xx ix] = max(x) ;
   hn = scatter(pdd.ts.lon(I)(in), pdd.ts.lat(I)(in), cx*x(in), "r", "o", "filled") ;
   hx = scatter(pdd.ts.lon(I)(ix), pdd.ts.lat(I)(ix), cx*x(ix), "r", "o", "filled") ;
   sn = sprintf("{\\it{E_{T,A}}} = %.0f", xn) ; sx = sprintf("{\\it{E_{T,A}}} = %.0f", xx) ;
   legend([hx hn], {sx sn}, "box", "off", "location", "northeast") ;
endif
posa2 = get(ax, "position") ; posa2(2) = posa(2) ;  set(ax, "position", posa2) ;
posc2 = get(hc, "position") ; posc2([2 4]) = posc([2 4]) ; set(hc, "position", posc2) ;

hgsave(sprintf("nc/%s.%02d/cape.%s.og", EXP, NH, ds)) ;
print("-r300", sprintf("nc/%s.%02d/cape.%s.png", EXP, NH, ds)) ;


text(4.2, 56.5, "(a)") ;
hc = colorbar(ax) ;
pos = get(get(hc, "label"), "extent")(2) ;
set(get(hc, "label"), "string", "cape  [J/kg]", "position", [0.5 2.1*pos], "rotation", 0) ;
ax = subplot(1, 2, 2)
text(4.2, 56.5, "(b)") ;
hc = colorbar(ax) ;
pos = get(get(hc, "label"), "extent")(2) ;
set(get(hc, "label"), "string", "cape  [J/kg]", "position", [0.5 2.1*pos], "rotation", 0) ;

set(findall(h2, "-property", "fontname"), "fontname", "Libertinus Sans") ;
set(findall(h2, "type", "axes"), "fontsize", 14) ;
set(findall(h2, "type", "text"), "fontsize", 14) ;

printf("--> nc/%s.%02d/%s.svg\n", EXP, NH, ds) ;
hgsave(h1, sprintf("nc/%s.%02d/%s.og", EXP, NH, ds)) ;
print(h1, sprintf("nc/%s.%02d/%s.svg", EXP, NH, ds)) ;
printf("--> nc/%s.%02d/cape.%s.svg\n", EXP, NH, ds) ;
hgsave(h2, sprintf("nc/%s.%02d/cape.%s.og", EXP, NH, ds)) ;
print(h2, sprintf("nc/%s.%02d/cape.%s.svg", EXP, NH, ds)) ;

figure(1, "position", [0.7 0.4 0.3 0.6]) ;
clf ; hold on ; cx = 5 ;
E = {"NS" "EW"} ;
for i = 1 : 2
   subplot(1, 2, i) ; hold on

   I = sdate(cape.id, d) ;
   xm = squeeze(cape.x(I,:,:)) ;
   imagesc(cape.lon, cape.lat, xm') ;
   set(gca, "ydir", "normal") ;
   colormap(brewermap(9, "Blues"))

   plot_ger(:, GLON, GLAT, E{i})

   I = sdate(pdd.ts.id, d) ;
   x = pdd.ts.x(I,jVAR) ;
   h = scatter(pdd.ts.lon(I), pdd.ts.lat(I), cx*x, "r", "o", "filled") ;
   [xn in] = min(x) ; [xx ix] = max(x) ;
   hn = scatter(pdd.ts.lon(I)(in), pdd.ts.lat(I)(in), cx*x(in), "r", "o", "filled") ;
   hx = scatter(pdd.ts.lon(I)(ix), pdd.ts.lat(I)(ix), cx*x(ix), "r", "o", "filled") ;
   sn = sprintf("{\\it{E_{T,A}}} = %.0f", xn) ; sx = sprintf("{\\it{E_{T,A}}} = %.0f", xx) ;

endfor
hgsave(sprintf("nc/regions_%s.og", EXP)) ;
print(sprintf("nc/regions_%s.svg", EXP)) ;

## compare cape orig, regridded, and reconstructed
load("data/ind/ind_DE.ob") ; orig = era5.cape ;
load("data/ind/ind_DE.rgr.ob") ; rgr = era5.cape ;
load("data/era5/ptr_24.ob") ; rec = era5.cape ;

cl = [0 1000] ;
figure(1, "position", [0.5 0.7 0.5 0.3]) ; clf ;
subaxis(1, 3, 1, "SpacingHoriz", sh) ;
plot_fld(orig, d, cl) ; title("original") ;
subaxis(1, 3, 2, "SpacingHoriz", sh) ;
plot_fld(rgr, d, cl) ; title("regrid") ;
subaxis(1, 3, 3, "SpacingHoriz", sh) ;
plot_fld(rec, d, cl) ; title("recon") ;
set(findall("type", "axes"), "fontsize", 14) ;
set(findall("type", "text"), "fontsize", 14) ;


## training curves
COL = [0.2 0.7 0.2 ; 0.2 0.2 0.7] ;
jNET = 2 ;
clf ;
jNET++ ;
net = NET{jNET} ;
lfile = sprintf("%s.log", pfx(net)) ;
plot_log(gca, lfile, "loss", gap = 2, pse = 0, plog = 0) ;
title(net)
hgsave(sprintf("nc/loss.%s.og", net)) ;
print(sprintf("nc/loss.%s.svg", net)) ;

GAP = [1 1 1 1 1 1 1 1 1] ;
clf ; j = 0 ; clear H ;
for net = NET((1:9) ~= jNET)
   net = net{:} ;
   ax(++j) = subplot(2, 4, j) ;
   if exist(lfile = sprintf("%s.log", pfx(net)), "file") ~= 2
      warning("file not found: %s", lfile) ;
   endif
   jN = find(strcmp(NET, net)) ;
   H(j,:) = plot_log(ax(j), lfile, "loss", gap = GAP(jN), pse = 0, plog = 0) ;
   title(net)
endfor
delete(H(:,3)) ;
set(findobj("-property", "fontsize"), "fontsize", 12) ;
set(ax, "xtick", 2:2:8) ;
hgsave(sprintf("nc/%s.%02d/loss.og", EXP, NH)) ;
print(sprintf("nc/%s.%02d/loss.svg", EXP, NH)) ;

## skill plot w/ uncertainties
source conf.m ; E = {EXP} ; JPDD = [6 1 2] ;
M = union(MDL(JMDL), NET(JNET), "stable") ;
ML = union(toupper(MDL(JMDL)), NET(JNET), "stable") ;
JMDL = 2 : length(MDL) ; JNET = 1 : length(NET) ;
if mW = (length(E) * length(JPDD) > 1)
   figure(1, "position", [0.7 0.25 0.35 0.75]) ;
endif
clf ; clear hT ax
for jPDD = JPDD
   j = find(JPDD == jPDD) ;
   if mW
      ax(j) = subaxis(length(JPDD), 1, j, "SpacingVert", 0.17) ;
   else
      ax = gca ;
   endif
   hold on ;
   PDD = {"WEI" "xWEI" "cape" "cp" "regnie" "CatRaRE"}{jPDD} ;
   lname = sprintf("%s_%02d_%02.0f", PDD, CNVDUR, 100 * Q0) ;
   i = 0 ; MSKL = {"ACCURACY" "RPSS" SKL{:}} ; jSKL = 2 ; clear S ;
   for Exp = E

      jPLT = 0 ; yx = -Inf ;
      for m = M
	 jPLT++ ; m = m{:} ;
	 if ~isempty(jM = find(strcmp(m, MDL)))
	    fE = strrep(Exp{:}, "/", "") ;
	    load(sprintf("nc/%s.%02d/skl.Shallow.%s.%s.ot", fE, NH, IND, lname)) ;
	    S(jPLT) = skl.VAL(jM,jSKL) ;
	 else
	    if exist(ifile = sprintf("nc/%s.%02d/skl.%s.%s.%s.ot", fE, NH, m, IND, lname), "file") == 2
	       printf("%d <-- %s\n", jPLT, ifile) ;
	       load(ifile) ;
	       S(jPLT) = mean(skl.VAL(:,jSKL)) ; Se(jPLT) = range(skl.VAL(:,jSKL)) / 2 ;
	       yx = max(yx, max(skl.VAL(:,jSKL))) ;
	    endif
	 endif
      endfor

      [~, ISS] = sort(S) ; [~,IS] = sort(ISS) ;

      jPLT = 0 ;
      for m = M
	 jPLT++ ; m = m{:} ;
	 if S(jPLT) < 0 continue ; endif
	 jM = find(strcmp(m, MDL)) ;
	 jN = find(strcmp(m, NET)) ;
	 if ~isempty(jM)
	    hS(jM) = errorbar(IS(jPLT), S(jPLT), 0, 0) ;
	    set(hS(jM), "color", colS(jM,:), "marker", "o", "markersize", 14, "markerfacecolor", colS(jM,:), "markeredgecolor", [1 1 1]) ;
	 else
	    hD(jN) = errorbar (IS(jPLT), S(jPLT), Se(jPLT), Se(jPLT), "~o") ;
	    set(hD(jN), "color", colD(jN,:), "marker", "d", "markersize", 16, "markerfacecolor", colD(jN,:), "markeredgecolor", [1 1 1]) ;
	 endif
      endfor

##      for jMDL = JMDL
##	 jPLT++ ;
##	 if S(jPLT) < 0 continue ; endif
##	 hS(jMDL) = errorbar(IS(jPLT), S(jPLT) = skl.VAL(jMDL,jSKL), 0, 0) ;
##	 set(hS(jMDL), "color", colS(jMDL,:), "marker", "o", "markersize", 14, "markerfacecolor", colS(jMDL,:), "markeredgecolor", [1 1 1]) ;
##      endfor
##      for jNET = JNET
##	 if exist(ifile = sprintf("nc/%s.%02d/skl.%s.%s.%s.ot", fE, NH, NET{jNET}, IND, lname), "file") == 2
##	    jPLT++ ;
##	    if S(jPLT) < 0 continue ; endif
##	    load(ifile) ;
##	    m = mean(skl.VAL(:,jSKL)) ; e = range(skl.VAL(:,jSKL)) / 2 ;
##	    hD(jNET) = errorbar (IS(jPLT), S(jPLT) = m, e, e, "~o") ;
##	    set(hD(jNET), "color", colD(jNET,:), "marker", "d", "markersize", 16, "markerfacecolor", colD(jNET,:), "markeredgecolor", [1 1 1]) ;
##	 endif
##      endfor

      ylabel(MSKL{jSKL}) ;
      if mW
	 ht(j) = title(sprintf("%s", PDD)) ;
      else
	 ht(j) = title(sprintf("%s (%02d%%-quantile)", PDD, 100*Q0)) ;
      endif
      LBL = ML(ISS(S(ISS)>0)) ;
      xt = 1+sum(S<0) : length(S) ; [xt0 xt1] = bounds(xt) ;
      set(gca, "xtick", xt, "xticklabel", LBL, "xticklabelrotation", -90, "tickdir", "out") ;
      set(gca, "xlim", [xt0-1 xt1+1]) ;
      xtl = xticklabels ();
      xt = xticks ();
      yt = yticks ();
      xticklabels ([]);
      for ii = 1:numel (xtl)
	 hT(ii) = text (xt(ii), yt(1)-0.2*(yt(2)-yt(1)), xtl{ii}, "rotation", 90, "horizontalalignment", "right") ;
      endfor
      set(hT, "fontsize", 16) ;
   endfor
   pos0 = pos = get(gca, "position") ;
   pos(2) += 0.2 ; pos(4) -= 0.22 ; set(gca, "position", pos) ;
endfor
set(ax, "ygrid", "on", "box", "off")
##set(ax, "ylim", [-0.02 1.2*yx])
set(findobj("-property", "fontsize"), "fontsize", 20) ;
set(ht, "fontsize", 24) ;
hgsave(sprintf("nc/%s.%s.%s.%02d.og", EXP, MSKL{jSKL}, IND, 100*Q0)) ;
print(sprintf("nc/%s.%s.%s.%02d.svg", EXP, MSKL{jSKL}, IND, 100*Q0)) ;


## reliability curves
set(gcf, "position", get(groot, "defaultfigureposition")) ;
clf ; j = 0 ; clear ax h r
for kMDL = 1 : length(MDL)
   mdl = MDL{kMDL} ; sfx = sprintf("data/%s.%02d/%dx%d", EXP, NH, RES{kMDL}) ;
   if exist(pfile = sprintf("data/%s.%02d/Shallow.%s.%s.%s.ob", EXP, NH, mdl, IND, LNAME), "file") == 2
      load(pfile) ;
   else
      warning("file not found: %s", pfile) ;
   endif
   [r(kMDL,:) b] = rlb(shallow.prob.x(:,jC), pdd.lc, 10) ;
##   ax(++j) = subaxis(2, 2, j, "SpacingH", 0.07, "SpacingV", 0.17) ;
   ax(++j) = subplot(2, 2, j) ;
   plot(b, r(kMDL,:), "color", colS(kMDL,:), [0 1], [0 1], "k--") ;
   title(upper(mdl))
   pos = get(gca, "position") ;
   ix(j) = axes("position",[pos(1)+0.21 pos(2)+0.05 pos(3)/3 pos(4)/3]) ;
   [nn xx] = hist(shallow.prob.x(:,jC), 10, "facecolor", "k", "edgecolor", "k") ;
   bar(xx, nn/size(shallow.prob.x,1), "barwidth", 0.2, "facecolor", "k", "edgecolor", "k") ;
   ylim([0 0.35]) ;
   set(get(get(gca, "children"), "baseline"), "visible", "off") ;
   set(gca, "XTick", [], "YTick", [], "box", "off", "ycolor", "none")
endfor
set(ax, "XTick", [0.2 0.5 0.8], "YTick", [0.2 0.5 0.8]) ;
xlabel(ax(3), "forecast prob.") ; ylabel(ax(3), "observed freq.")
set(findobj("-property", "fontsize"), "fontsize", 16) ;
hgsave(sprintf("nc/%s.%02d/Shallow.rlb.%s.%s.%s.og", EXP, NH, RGN, SKL{jSKL}, IND)) ;
print(sprintf("nc/%s.%02d/Shallow.rlb.%s.%s.%s.svg", EXP, NH, RGN, SKL{jSKL}), IND) ;

clf ; j = 0 ; clear ax h r
for kNET = 1 : length(NET)
   net = NET{kNET} ; sfx = sprintf("data/%s.%02d/%dx%d", EXP, NH, RES{kNET}) ;
   if exist(pfile = sprintf("%s/Deep.%s.%s.%s.ob", sfx, net, IND, LNAME), "file") == 2
      load(pfile) ;
   else
      warning("file not found: %s", pfile) ;
   endif
   [r(:,kNET) b] = rlb(deep.prob.x(:,jC), pdd.lc, 10) ;
   ax(++j) = subplot(3, 3, j) ;
   plot(b, r(:,kNET), "color", colD(kNET,:), [0 1], [0 1], "k--") ;
   title(net)
   pos = get(gca, "position") ;
   ix(j) = axes("position",[pos(1)+0.12 pos(2)+0.03 pos(3)/3 pos(4)/3]) ;
   [nn xx] = hist(deep.prob.x(:,jC), 10, "facecolor", "k", "edgecolor", "k") ;
   bar(xx, nn/size(deep.prob.x,1), "barwidth", 0.2, "facecolor", "k", "edgecolor", "k") ;
   ylim([0 0.35]) ;
   set(get(get(gca, "children"), "baseline"), "visible", "off") ;
   set(gca, "XTick", [], "YTick", [], "box", "off", "ycolor", "none")
endfor
set(ax, "XTick", [0.2 0.5 0.8], "YTick", [0.2 0.5 0.8]) ;
xlabel(ax(7), "forecast prob.") ; ylabel(ax(7), "observed freq.")
set(findobj("-property", "fontsize"), "fontsize", 14) ;
hgsave(sprintf("nc/%s.%02d/Deep.rlb.%s.%s.%s.og", EXP, NH, RGN, SKL{jSKL}, IND)) ;
print(sprintf("nc/%s.%02d/Deep.rlb.%s.%s.%s.svg", EXP, NH, RGN, SKL{jSKL}, IND)) ;


## plot scatter of obs vs sim annual probs!!
for EXP = {"NS" "EW"}
   EXP = EXP{:} ;
   load(sprintf("data/%s.%02d/%s_%02d.ob", EXP, NH, PDD, CNVDUR)) ;
   for jC = 3 : 4

      pdd.lc = c2l(pdd.c)(:,jC) ; qEta = mean(pdd.lc) ; RGN = pdd.uc(jC) ;

      C1 = cellfun(@(mdl) sprintf("Shallow.%s", mdl), MDL, "UniformOutput", false) ;
      C1n = toupper(MDL) ;
      C2= cellfun(@(net) sprintf("Deep.%s", net), NET, "UniformOutput", false) ;
      C2n = NET ;
      JM = [2 8] ;
      C = union(C1, C2, "stable")(JM) ;
      Cn = union(C1n, C2n, "stable")(JM) ;

      ## ERA5
      COL = [1 1 1 ; 0.2 0.7 0.2 ; 0.2 0.2 0.7 ; 0.7 0.2 0.2] ;
      figure(1, "position", [0.7 0.7 0.4 0.34]) ; sz = 40 ;
      [o.id o.x] = annstat(pdd.id, real(pdd.lc), @nanmean) ;
      oC = sdate(o.id, [2001 1 1 ; 2010 1 1]) ;
      oV = sdate(o.id, [2011 1 1 ; 2020 1 1]) ;

      jMDL = 0 ; clf ; hold on
      for mdl = C
	 jMDL++ ;
	 mdl = mdl{:} ;
	 eval(sprintf("[s.id s.x] = annstat(ana.prob.id, ana.prob.%s, @nanmean) ;", m2s(mdl))) ;
	 s.x = s.x(:,jC) ;
	 sC = sdate(s.id, [2001 1 1 ; 2010 1 1]) ;
	 sV = sdate(s.id, [2011 1 1 ; 2020 1 1]) ;
	 xn = min([o.x ; s.x]) ; xx = max([o.x ; s.x]) ;
	 ax1 = subplot(2, 3, 3*(jMDL-1) + (1:2)) ; hold on
	 plot(s.id([1 end],1), [qEta qEta], "color", 0.7*COL(1,:), "linewidth", 2, "linestyle", "--") ;
	 scatter(o.id(:,1), o.x, 0.7*sz, 0*COL(1,:), "x") ;
	 scatter(s.id(:,1), s.x, sz, COL(3,:), "filled") ;
	 scatter(s.id(sC,1), s.x(sC), sz, COL(2,:), "filled") ;
	 ##   scatter(s.id(sV,1), s.x(sV), sz, COL(3,:), "filled") ;
	 [B, BINT, R, RINT, STATS] = regress(o.x, [ones(rows(o.x),1) o.id(:,1)]) ;
	 if STATS(3) < alpha
	    yf = [ones(rows(o.x),1) o.id(:,1)] * B ;
	    h = plot(o.id(:,1), yf, "color", 0*COL(1,:), "linestyle", "--", "linewidth", 1) ;
	 endif
	 [B, BINT, R, RINT, STATS] = regress(s.x, [ones(rows(s.x),1) s.id(:,1)]) ;
	 if STATS(3) < alpha
	    yf = [ones(rows(s.x),1) s.id(:,1)] * B ;
	    h = plot(s.id(:,1), yf, "color", 0*COL(1,:), "linewidth", 2) ;
	    xt = mean(get(h, "xdata")) - 5 ;
	    text(xt, 0.9*ylim()(2), sprintf("{\\ittr=%.2f}", 100*B(2)), "color", 0*COL(1,:)) ;
	 endif
	 xlim(s.id([1 end],1)) ;
	 ylim([0.8*ylim()(1) 1.1*ylim()(2)]) ;
	 xn = 0 ; xx = 0.4 ; ylim([xn xx]) ; 
	 xlabel("year") ; ylabel(sprintf("P")) ;
	 set(gca, "fontsize", 14)
	 ax2 = subplot(2, 3, 3*(jMDL-1) + 3) ; hold on
	 plot([xn xx], [xn xx], "color", 0*COL(1,:), "linewidth", 2, "linestyle", "--") ;
	 scatter(o.x(oC), s.x(sC), sz, COL(2,:), "filled") ;
	 scatter(o.x(oV), s.x(sV), sz, COL(3,:), "filled") ;
	 xlim([xn xx]) ; ylim([xn xx]) ;
	 axis square ;
	 xlabel("OBS") ; ylabel(sprintf("%s", Cn{jMDL})) ;
	 ##   xlabel("{\\itP_{CatRaRE}}, OBS") ; ylabel(sprintf("{\\itP_{CatRaRE}}, %s", mdl)) ;
	 r = corr(o.x(oV), s.x(sV)) ;
	 printf("%s:\t%.2f\t%.2f\t%.2f\n", mdl, r, 100*B(2), STATS(3)<alpha) ;
	 text(xn, xx, sprintf("{\\it\\rho = %.2f}", r), "color", COL(3,:)) ;
	 set(gca, "fontsize", 14, "XTick", [0.4 0.6], "YTick", [0.4 0.6]) ;
	 yl = [min(ylim(ax1)(1), ylim(ax2)(1)) max(ylim(ax1)(2), ylim(ax2)(2))] ;
	 set([ax1 ax2], "ylim", yl)
      endfor
      hgsave(sprintf("nc/%s.%02d/ERA5.%s.%s-%s.og", EXP, NH, RGN, Cn{:})) ;
      print(sprintf("nc/%s.%02d/ERA5.%s.%s-%s.svg", EXP, NH, RGN, Cn{:})) ;

      figure(1, "position", [0.7 0.3 0.3 0.7]) ; sz = 20 ;
      JMa = setxor(JM, 1 : length(MDL) + length(NET)) ;
      Call = union(C1, C2, "stable")(JMa) ;
      Calln = union(C1n, C2n, "stable")(JMa) ;
      clf ; hold on ; clear ax
      jPLT = jMDL = 0 ; nMDL = round(length(Call)/2) ; fs = 14 ;
      for mdl = Call
	 jPLT++ ; jMDL++ ;
	 if jPLT/nMDL > 1
	    set(findobj("-property", "fontsize"), "fontsize", fs) ;
	    hgsave(sprintf("nc/%s.%02d/ERA5.1.%s.og", EXP, NH, RGN)) ;
	    print(sprintf("nc/%s.%02d/ERA5.1.%s.svg", EXP, NH, RGN)) ;
	    jPLT = rem(jPLT, nMDL) ;
	    clf ; hold on ;
	 endif
	 mdl = mdl{:} ;
	 ax(jPLT,1) = subplot(nMDL, 3, 3*(jPLT-1) + (1:2)) ; hold on
	 eval(sprintf("[s.id s.x] = annstat(ana.prob.id, ana.prob.%s, @nanmean) ;", m2s(mdl))) ;
	 s.x = s.x(:,jC) ;
	 sC = sdate(s.id, [2001 1 1 ; 2010 1 1]) ;
	 sV = sdate(s.id, [2011 1 1 ; 2020 1 1]) ;
	 xn = min([o.x ; s.x]) ; xx = max([o.x ; s.x]) ;
	 plot(s.id([1 end],1), [qEta qEta], "color", 0.7*COL(1,:), "linewidth", 2, "linestyle", "--") ;
	 scatter(o.id(:,1), o.x, 0.7*sz, 0*COL(1,:), "x") ;
	 [B, BINT, R, RINT, STATS] = regress(o.x, [ones(rows(o.x),1) o.id(:,1)]) ;
	 if STATS(3) < alpha
	    yf = [ones(rows(o.x),1) o.id(:,1)] * B ;
	    h = plot(o.id(:,1), yf, "color", 0*COL(1,:), "linestyle", "--", "linewidth", 1) ;
	 endif
	 scatter(s.id(~(sC),1), s.x(~(sC)), sz, COL(3,:), "filled") ;
	 scatter(s.id(sC,1), s.x(sC), sz, COL(2,:), "filled") ;
	 scatter(s.id(sV,1), s.x(sV), sz, COL(3,:), "filled") ;
	 [B, BINT, R, RINT, STATS] = regress(s.x, [ones(rows(s.x),1) s.id(:,1)]) ;
	 if STATS(3) < alpha
	    yf = [ones(rows(s.x),1) s.id(:,1)] * B ;
	    h = plot(s.id(:,1), yf, "color", 0*COL(1,:), "linewidth", 2) ;
	    xt = get(h, "xdata")(1) + 5 ;
	    text(xt, 0.9*ylim()(2), sprintf("{\\ittr=%.2f}", 100*B(2)), "color", 0*COL(1,:)) ;
	 endif
	 xlim(s.id([1 end],1)) ;
	 ylim([0.8*ylim()(1) 1.1*ylim()(2)]) ;
	 xn = 0 ; xx = 0.4 ; ylim([xn xx]) ; 
	 xlabel("year") ; ylabel(sprintf("P")) ;
	 set(gca, "fontsize", 14)
	 ax(jPLT,2) = subplot(nMDL, 3, 3*(jPLT-1) + 3) ; hold on
	 plot([xn xx], [xn xx], "color", 0*COL(1,:), "linewidth", 2, "linestyle", "--") ;
	 scatter(o.x(oC), s.x(sC), sz, COL(2,:), "filled") ;
	 scatter(o.x(oV), s.x(sV), sz, COL(3,:), "filled") ;
	 xlim([xn xx]) ; ylim([xn xx]) ;
	 set(gca, "XTick", [0.4 0.6], "YTick", [0.4 0.6]) ;
	 axis square ;
	 xlabel("OBS") ; ylabel(sprintf("%s", Calln{jMDL})) ;
	 ##   xlabel("{\\itP_{CatRaRE}}, OBS") ; ylabel(sprintf("{\\itP_{CatRaRE}}, %s", mdl)) ;
	 r = corr(o.x(oV), s.x(sV)) ;
	 printf("%s:\t%.2f\t%.2f\t%.2f\n", mdl, r, 100*B(2), STATS(3)<alpha) ;
	 text(xn, xx, sprintf("{\\it\\rho = %.2f}", r), "color", COL(3,:)) ;
	 set(gca, "fontsize", 14)
	 yl = [min(ylim(ax(jPLT,1))(1), ylim(ax(jPLT,2))(1)) max(ylim(ax(jPLT,1))(2), ylim(ax(jPLT,2))(2))] ;
	 set(ax(jPLT,:), "ylim", yl)
	 drawnow
      endfor
      set(findobj("-property", "fontsize"), "fontsize", fs) ;
      hgsave(sprintf("nc/%s.%02d/ERA5.2.%s.og", EXP, NH, RGN)) ;
      print(sprintf("nc/%s.%02d/ERA5.2.%s.svg", EXP, NH, RGN)) ;

      ## GCM/RCM
      figure(1, "position", [0.7 0.7 0.4 0.4]) ; sz = 30 ;
      jMDL = 0 ; clf ; hold on
      for mdl = C
	 jMDL++ ;
	 mdl = mdl{:} ;
	 subplot(2, 1, jMDL) ; hold on
	 clear h stats yf ; j = 0 ;
	 h(++j) = plot([1951 2100], [qEta qEta], "color", 0.7*COL(1,:), "linewidth", 2, "linestyle", "--") ;
	 for S = SIM(j+1:end)
	    j++ ;
	    load(sprintf("data/%s.%02d/%s.%s.%s.ob", EXP, NH, S{:}, IND, LNAME)) ;
	    eval(sprintf("sim = %s ;", S{:})) ;
	    eval(sprintf("[s.id s.x] = annstat(sim.prob.id, sim.prob.%s, @nanmean) ;", m2s(mdl))) ;
	    scatter(s.id(:,1), s.x(:,jC), sz, COL(j+1,:), "filled") ; axis tight
	    [B, BINT, R, RINT, STATS] = regress(s.x(:,jC), [ones(rows(s.x),1) s.id(:,1)]) ;
	    ct(j) = 100*B(2) ;
	    stats(j,:) = STATS ;
	    yf.id{j} = s.id(:,1) ;
	    yf.x{j} = [ones(rows(s.x),1) s.id(:,1)] * B ;
	    xlabel("year") ; ylabel(sprintf("P")) ;
	 endfor
	 ylim([0 0.4]) ;
	 printf("%s:\t%.2f\t%d\t%.2f\t%d\n", mdl, ct(2), stats(2,3)<alpha, ct(3), stats(3,3)<alpha) ;
	 ##   ylim([0.25 0.7]) ;
	 for j = 2:rows(stats)
	    if stats(j,3) < alpha
	       h(j) = plot(yf.id{j}, yf.x{j}, "color", COL(j+1,:), "linewidth", 2) ;
	       xt = mean(get(h(j), "xdata")) - 10 ;
	       text(xt, 1.2*ylim()(1), sprintf("{\\ittr=%.2f}", ct(j)), "color", COL(j+1,:)) ;
	    endif
	 endfor
	 if any(I = (1 : length(h)) > 1 & h ~= 0)
	    legend(h(I), {"CLIM" NSIM{2:end}}(I), "box", "off", "location", "southeast") ;
	 endif
	 j = eval(sprintf("find(strcmp({%s.prob.nc.Attributes.Name}, \"driving_model_id\")) ;", SIM{2})) ;
	 GCM = eval(sprintf("%s.prob.nc.Attributes(j).Value ;", SIM{2})) ;
	 j = eval(sprintf("find(strcmp({%s.prob.nc.Attributes.Name}, \"model_id\")) ;", SIM{2})) ;
	 RCM = eval(sprintf("%s.prob.nc.Attributes(j).Value ;", SIM{2})) ;
	 htit = title(sprintf("%s", Cn{jMDL}), "fontsize", 20) ;
	 pos = get(htit, "position") ;
	 ht = text(1960, pos(2) - 0.05, sprintf("GCM: %s\nRCM: %s", GCM, RCM), "fontsize", 14) ;
      endfor
      hgsave(sprintf("nc/%s.%02d/%s.%s.%s.%s-%s.og", EXP, NH, RGN, GCM, RCM, Cn{:})) ;
      print(sprintf("nc/%s.%02d/%s.%s.%s.%s-%s.svg", EXP, NH, RGN, GCM, RCM, Cn{:})) ;

      figure(1, "position", [0.7 0.3 0.3 0.7]) ; sz = 20 ;
      clf ; hold on ;
      jPLT = jMDL = 0 ; nMDL = round(length(Call)/2) ; sz = 10 ; fs = 12 ;
      for mdl = Call
	 jPLT++ ; jMDL++ ;
	 if jPLT/nMDL > 1
	    set(findobj("-property", "fontsize"), "fontsize", fs) ;
	    hgsave(sprintf("nc/%s.%02d/%s.GCM_RCM.1.og", EXP, NH, RGN)) ;
	    print(sprintf("nc/%s.%02d/%s.GCM_RCM.1.svg", EXP, NH, RGN)) ;
	    jPLT = rem(jPLT, nMDL) ;
	    clf ; hold on ;
	 endif
	 mdl = mdl{:} ;
	 ax(jPLT,1) = subplot(nMDL, 1, jPLT) ; hold on
	 clear h stats yf ; j = 0 ;
	 h(++j) = plot([1951 2100], [qEta qEta], "color", 0.7*COL(1,:), "linewidth", 2, "linestyle", "--") ;
	 for S = SIM(j+1:end)
	    j++ ;
	    eval(sprintf("sim = %s ;", S{:})) ;
	    eval(sprintf("[s.id s.x] = annstat(sim.prob.id, sim.prob.%s, @nanmean) ;", m2s(mdl))) ;
	    scatter(s.id(:,1), s.x(:,jC), sz, COL(j+1,:), "filled") ; axis tight
	    [B, BINT, R, RINT, STATS] = regress(s.x(:,jC), [ones(rows(s.x),1) s.id(:,1)]) ;
	    ct(j) = 100 * B(2) ;
	    stats(j,:) = STATS ;
	    yf.id{j} = s.id(:,1) ;
	    yf.x{j} = [ones(rows(s.x),1) s.id(:,1)] * B ;
	    xlabel("year") ; ylabel(sprintf("P")) ;
	 endfor
	 printf("%s:\t%.2f\t%d\t%.2f\t%d\n", mdl, ct(2), stats(2,3)<alpha, ct(3), stats(3,3)<alpha) ;
	 ##   ylim([0.25 0.7]) ;
	 for j = 2:rows(stats)
	    if stats(j,3) < alpha
	       h(j) = plot(yf.id{j}, yf.x{j}, "color", COL(j+1,:), "linewidth", 2) ;
	       xt = mean(get(h(j), "xdata")) - 10 ;
	       text(xt, 1.2*ylim()(1), sprintf("{\\ittr=%.2f}", ct(j)), "color", COL(j+1,:)) ;
	    endif
	 endfor
	 htit = title(sprintf("%s", Calln{jMDL}), "fontsize", 20) ;
	 if jPLT ~= 1 continue ; endif
	 ##   j = eval(sprintf("find(strcmp({%s.prob.nc.Attributes.Name}, \"driving_model_id\")) ;", SIM{2})) ;
	 ##   GCM = eval(sprintf("%s.prob.nc.Attributes(j).Value ;", SIM{2})) ;
	 ##   j = eval(sprintf("find(strcmp({%s.prob.nc.Attributes.Name}, \"model_id\")) ;", SIM{2})) ;
	 ##   RCM = eval(sprintf("%s.prob.nc.Attributes(j).Value ;", SIM{2})) ;
	 ##   pos = get(htit, "position") ;
	 ##   ht = text(1955, pos(2) - 0.05, sprintf("GCM: %s\nRCM: %s", GCM, RCM), "fontsize", 14) ;
	 if any(I = (1 : length(h)) > 1 & h ~= 0)
	    legend(h(I), {"CLIM" NSIM{2:end}}(I), "box", "off", "location", "southeast") ;
	 endif
      endfor
      set(findobj("-property", "fontsize"), "fontsize", fs) ;
      hgsave(sprintf("nc/%s.%02d/%s.GCM_RCM.2.og", EXP, NH, RGN)) ;
      print(sprintf("nc/%s.%02d/%s.GCM_RCM.2.svg", EXP, NH, RGN)) ;

   endfor
endfor

## ptr trends
YUNIT = {"J/kg" "J/kg" "s^-1" "s^-1" "m" "kg/m^2"} ;
m2s = @(mdl, k) strrep(strrep(mdl, "-", "_"), "_", [repmat("\\", 1, k), "_"]) ;
COL = [0 0 0 ; 0.2 0.2 0.7 ; 0.7 0.5 0.2 ; 0.7 0.2 0.2] ;
yl = [-0.8 0.6 ; -0.2 0.2 ; -0.8 0.8] ;
hloc = {"northwest" "southeast" "southeast"} ;
figure(1) ; sz = 12 ; fs = 20 ;
statfun = @(x) prctile(x, 50) ;
statfun = @(x) mean(x) ;
for jSIM = 1 : length(SIM)
   sim = SIM{jSIM} ;
   printf("<-- data/ptr.%s.ob\n", sim) ;
   load(sprintf("data/ptr.%s.ob", sim)) ;
   VP = trl_atm("atmvar.lst", [1 2 2 2](jSIM)) ;
   for k = VP(JVAR)
      b.(sim).(k{:}) = [] ;
      blbl.(sim).(k{:}) = {} ;
   endfor
   if strcmp(sim, "ana")
      for [v sv] = ptr
	 clf ; hold on
	 jv = find(strcmp(VP, sv)) ;
	 if ~ismember(jv, JVAR) continue ; endif
	 [s.id s.x] = annstat(v.id, mean(mean(v.x, 2), 3), statfun) ;
	 scatter(s.id(:,1), s.x(:,end), sz, COL(jSIM,:), "filled") ; axis tight
	 [B, BINT, R, RINT, STATS] = regress(s.x(:,end), [ones(rows(s.x),1) s.id(:,1)]) ;
	 b.(sim).(sv) = [b.(sim).(sv) B(2)] ;
	 blbl.(sim).(sv) = {blbl.(sim).(sv){:}} ;
	 yf = [ones(rows(s.x),1) s.id(:,1)] * B ;
	 if STATS(3) < alpha
	    plot(s.id(:,1), yf, "color", COL(jSIM,:), "linestyle", "-", "linewidth", 3) ;
	 else
	    plot(s.id(:,1), yf, "color", COL(jSIM,:), "linestyle", "--", "linewidth", 2) ;
	 endif
	 xlabel("year") ; ylabel("anomalies") ;
#	    legend(hs(:,jVAR), {"ERA5" NSIM{2:end}}, "location", hloc{jVAR}, "box", "off") ;
	 title(sprintf("%s (%s)", v.name, sim)) ;
##	 pause(1) ;
      endfor
   else
      jj = 0 ;
      for [gcm sgcm] = ptr
	 for [rea srea] = gcm
	    for [v sv] = rea
	       jv = find(strcmp(VP, sv)) ;
	       if ~ismember(jv, JVAR) continue ; endif
##	       if ~strcmp(sv, "cin") continue ; endif
##	       jj++ ;
##	       printf("%d: %s %s %s:\t%12.7f\t%12.7f\n", jj, sgcm, srea, sv, nanmean(v.x(:)), nanstd(v.x(:))) ;
	       clf ; hold on
	       [s.id s.x] = annstat(v.id, mean(mean(v.x, 2), 3), statfun) ;
	       if sum(isnan(s.x)) > 20 continue ; endif
	       scatter(s.id(:,1), s.x(:,end), sz, COL(jSIM,:), "filled") ; axis tight
	       [B, BINT, R, RINT, STATS] = regress(s.x(:,end), [ones(rows(s.x),1) s.id(:,1)]) ;
	       b.(sim).(sv) = [b.(sim).(sv) B(2)] ;
	       blbl.(sim).(sv) = {blbl.(sim).(sv){:} [sgcm "." srea]} ;
	       yf = [ones(rows(s.x),1) s.id(:,1)] * B ;
	       if STATS(3) < alpha
		  plot(s.id(:,1), yf, "color", COL(jSIM,:), "linestyle", "-", "linewidth", 3) ;
	       else
		  plot(s.id(:,1), yf, "color", COL(jSIM,:), "linestyle", "--", "linewidth", 2) ;
	       endif
	       xlabel("year") ; ylabel(sprintf("annual mean %s  [%s]", sv, YUNIT{jv})) ;
#	    legend(hs(:,jVAR), {"ERA5" NSIM{2:end}}, "location", hloc{jVAR}, "box", "off") ;
	       title(sprintf("%s (%s, %s,%s)", v.name, sim, m2si(sgcm, 0), srea)) ;
	       pfile = sprintf("nc/%s.%02d/trend/%s.%s.%s.%s.svg", EXP, NH, v.name, sim, m2si(sgcm, 0), srea) ;
	       printf("--> %s\n", pfile) ;
	       print(pfile) ;
##	       pause(1) ;
	    endfor
	 endfor
      endfor
   endif
endfor
set(findobj("-property", "fontsize"), "fontsize", fs) ;
set(findobj(hl, "type", "scatter"), "sizedata", 38) ;
hgsave(sprintf("nc/%s.%02d/ptr_trends.%s.og", EXP, NH, RGN)) ;
print(sprintf("nc/%s.%02d/ptr_trends.%s.svg", EXP, NH, RGN)) ;

## probs
statfun = @(x) nanmean(x) ;
YLBL = {"cape (norm.)" "cin (norm.)" "div (norm.)" "vort (norm.)" "cp (norm.)" "pw (norm.)"}(IND == "1") ;
YLBL = ["CatRaRE probabilities [%]" YLBL] ;
for jSIM = 1 : length(SIM)
   sim = SIM{jSIM} ;
   f = sprintf("data/ptr.%s.%s.ob", sim, IND) ;
   printf("<-- %s\n", f) ; load(f) ;
   if jSIM < 2
      eval(sprintf("[%s.id %s.x] = annstat(ptr.id, mean(mean(ptr.x, 3), 4), statfun) ;", sim, sim)) ;
   else
      s = {} ;
      for [gcm sgcm] = ptr
	 for [rea srea] = gcm
	    s = union(s, strcat(sgcm, ".", srea)) ;
	    eval(sprintf("[%s.%s.%s.id %s.%s.%s.x] = annstat(rea.id, mean(mean(rea.x, 3), 4), statfun) ;", sim, sgcm, srea, sim, sgcm, srea)) ;
	 endfor
      endfor
      eval(sprintf("S_%s = s ;", sim)) ;
   endif
endfor
M = intersect(intersect(S_historical, S_ssp126), S_ssp585) ;
load(sprintf("data/%s.%02d/%s.ob", EXP, NH, LNAME)) ;
pdd.lc = c2l(pdd.c) ; qEta = mean(pdd.lc) ;
p = reorder(pdd.uc, REG.name) ; pdd.oc = pdd.uc(p) ;
COL = [0.2 0.2 0.7 ; 0.2 0.7 0.2 ; 1 0.7 0 ; [0.8 0.1 0.1]] ;
yl = [-0.8 0.6 ; -0.2 0.2 ; -0.8 0.8] ;
hloc = {"northwest" "southeast" "southeast"} ;
sz = 12 ; fs = 20 ;
smdl = "tree" ; jC = 6 ; RGN = pdd.uc{jC} ;
for m = M
   m = strsplit(m{:}, ".") ;
   for jV = 1:length(VAR)+1 figure(jV) ; clf ; hold on ; endfor
   figure(1) ;
   h(1) = plot([1960 2100], 100*[qEta(jC) qEta(jC)], "--k") ;
   sgcm = m{1} ; srea = m{2} ;
   for jSIM = 2 : length(SIM)
      sim = SIM{jSIM} ;

      figure(1) ; ax(1) = gca ;
     load(sprintf("data/%s.%02d/%s.%s.%s.prob.ob", EXP, NH, sim, IND, LNAME)) ;
      s = prob.(smdl).(sgcm).(srea) ;
      [s.id s.x] = annstat(s.id, s.x(:,jC), statfun) ;
      hs(jSIM) = scatter(s.id(:,1), 100*s.x, sz, COL(jSIM,:), "filled") ; axis tight
      [B, BINT, R, RINT, STATS] = regress(s.x, [ones(rows(s.x),1) s.id(:,1)]) ;
      yf = [ones(rows(s.x),1) s.id(:,1)] * B ;
      if STATS(3) < alpha
	 h(jSIM) = plot(s.id(:,1), 100*yf, "color", COL(jSIM,:), "linestyle", "-", "linewidth", 3) ;
      else
	 h(jSIM) = plot(s.id(:,1), 100*yf, "color", COL(jSIM,:), "linestyle", "--", "linewidth", 2) ;
      endif

      eval(sprintf("s = %s.(sgcm).(srea) ;", sim)) ;
      for jV = 1 : length(VAR)
	 figure(1+jV) ; ax(1+jV) = gca ;
	 gs(jV,jSIM) = scatter(s.id(:,1), s.x(:,jV), sz, COL(jSIM,:), "filled") ; axis tight
	 [B, BINT, R, RINT, STATS] = regress(s.x(:,jV), [ones(rows(s.x),1) s.id(:,1)]) ;
	 yf = [ones(rows(s.x),1) s.id(:,1)] * B ;
	 if STATS(3) < alpha
	    g(jV,jSIM) = plot(s.id(:,1), yf, "color", COL(jSIM,:), "linestyle", "-", "linewidth", 3) ;
	 else
	    g(jV,jSIM) = plot(s.id(:,1), yf, "color", COL(jSIM,:), "linestyle", "--", "linewidth", 2) ;
	 endif
      endfor

   endfor
   arrayfun(@(a) xlabel(a, "year"), ax) ;
   arrayfun(@(jV) ylabel(ax(jV), "norm. units"), 1:length(VAR)+1) ;
   title(ax(1), sprintf("prob %s (%s, %s, %s)", pdd.uc{jC}, smdl, m2si(sgcm, 0), srea)) ;
   arrayfun(@(jV) title(ax(1+jV), sprintf("%s  (%s, %s)", VAR{jV}, m2si(sgcm, 0), srea)), 1:length(VAR)) ;
   hl = legend(ax(1), h, {"OBS" NSIM{2:end}}, "location", "northwest", "box", "off") ;
   arrayfun(@(jV) legend(g(jV,2:end), {NSIM{2:end}}, "location", "northwest", "box", "off"), 1:length(VAR)) ; ;
   set(findobj("-property", "fontsize"), "fontsize", fs) ;
##   hgsave(sprintf("nc/%s.%02d/prob.%s.%s.%s.%s.og", EXP, NH, pdd.uc{jC}, smdl, sgcm, srea)) ;
   print(1, sprintf("nc/%s.%02d/prob.%02d.%s.%s.%s.%s.svg", EXP, NH, 100*Q0, pdd.uc{jC}, smdl, sgcm, srea)) ;
   arrayfun(@(jV) print(1+jV, sprintf("nc/%s.%s.%s.svg\n", VAR{jV}, sgcm, srea)), 1:length(VAR)) ;
endfor
##endfor


### 4R plots
reset(0) ;
set(0, "defaultfigureunits", "normalized", "defaultfigurepapertype", "A4") ;
set(0, "defaultfigurepaperunits", "normalized", "defaultfigurepaperpositionmode", "auto") ;
set(0, "defaultfigureposition", [0.7 0.7 0.3 0.3]) ;
set(0, "defaultaxesgridalpha", 0.3)
set(0, "defaultaxesfontname", "Libertinus Sans", "defaulttextfontname", "Libertinus Sans") ;
set(0, "defaultaxesfontsize", 18, "defaulttextfontsize", 18, "defaultlinelinewidth", 2) ;

addpath ~/oct/nc/maxdistcolor
JREG = 1 : 6 ;
col = maxdistcolor(length(JREG), @(m) sRGB_to_OSAUCS (m, true, true)) ;
m2s = @(mdl, k) strrep(strrep(mdl, "-", "_"), "_", [repmat("\\", 1, k), "_"]) ;

source("conf.m") ;

for jMDL = 1 : length(MDL)
   mdl = MDL{jMDL} ;
   load(sprintf("data/%s.%02d/Shallow.%s.%s.%s.ob", EXP, NH, [mdl sIMB], IND, LNAME)) ;
   skl.(mdl) = shallow.rpss.VAL ;
endfor
for jNET = 1 : length(NET)
   mdl = NET{jNET} ;
   load(sprintf("data/%s.%02d/%dx%d/Deep.%s.%s.%s.ob", EXP, NH, RES{jNET}, [mdl sIMB], IND, LNAME)) ;
   skl.(mdl) = deep.rpss.VAL ;
endfor

load(sprintf("data/%s.%02d/%s.ob", EXP, NH, LNAME)) ;
p = reorder(pdd.uc, REG.name) ; pdd.oc = pdd.uc(p) ;
fmt = repmat("%7.1f", 1, length(REG.name) + 2) ;

jp = 91 ;
j = 0 ; skl0 = 0.2 ; jp++ ; clear sp ;
for sim = SIM
   sim = sim{:} ;
   for mdl = [MDL NET]
      mdl = mdl{:} ;
      if skl.(mdl) < skl0 continue ; endif
      if exist(tfile = sprintf("data/%s.%02d/%s.%s.%s.%s.prob.ob", EXP, NH, sim, mdl, IND, LNAME), "file") ~= 2
	 continue
      endif
      load(tfile) ;
      if isfield(tprob, "x")
	 printf(["%5d%12s%10s (%.2f)%15s:" fmt "\n"], ++j, sim, mdl, skl.(mdl), "", [100*nanmean(tprob.x)]) ;
	 if j == jp
	    sp = tprob ; simp = sim ; mdlp = mdl ;
	    ttl = sprintf("%s %s", toupper(sim), mdl) ;
	 endif
      else
	 for [gcm sgcm] = tprob
	    for [rea srea] = gcm
	       printf(["%5d%12s%10s (%.2f)%15s%10s:" fmt "\n"], ++j, sim, mdl, skl.(mdl), sgcm, srea, [100*nanmean(rea.x)]) ;
	       if j == jp
		  sp = rea ; simp = sim ; mdlp = mdl ; gcmp = sgcm ; reap = srea ;
		  ttl = sprintf("%s %s %s %s", toupper(sim), mdl, m2s(sgcm, 1), srea) ;
	       endif
	    endfor
	 endfor
      endif
   endfor
endfor
printf(["%5d%12s%10s%15s%10s:" fmt "\n"], jp, simp, mdlp, gcmp, reap, [100*nanmean(sp.x)]) ;
v = rclim(sp, 5) ;

clf ; J = 2:5 ; hold on
set(gca, "NextPlot", "add", "box", "off") ;
plot(datenum(v.id([1 end],:)), 100*[pdd.qc(p)(J) pdd.qc(p)(J)]', "--")
set(gca, "colororderindex", 1)
plot(datenum(v.id), 100*v.x(:,p)(:,J))

xlabel("year") ; ylabel("prob  [%]") ;
title(ttl) ;
legend(pdd.uc(p)(J), "location", "eastoutside") ;
datetick yyyy keeplimits

area(datenum(v.id), v.x(:,p))
xlabel("year") ; ylabel("prob") ;
datetick yyyy keeplimits
set(gca, "box", "off") ;
axis tight
legend(pdd.uc(p), "location", "eastoutside") ;


I = sdate(s.id, [2022 7 1], [2022 7 15]) ;
area(datenum(s.id(I,:)), cumsum(s.x(I,p)))
datetick yyyy-mm-dd keeplimits
set(gca, "box", "off") ;
axis tight


j = 2 ;
mdl = [MDL NET]{j} ;
rea = "r1i1p1f1" ;
if isfield(probs.(net), m2s("MRI_ESM2_0", 0))
   s = probs.(mdl).(m2s("MRI_ESM2_0", 0)).(rea) ;
   ttl = sprintf("%s %s %s %s", toupper(sim), mdl, m2s("MRI_ESM2_0", 1), net, rea) ;
else
   s = probs.(mdl) ;
   ttl = sprintf("%s %s %s %s", toupper(sim), mdl) ;
endif


for [u mdl] = probs
   for [v gcm] = u
      if isstruct(v)
	 for [w rea] = v
	    printf("%s.%s.%s\n", mdl, gcm, rea)
	 endfor
      elseif strcmp(gcm, "id")
	    printf("%s\n", mdl)
      endif
   endfor
endfor

