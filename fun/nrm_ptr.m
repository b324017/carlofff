## usage: res = nrm_ptr (s, ref)
##
## normalize predictors
function res = nrm_ptr (s, ref)

   xm = ref(1:3) ;
   xs = ref(4:6) ;

   for j = 1 : 3
      s.x(:,j,:,:) = (s.x(:,j,:,:) - xm(j)) ./ xs(j) ;
   endfor

   res = s ;
   
endfunction
