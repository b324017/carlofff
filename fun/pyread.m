## usage: pyread (varargin)
##
##
function pyread (lbl="", D=[], varargin)

   persistent PY = false

   if ~PY
      setenv("PATH", [pwd "/penv/bin:" getenv("PATH")]) ;
      PY = true ;
   endif

   global RCNT

   if ~isempty(RCNT)
      lbl = RCNT.lbl ;
      D = RCNT.D ;
   endif
   if isnewer(sprintf("data/%s/ptr.ob", lbl), sprintf("data/%s", lbl))
      return
   endif

   pkg load pythonic

   pyexec ("import os") ;
   pyexec ("import os.path") ;
   pyexec ("from importlib import reload")

   D = int32(D) ;

   for i = 1 : nargin
      v = varargin{i} ;
      pyexec (sprintf("from python.era5 import era5_%s as %s", v, v)) ;
      pycall (v, lbl, D(1,1), D(1,2), D(1,3), D(2,1), D(2,2), D(2,3)) ;
   endfor

endfunction
