## usage: [count stride Ilon Ilat Ilev varargout] = get_cs (nc, jv, GLON, GLAT, GLEV)
##
##
function [count stride Ilon Ilat Ilev varargout] = get_cs (nc, jv, GLON, GLAT, GLEV)

   ncf = nc.Filename ;

   count = stride = Ilon = Ilat = Ilev = [] ; iv = 0 ;
   varargout = repmat({[]}, 1, nargout - 5) ;

   if ismember("rotated_pole", {nc.Variables.Name})

      for v = {nc.Variables(jv).Dimensions.Name}
	 v = v{:} ; V = toupper(v)(1:3) ;
	 try
	    eval(str = sprintf("%s = ncread(ncf, \"%s\") ;", v, v)) ;
	 catch
	    warning("no connection 2:\n%s\n", str) ;
	    return ;
	 end_try_catch
	 if exist(["G" V], "var") == 1
	    if ismember(V, {"LON" "LAT"})
	       eval(sprintf("d = max(diff(%s)) ;", v)) ;
	    else
	       d = 0 ;
	    endif
	    eval(sprintf("I%s = find(min(G%s)-d <= %s & %s <= max(G%s)+d) ;", v, V, v, v, V)) ;
	    if eval(sprintf("isempty(I%s)", v))
	       warning("incorrect model levels for %s, no selection made\n", v) ;
	       eval(sprintf("I%s = find(true(size(%s))) ;", v, v)) ;
	    endif
	    eval(sprintf("varargout{++iv} = %s(I%s) ;", v, v)) ;
	    eval(sprintf("count = [count length(I%s)] ;", v)) ;
	    eval(sprintf("stride = [stride I%s(2) - I%s(1)] ;", v, v)) ;
	 endif
      endfor

   else

      for v = {nc.Variables(jv).Dimensions.Name}
	 v = v{:} ; vv = v(end-2:end) ; V = toupper(vv) ;
	 try
	    [st out] = system(sprintf("curl --connect-timeout 5 %s.dds 2>/dev/null", ncf)) ;
	    if st ~= 0
	       warning("no connection 2:\n%s\n", ncf) ;
	       return ;
	    endif
	    eval(str = sprintf("%s = ncread(ncf, \"%s\") ;", v, v)) ;
	 catch
	    warning("no connection 2:\n%s\n", ncf) ;
	    return ;
	 end_try_catch

	 if exist(["G" V], "var") == 1
	    if ismember(V, {"LON" "LAT"})
	       eval(sprintf("d = max(diff(%s)) ;", v)) ;
	    elseif strcmp(V, "LEV")
	       d = Inf ;
	    else
	       d = 0 ;
	    endif
	    eval(sprintf("I%s = find(min(G%s)-d <= %s & %s <= max(G%s)+d) ;", vv, V, v, v, V)) ;
	    if eval(sprintf("isempty(I%s)", vv))
	       warning("incorrect model levels for %s, no selection made\n", v) ;
	       eval(sprintf("I%s = find(true(size(%s))) ;", vv, v)) ;
	    endif
	    eval(sprintf("varargout{++iv} = %s(I%s) ;", v, vv)) ;
	    eval(sprintf("count = [count length(I%s)] ;", vv)) ;
	    eval(sprintf("stride = [stride I%s(2) - I%s(1)] ;", vv, vv)) ;
	 endif
      endfor

   endif

endfunction
