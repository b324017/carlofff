function v = regrid (u, lon, lat, qres = 1.0, method = "linear", extrap = NA)

   ## usage:  v = regrid (u, lon, lat, qres = 1.0, method = "linear", extrap = NA)
   ##
   ## regrid u.x with u.lon, u.lat, using vectors lon, lat

   f = dbstack ;
   idx.type = "()" ;

   v = u ;
   v.lon = lon ; v.lat = lat ;

   N = size(u.x) ;

   nlon = length(u.lon) ; nlat = length(u.lat) ;
   N = [N(1) nlon nlat N(4:end)] ;
   u.x = reshape(u.x, N) ;
   idx.subs = repmat({":"}, 1, length(N)) ;
   [u.lon plon] = sort(u.lon) ;
   [u.lat plat] = sort(u.lat) ;
   idx.subs{2} = plon ; idx.subs{3} = plat ; 
   u.x = subsref(u.x, idx) ;
   I(1:3) = {ones(1, N(1)), nlon, nlat} ;
   for i=4:length(N)
      I{i} = ones(1, N(i)) ;
   endfor
   c = squeeze(mat2cell(u.x, I{:})) ;

   cold = mean([nanmean(diff(u.lon)) nanmean(diff(u.lat))]) ;
   cnew = mean([nanmean(diff(lon)) nanmean(diff(lat))]) ;

   opts = {"UniformOutput", false} ;
   if cold / cnew > qres
      warning("resolution too coarse, %.2f >> %.2f\n", cold, cnew) ;
      v = NaN ;
   elseif cold / cnew > 1    ## interpolation
      printf("%s> interpolating to smaller grid: %.2f --> %.2f\n", f(1).name, cold, cnew) ;
      y = parfun(@(x) interp2(u.lon, u.lat, squeeze(x)', lon, lat', method, extrap)', c, opts{:}) ;
      y = cell2mat(y) ;
      y = reshape(y, [length(lon) N(1) length(lat)]) ;
      v.x = permute(y, [2 1 3]) ;
   elseif cold / cnew < 1    ##   aggregate
      printf("%s> resampling to larger grid: %.2f --> %.2f\n", f(1).name, cold, cnew) ;
      y = parfun(@(v) resample_grid(u.lon, u.lat, v, lon, lat), c, opts{:}) ;
      y = cell2mat(y) ;
      v.x = reshape(y, [N(1) length(lon) length(lat) N(4:end)]) ;
   else
      v.x = u.x ;
   endif
   
endfunction
