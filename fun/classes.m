## usage: [c cn qc] = classes (pdd, jVAR, cfun = "count1_")
##
## classify Q0-events in pdd.x(:,jVAR,:,:) according to region
## use cfun = @sum for class counts
function [c cn qc] = classes (pdd, jVAR, cfun = "count1_")

   global REG

   w = squeeze(pdd.x(:,jVAR,:,:)) ;

   printf("class rates: %.2f %%  %.2f %%\n\n", 100 * [sum(w(:) > 0) sum(w(:) == 0)] / numel(w)) ;

   count = class_count(w, pdd.lon, pdd.lat) ;

   [c cn qc] = feval(cfun, count) ;

endfunction


## usage: [c cn qc] = count1_ (count, q=0.5)
##
##
function [c cn qc] = count1_ (count, q=0.5)

   global REG

   N = size(count) ;

   I0 = ~any(count > 0, 2) ;
   c(find(I0),1) = {"0"} ;
##   count = count(~I0,:) ;
   S = sum(count, 2) ;

   I = arrayfun(@(i) count(i,:) > q*S(i), 1 : N(1), "UniformOutput", false)' ;
   I1 = arrayfun(@(i,j) ~any(i | j{:}), I0, I, "UniformOutput", ~false) ;
   c(find(I1),1) = {"1"} ;
   c(find(~(I0|I1)),1) = cellfun(@(i) REG.name{find(i)}, I(~(I0|I1)), "UniformOutput", false) ;

   cn = unique(c) ;

   qc = arrayfun(@(uc) sum(strcmp(c, uc)), cn) / N(1) ;
   arrayfun(@(jc) printf("class count %s: %4.1f%% (%d)\n", cn{jc}, 100 * qc(jc), qc(jc)*N(1)), 1 : length(qc)) ;

endfunction

## usage: [c cn qc] = count2_ (count)
##
##
function [c cn qc] = count2_ (count)

   global REG

   N = size(count) ;
   nREG = length(REG.name) ;

   [~,J] = max(count, [], 2) ;

   I0 = @arrayfun(@(i) sum(count(i,:), 2) < 1, 1 : N(1)) ;
   I1 = ~I0 & (nREG < 2 | @arrayfun(@(i) count(i,J(i)) / sum(count(i,(1:nREG)~=J(i))) < 2, 1 : N(1))) ;

   c = REG.name(J) ;
   c(I0) = "0" ;
   c(I1) = "1" ;

   c = c(:) ;
   cn = unique(c) ;

   qc = arrayfun(@(uc) sum(strcmp(c, uc)), cn) / N(1) ;
   arrayfun(@(jc) printf("class count %s: %4.1f%% (%d)\n", cn{jc}, 100 * qc(jc), qc(jc)*N(1)), 1 : length(qc)) ;

endfunction
