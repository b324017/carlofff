## usage: [cold cnew ncf] = get_res (f, lon, lat)
##
## get spatial resolution
function [cold cnew ncf] = get_res (f, lon, lat)

   load(f) ;

   cold = mean([nanmean(diff(s.lon)) nanmean(diff(s.lat))]) ;
   cnew = mean([nanmean(diff(lon)) nanmean(diff(lat))]) ;

   if ~(isfield(s, "nc") && isfield(s.nc, "Filename"))
      warning("incorrect nc: %s\n", f) ;
##      unlink(f) ;
      cold = cnew = NaN ;
      return ;
   endif

   ncf = s.nc.Filename ;   
   if iscell(ncf) ncf = ncf{1} ; endif
   return ;

   [st out] = system(sprintf("curl --max-time 5 %s 2>/dev/null", [ncf ".dds"])) ;
   if st
      cold = cnew = NaN ;
      return ;
   endif

   try

      jv = find(arrayfun(@(v) strncmp(v.Name, "lon", 3), s.nc.Dimensions)) ;
      v = s.nc.Dimensions(jv).Name ;
      vlon = ncread(ncf, v) ;
      jv = find(arrayfun(@(v) strncmp(v.Name, "lat", 3), s.nc.Dimensions)) ;
      v = s.nc.Dimensions(jv).Name ;
      vlat = ncread(ncf, v) ;

      cold = mean([nanmean(diff(vlon)) nanmean(diff(vlat))]) ;
      cnew = mean([nanmean(diff(lon)) nanmean(diff(lat))]) ;

   catch

      cold = cnew = NaN ;

   end_try_catch

endfunction
