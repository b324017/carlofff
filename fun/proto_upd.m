## usage: [solver deploy] = proto_upd (BLD = true, ptr, pdd, res, proto, area, Dd)
##
## update proto files
function [solver deploy] = proto_upd (BLD = false, ptr, pdd, res, proto, area, Dd)

   ## solver
   fSolver{1} = sprintf("models/%s/solver_%s.tpl", proto, area) ;
   fSolver{2} = sprintf("models/%s/solver.tpl", proto) ;
   fSolver{3} = "models/solver.tpl" ;
   [Solver fSolver] = lread(fSolver) ;
   Solver = [Solver "net: \"DATA_tpl/PROTO_tpl.IND_tpl.PDD_tpl.prototxt\"\n"] ;
   Solver = [Solver "snapshot_prefix: \"DATA_tpl/PROTO_tpl.IND_tpl.PDD_tpl\"\n"] ;

   ## Data
   fData{1} = sprintf("models/%s/data.tpl", proto) ;
   fData{2} = sprintf("models/%s/data_%s.tpl", proto, area) ;
   fData{3} = "models/data.tpl" ;
   [Data fData] = lread(fData) ;

   ## net
   fNet{1} = sprintf("models/%s/net.tpl", proto) ;
   fNet{2} = sprintf("models/%s/net_%s.tpl", proto, area) ;
   fNet{3} = "models/net.tpl" ;
   [Net fNet] = lread(fNet) ;

   ## loss
   Loss = fileread(fLoss = "models/loss.tpl") ;

   Proto = strcat(Data, Net, Loss) ;

   ## deploy
   if exist(fDeploy = sprintf("models/%s/deploy.tpl", proto), "file") == 2
      Deploy = fileread(fDeploy) ;
   else
      Inp = fileread(sprintf("models/inp.tpl")) ;
      Prob = fileread(sprintf("models/prob.tpl")) ;
      Deploy = strcat(Inp, Net, Prob) ;
   endif

   solver = sprintf("%s/%s.%s.%s_solver.prototxt", Dd, proto, ptr.ind, pdd.lname) ;
   if isnewer(Lossv = strrep(solver, "solver", "solver_upd"), solver)
      solver = Lossv ;
   endif
   net = sprintf("%s/%s.%s.%s.prototxt", Dd, proto, ptr.ind, pdd.lname) ;
   deploy = sprintf("%s/%s.%s.%s_deploy.prototxt", Dd, proto, ptr.ind, pdd.lname) ;

   if ~isnewer(solver, fSolver)
      print_str(ptr, pdd, res, proto, Dd, Solver, solver) ;
   endif
   if ~isnewer(net, fData, fNet, fLoss)
      print_str(ptr, pdd, res, proto, Dd, Proto, net) ;
   endif
   if ~isnewer(deploy, fData, fNet, fDeploy)
      print_str(ptr, pdd, res, proto, Dd, Deploy, deploy) ;
   endif

endfunction


## usage: print_str (ptr, pdd, res, proto, Dd, str, ofile)
##
## print suitable string to ofile
function print_str (ptr, pdd, res, proto, Dd, str, ofile)

   global NH

   N = [size(ptr.x)(1:2) res] ;
   if length(N) < 3
      N = [N 1 1] ;
   endif

   NHs = sprintf("%02d", NH);
   Nc = numel(unique(pdd.c(:))) ;

   str = strrep(str, "PROTO_tpl", proto);
   str = strrep(str, "NCLASS_tpl", num2str(Nc));
   str = strrep(str, "IND_tpl", ptr.ind);
   str = strrep(str, "NH_tpl", NHs);
   str = strrep(str, "DATA_tpl", Dd);
   str = strrep(str, "PDD_tpl", pdd.lname);
   str = strrep(str, "CHANNEL_tpl", num2str(N(2)));
   str = strrep(str, "WIDTH_tpl", num2str(N(3)));
   str = strrep(str, "HEIGHT_tpl", num2str(N(4)));

   fid  = fopen(ofile, "wt") ;
   fprintf(fid, "%s", str) ;
   fclose(fid) ;
   printf("--> %s\n", ofile) ;

endfunction


## usage: [res f] = lread (S)
##
## fileread loop
function [res f] = lread (S)
      for s = S
	 f = s{:} ;
	 if exist(f, "file") == 2
	    res = fileread(f) ;
	    return ;
	 endif
      endfor
      error("lread> no files") ;
endfunction
