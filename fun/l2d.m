## usage: x = l2d (L)
##
## logical to double
function x = l2d (L)
   N = size(L) ;
   C = mat2cell(L, ones(N(1), 1), N(2)) ;
   x = cellfun(@(e) find(e), C) ;
   x = x - 1 ;	    # make classes 0, 1, 2,...
endfunction
