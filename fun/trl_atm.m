## usage: [SVAR fld res] = trl_atm (tfile, jSIM = 1, JVAR = [])
##
## translate atmospheric variables
function [SVAR fld, res] = trl_atm (tfile, jSIM = 1, JVAR = [])

   fid = fopen(tfile, "rt") ;
   t = textscan(fid, "%s,%s,%s", "Delimiter", ",") ;
   fclose(fid) ;

   I = ~strncmp(t{1}, "#", 1) ;
   t = {t{1}(I) t{2}(I) t{3}(I)} ;

   if isempty(JVAR) || isequal(JVAR, ":")
      SVAR = t{jSIM}' ;
      fld = t{3}' ;
   else
      SVAR = t{jSIM}'(JVAR) ;
      fld = t{3}'(JVAR) ;
   endif


endfunction
