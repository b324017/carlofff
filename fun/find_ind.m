## usage: [I J] = find_ind (id, id1, id2, d = 6/24)
##
##
function [I J] = find_ind (id, id1, id2, d = 6/24)

   t = datenum(id) ;
   t1 = datenum(id1) ;
   t2 = datenum(id2) ;

   T = t1 : d : t2 + d ;
   J = arrayfun(@(tt) lookup(T, tt), t) ;

   I = J > 0 & J < length(T) ;
   J = ind2log(J(I), length(T) - 1) ;

endfunction
