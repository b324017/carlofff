function v = fill_date (u, mode="")

   ## usage:  v = fill_date (u, mode)
   ##
   ## fill missing dates
   
   d = 1/10 ;

   if !isstruct(u)
      v = u ;
      return
   endif

   if !isfield(u, "cal")
      if isfield(u, "nc")
	 u.cal = getcal(u.nc) ;
      else
	 u.cal = "gregorian" ;
      endif
   endif

   idx.type = "()" ;
   idx.subs = repmat({":"}, 1, ndims(u.x)) ;

   nc = columns(u.id) ;
   while columns(u.id) < 3
      u.id = [u.id ones(rows(u.id), 1)] ;
   endwhile

   Ju = date2cal(u.id, u.cal) ;

   idx.subs = repmat({":"}, 1, columns(u.x)) ;
   idx.subs{1} = fixsort(Ju) ;
   Ju = Ju(idx.subs{1}) ; u.id = u.id(idx.subs{1},:) ;
   u.x = subsref(u.x, idx) ;

   I = fill_jul(Ju) ;

   v = u ;

   v.id = cell(1, nc) ;
   [v.id{:}] = cal2date(I, u.cal) ; v.id = cell2mat(v.id) ;

   if strcmp(mode, "interpol")
      v.x = interp1 (Ju, u.x, I, "nearest") ;
      return ;
   endif

   j = 0 ; Lu = nan(rows(Ju),1) ;
   for i=1:rows(Ju)
      while j < length(I) && Ju(i) - I(++j) > d
	 ##disp([i j]) ;
      endwhile
      if j == length(I) && i < rows(Ju)
	 error("fill_date: running out of values for i = %d, at %d %d %d\n", i, u.id(i,1:3)) ;
      endif
      Lu(i) = j ;
   endfor

   for [s key]=u

      if !strcmp(key, "x"), continue, endif

      N = size(s) ; N0 = prod(N(2:end)) ;
      v.(key) = nan(rows(v.id),N0) ;
      v.(key)(Lu,:) = reshape(s, rows(u.id), N0) ;
      v.(key) = reshape(v.(key), [rows(v.id), N(2:end)]) ;

   endfor

endfunction


function res = fixsort (x)

   ## usage:  res = fixdate (x)
   ##
   ## fix sorting of x

   n = length(x) ;
   res = false(n, 1) ;

   j = i = 1 ; res(i) = true ; wx = x(i) ;
   while (++j <= n)
      if (res(j) = (x(j) > wx))
	 wx = x(j) ;
      else
	 if (j == n)
	    error("fill_date: fixsort: reached eof.") ;
	 endif
      endif
   endwhile

endfunction


function I = fill_jul (varargin)

   ## usage: I = fill_jul (varargin)
   ## 
   ## fill Julian dates

   d = inf ; I0 = inf ; I1 = -inf ;
   for i = 1:nargin
      Ju = varargin{i} ;
      dJu = unique(diff(Ju)) ;
      c = arrayfun(@(x) sum(dJu==x), dJu) ;
      d = min(d, dJu(find(c == max(c))(1))) ;
      if d < 1, d = 1 / round(1/d) ; endif
      I0 = min(I0, Ju(1)) ;
      I1 = max(I1, Ju(end)) ;
   endfor

   if numel(unique(datevec(Ju)(:,2))) == 1 && numel(unique(datevec(Ju)(:,3))) == 1
      I = (datevec(I0)(1,1):datevec(I1)(1,1))' ;
      I = datenum([I ones(rows(I),2)]) ;
   else
      I = datenum(datevec(I0:d:I1)) ;
   endif

endfunction
