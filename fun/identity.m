function varargout = identity (varargin)

   ## usage:  varargout = identity (varargin)
   ##
   ## return identity

   for i = 1:nargin
      varargout{i} = varargin{i} ;
   endfor

endfunction
