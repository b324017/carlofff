## usage: res = canon_corr (u, v, jVAR)
##
##
function res = canon_corr (u, v, jVAR)

   global NH

   v.x = squeeze(v.x(:,jVAR,:,:)) ;

   for [f k] = u
      if ndims(f.x) > 3
	 f.x = squeeze(f.x(:,:,:,end)) ;
      endif
      f = agg(f, NH, @nanmean) ;
      tf = datenum(f.id) ;
      tv = datenum(v.id) ;
      tn = ceil(max(tf(1), tv(1))) ;
      tx = floor(min(tf(end), tv(end))) ;
      If = tn <= tf & tf <= tx ;
      Iv = tn <= tv & tv <= tx ;

      if 0
	 res.(k) = ndcorr(f.x(If,:,:), v.x(Iv,:,:)) ;
      else
	 res.(k) = corr(f.x(If,:,:)(:), v.x(Iv,:,:)(:)) ;
      endif

   endfor

endfunction
