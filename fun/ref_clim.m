## usage: res = ref_clim (s, ID1, ID2)
##
## calculate reference climatology
function res = ref_clim (s, ID1, ID2)

   if nargin > 1
      s = selper(s, ID1, ID2) ;
   endif
   
   if ndims(s.x) == 4
      res.xm = arrayfun(@(j) nanmean(s.x(:,j,:,:)(:)), 1:size(s.x, 2)) ;
      res.xs = arrayfun(@(j) nanstd(s.x(:,j,:,:)(:)), 1:size(s.x, 2)) ;
   else
      res.xm = nanmean(x(:)) ;
      res.xs = nanstd(x(:)) ;
   endif

endfunction
