## usage: plot_prob (s, skl, pdd, d, d0)
##
##
function plot_prob (s, skl, pdd, d, d0)

   global REG

   I1 = sdate(s.prob.id, d) ;
   I2 = sdate(pdd.id, d) ;

   clf ; hold on
   c = c2l(pdd.c) ;
   hb = bar(datenum(pdd.id(I2,:)), double(c(I2,end)), 1) ;
   set(hb, "facecolor", 0.7*[1 1 1], "edgecolor", 0.7*[1 1 1]) ;
   plot(datenum(s.prob.id(I1,:)), s.prob.x(I1,end), "k", "linewidth", 3) ;
   plot(datenum(s.prob.id(I1,:)([1 end],:)), [s.th(end).(skl) s.th(end).(skl)], "k--", "linewidth", 1) ;

   I = all(pdd.id(I2,1:length(d0)) == d0, 2) ;
   w = 0 * double(c(I2,end)) ; w(I) = 1 ;

   if c(I2,end)(I)
      fc = "r" ;
   else
      fc = "w" ;
   endif
   
   hb = bar(datenum(pdd.id(I2,:)), w, 1, "edgecolor", "r", "facecolor", fc) ;

   datetick("mmm/dd", "keeplimits") ;
   axis tight

   ds = datestr(datenum(d(1,:)), "yyyy") ;

   xlabel(sprintf("%s", ds)) ;
   ylabel("probability") ;
   
endfunction
