## usage: [h1, h2] = plot_case (ptr, pdd, s, D, d, jVAR = 1, skl, cx = 1, ax = [])
##
## plot pattern and probs for D, d
function [h1, h2] = plot_case (ptr, pdd, s, D, d, jVAR = 1, skl, cx = 1, ax = [])

   h1 = figure(1) ;
   clf ;
   plot_prob(s, skl, pdd, D, d) ;

   if isempty(ax)
      h2 = figure(2, "position", [0.7 0.4 0.3 0.6]) ;
   else
      h2 = get(ax, "parent") ;
      axes(ax) ;
   endif
   cla ; hold on ; pause(2) ;

   plot_fld(ptr, d, 3) ;

   I = sdate(pdd.ts.id, d) ;
   if any(I)
      x = pdd.ts.x(I,jVAR) ;
      h = scatter(pdd.ts.lon(I), pdd.ts.lat(I), cx*x, "r", "o", "filled") ;
      [xn in] = min(x) ; [xx ix] = max(x) ;
      hn = scatter(pdd.ts.lon(I)(in), pdd.ts.lat(I)(in), cx*x(in), "r", "o", "filled") ;
      hx = scatter(pdd.ts.lon(I)(ix), pdd.ts.lat(I)(ix), cx*x(ix), "r", "o", "filled") ;
      sn = sprintf("{\\it{E_{T,A}}} = %.0f", xn) ; sx = sprintf("{\\it{E_{T,A}}} = %.0f", xx) ;
      legend([hx hn], {sx sn}, "box", "off", "location", "northeast") ;
   endif

endfunction
