function out = structflat(struct)

   out = {};
   if isstruct(struct)
      for [v k] = struct
	 out = [out ; structflat(v)(:)];
      endfor
   else
      out = [out ; struct];
   endif

   out = cell2mat(out) ;

endfunction
