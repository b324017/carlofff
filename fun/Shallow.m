## usage: res = Shallow (ptr, pdd, PCA, TRC="CVE", mdl, SKL={"GSS" "HSS"}, varargin)
##
## calibrate and apply Shallow models
function res = Shallow (ptr, pdd, PCA = [], TRC="CVE", mdl, SKL={"GSS" "HSS"}, varargin)

   global EXP NH IMB MAXX JVAR VERBOSE

   if strcmp(class(PCA), "char") ; pkg load tisean ; endif
   init_mdl(mdl) ;	 

   Lfun = @(beta, x) beta(1,:) + x * beta(2:end,:) ;

   X = ptr.x ;
   N = size(X) ;

   if Lcv = ~isfield(pdd, "fit")
      for phs = {"CAL" "VAL"}
	 phs = phs{:} ;
	 eval(sprintf("ptr.%s = sdate(ptr.id, ptr.Y%s) ;", phs, phs)) ;
	 eval(sprintf("pdd.%s = sdate(pdd.id, ptr.Y%s) ;", phs, phs)) ;
      endfor
      prob.id = ptr.id ;
      prob.x = nan(size(prob.id, 1), numel(pdd.uc)) ;
   endif
   
   if ndims(X) > 2

      avar = trl_atm("atmvar.lst", 1, JVAR) ;
      X = reshape(X, N(1), N(2), prod(N(3:end))) ;
      PC = JPC = [] ;
      for j = 1 : size(X, 2)

	 if isequal(PCA, {})

	    E = eye(size(X, 3)) ;

	 else

	    efile = sprintf("data/%s.%02d/eof.%s.ob", EXP, NH, avar{j}) ;
	    if ~Lcv || isnewer(efile, ptr.ofile)
	       load(efile) ;
	       if VERBOSE printf("<-- %s [%d]\n", efile, columns(E)) ; endif
	    else
	       x = squeeze(X(ptr.CAL,j,:)) ;
	       switch class(PCA)
		  case "char"
		     [ev, E] = pca(x) ;
		  case {"function_handle" "double"}
		     E = gpca(x, PCA) ;
	       endswitch
	       if VERBOSE printf("[%d] --> %s\n", columns(E), efile) ; endif
	       save(efile, "E") ;
	    endif
	 endif

	 JPC = [JPC, j(ones(1, columns(E)))] ;
	 SJPC = ptr.vars(JPC) ;
	 PC = [PC, nanmult(X(:,j,:), E)] ;

      endfor

      if ~isempty(PCA) && PCA > size(PC, 2)
	 warning(sprintf("PCA = %d > %d = size(PC, 2)\n", PCA, size(PC, 2))) ;
      endif

   else

      PC = X ;
      PCA = N(2) ;

   endif

   uc = l2c(c2l(pdd.uc))' ;
   if Lcv
      Iptr = ptr.CAL | ptr.VAL ;
      Ipdd = pdd.CAL | pdd.VAL ;
      if isequal(PCA, {})
	 for j = 1 : size(X, 2)
	    printf("%10s: corr:\t%s\n", ptr.vars{j}, num2str(corr(nanmean(X(Iptr,j,:), 3), double(c2l(pdd.c(Ipdd)))))) ;
	 endfor
      else
	 printf("PC canon. corr:\t%.2f\n", nthargout(3, @canoncorr, PC(Iptr,:), c2l(pdd.c(Ipdd,:)))(1)) ;
      endif
   endif

   for phs = {"CAL" "VAL"}

      phs = phs{:} ;

      if strcmp(phs, "CAL")  th = [] ; endif

      if Lcv
	 x = PC(ptr.(phs),:) ;
	 y = pdd.c(pdd.(phs),:) ;
      else
	 x = PC ;
      endif

      if Lcv && strcmp(phs, "CAL")

	 ## oversampling
	 if isempty(IMB)
	    [xx yy] = deal(x, y) ;
	 else	 
	    sfile = sprintf("data/tmp/%s.%s.%s.ob", IMB, hash("md5", num2str(x)(:)'), hash("md5", char(y)(:)')) ;
	    if isnewer(sfile)
	       if VERBOSE printf("<-- %s\n", sfile) ; endif
	       load(sfile) ;
	    else
	       [xx yy] = oversmpl(x, y, IMB) ;
	       if VERBOSE printf("--> %s\n", sfile) ; endif
	       save(sfile, "xx", "yy") ;
	    endif
	 endif

	 ## remove missing data
	 I = all(~isnan(xx), 2) & cellfun(@(e) ~isequal(e, NaN), yy) ;
	 xx = xx(I,:) ; yy = yy(I,:) ;
	 yy = c2l(yy) ; # make 1-hot classes

	 tic ;
	 switch mdl

	    case "lasso"

	       if size(yy, 2) > 2
		  model = glm_multinomial(yy, xx) ;
	       else
		  yy = yy(:,end) ;
		  model = glm_logistic(yy, xx) ;
	       endif
	       warning("off", "Octave:nearly-singular-matrix") ;
	       pfit = penalized(model, @p_lasso, "standardize", true, "maximizer", @maxll, "solver", "adsmax") ;
	       if VERBOSE
		  printf("lasso> flag: %s\n", pfit.flag) ;
	       endif
	       AIC = goodness_of_fit("aic", pfit) ;
	       BIC = goodness_of_fit("bic", pfit, model) ;
	       CV = pfit.CV = cv_penalized(model, @p_lasso, "folds", 5, 'standardize', true, "maximizer", @maxll, "solver", "adsmax") ;
	       warning("on", "Octave:nearly-singular-matrix") ;
	       switch TRC
		  case "AIC"
		     [~, jLasso] = min(AIC) ;
		  case "BIC"
		     [~, jLasso] = min(BIC) ;
		  case "CVE"
		     [~, jLasso] = min(CV.cve) ;
		  case "PCA/2"
		     I = ~ismember((1 : size(pfit.beta, 1)), pfit.intercept) ;
		     jLasso = find(sum(pfit.beta(I,:) ~= 0, 1) >= PCA / 2)(1) ;
		  otherwise
		     jLasso = size(pfit.beta, 2) ;
	       endswitch

	       fit.par = reshape(pfit.beta(:,jLasso), size(pfit.beta, 1)/size(yy, 2), size(yy, 2), []) ;
	       fit.model = @(par, x, u) softmax(to_prob(par, x), @identity) ;
	       arrayfun(@(i) printf("Lasso: using %d predictors\n", sum(fit.par(:,i) ~= 0)), 1 : size(yy, 2)) ;

	       arrayfun(@(j) SJPC(fit.par(2:end,j) ~= 0), 1:size(yy,2), "UniformOutput", false) ;

	    case "nnet"

	       Pr = [min(xx) ; max(xx)]' ;
	       k = size(yy, 2) ;
	       SS = [7*k 3*k k] ; # based on some tests
	       if size(xx, 2) > MAXX
		  warning("trivial solution for xx(:,2) = %d\n", size(xx, 2)) ;
		  Net = newff(Pr, SS, {"tansig","logsig","purelin"}, "trainlm", "learngdm", "mse") ;
		  Net.trainParam.epochs = 1 ;
	       else
		  Net = arrayfun(@(i) newff(Pr, SS, {"tansig","logsig","purelin"}, "trainlm", "learngdm", "mse"), 1:20) ;
	       endif
	       for i = 1 : length(Net)
		  Net(i).trainParam.show = NaN ;
		  Net(i).trainParam.goal = 0.1 ;
	       endfor

	       fit.par = cell2mat(parfun(1, @(net) train(net, xx', yy')', Net, "UniformOutput", false)) ;
	       fit.model = @(par, x, u) clprob(@sim, par, x, u) ;
	       printf("NNET: using %d predictors\n", columns(xx)) ;

	    case "tree"

	       trainParams = [] ;
	       trainParamsEnsemble = m5pparamsensemble(200) ;
	       trainParamsEnsemble.getOOBContrib = false ;
	       if 0
		  trainParams = m5pparams(modelTree = true, minLeafSize = 2) ;
		  [results, residuals] = m5pcv(xx, double(yy), trainParams, [], k = 5, [], [], trainParamsEnsemble, 1) ;
		  [results1, residuals1] = m5pcv(xx, double(yy), trainParams, [], k = 5, [], 5, [], 1) ;
	       endif
	       fit.par = m5pbuild_new(xx, l2d(yy)+1, trainParams, [], trainParamsEnsemble, false) ;
	       fit.model = @(par, x, u) clprob(@m5ppredict_new, par, x, u) ;

	    case "rf"

	       D = prdataset(xx, double(yy)) ;
	       fit.par = rfLearning(D, 200) ;
	       fit.model = @(par, x, u) rfPredict(x, par)(:,end) ;

	    otherwise

	       beta0 = zeros(size(xx, 2)+1, 1) ;

	       if 0
		  opt = optimset("MaxIter", 1e5, "MaxFunEvals", 1e5, "TolFun", 1e-3, "TolX", 1e-3, "Display", "none") ;
		  [par, fval, exitflag, output] = parfun(@(j) fminsearch (@(par) sumsq(to_prob(par, xx) - yy(:,j)) / rows(yy), beta0, opt), 1 : size(yy, 2), "UniformOutput", false) ;
		  par = cell2mat(par) ;
	       else
		  opt = optimset("MaxIter", 200, "TolFun", 1e-8) ;
		  par = cell2mat(parfun(@(j) nlinfit (xx, yy(:,j), @to_prob, beta0, opt), 1 : size(yy, 2), "UniformOutput", false)) ;
	       endif
	       fit.model = @(par, x, u) softmax(to_prob(par, x), @identity) ;
	       fit.par = par ;

	 endswitch

	 printf('Execution time: %0.2f hours\n', toc/(60*60));

	 if 0
	    plot_fit (mdl, fit)
	    hgsave(sprintf("nc/%s.%02d/%s.og", REG, NH, mdl)) ;
	    print(sprintf("nc/%s.%02d/%s.svg", REG, NH, mdl)) ;
   	 endif

      endif

      if Lcv
	 prob.x(ptr.(phs),:) = feval(fit.model, fit.par, x, uc) ;
      else
	 res = feval(pdd.fit.model, pdd.fit.par, x, pdd.uc) ;
	 return ;
      endif

      lc = c2l(pdd.c(pdd.(phs),:)) ;
      printf("%s %s grand corr: %.2f\n", mdl, phs, corr(double(lc)(:), prob.x(ptr.(phs),:)(:))) ;
      o = l2d(lc) ; f = nthargout(2, @max, prob.x(ptr.(phs),:), [], 2) - 1 ;
      pc.(phs) = sum(f == o) / length(o) ;
      clear O P ;
      O(:,1,:) = l2d(c2l(pdd.c(pdd.(phs),:))) + 1 ;
      P(:,1,:) = prob.x(ptr.(phs),:) ;
      [~, rpss.(phs)] = validationRPS(O, P) ;
      [moc.(phs) th] = skl_est(prob.x(ptr.(phs),:), lc, SKL, th) ;

   endfor

   prob = selper(prob, ptr.CAL | ptr.VAL) ;

   res = struct("fit", fit, "prob", prob, "th", th, "moc", moc, "pc", pc, "rpss", rpss) ;
   res.uc = uc ;

endfunction


## usage: res = F (beta, X)
##
##
function res = F (beta, X)

   res = logicdf(beta(1) + X * beta(2:end), 0, 1) ;

   res = max(0, res) ;
   res = min(1, res) ;
   
endfunction

function [J, grad] = costFunction(theta, X, y)
%COSTFUNCTION Compute cost and gradient for logistic regression
%   J = COSTFUNCTION(theta, X, y) computes the cost of using theta as the
%   parameter for logistic regression and the gradient of the cost
%   w.r.t. to the parameters.

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;
grad = zeros(size(theta));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta.
%               You should set J to the cost.
%               Compute the partial derivatives and set grad to the partial
%               derivatives of the cost w.r.t. each parameter in theta
%
% Note: grad should have the same dimensions as theta
%

sigmoid = @(x) 1 ./ (1 + exp(-x)) ;
h = sigmoid(X*theta);
% J = (1/m)*sum(-y .* log(h) - (1 - y) .* log(1-h));
J = (1/m)*(-y'* log(h) - (1 - y)'* log(1-h));
grad = (1/m)*X'*(h - y);

% =============================================================

end


## usage: init_mdl (mdl)
##
## initialize model
function init_mdl (mdl)
   
   addpath Validation

   switch mdl
      case "lasso"
	 pkg load optim
	 source(tilde_expand("penalized/install_penalized.m"))
      case "nnet"
	 pkg load nnet parallel
      case "tree"
	 addpath ~/oct/nc/M5PrimeLab ~/oct/nc/M5PrimeLab/private ;
      case "rf"
	 addpath ~/oct/nc/prtools ~/oct/nc/bagging-boosting-random-forests/random-forests ;
      otherwise
	 pkg load optim	parallel
   endswitch
   
endfunction
