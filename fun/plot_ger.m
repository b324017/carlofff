## usage: h = plot_ger (ax = gca, GLON, GLAT, varargin)
##
## plot quadrants
function ax = plot_ger (ax = gca, GLON, GLAT, varargin)

   pkg load matgeom

   hb = borders("germany", "color", "black") ;

   nE = nargin - 3 ;
   if nE < 1 return ; endif

   for i = 1 : nE

      if nE > 1
	 ax(i) = subplot(1, nE, i) ; hold on
      else
	 axes(ax) ;
      endif

      E = varargin{i} ;
      k = double(strcmp(E, "NS")) ;
      REG = reg(E, GLON, GLAT) ;

      R = fieldnames (REG.geo) ;

      PX = circshift([REG.geo.(R{1})(1,1:2) REG.geo.(R{1})(1,2:-1:1)]', 1-k) ;
      PY = circshift([REG.geo.(R{1})(2,1:2) REG.geo.(R{1})(2,2:-1:1)]', k) ;
      P = drawPolygon(PX, PY, "color", [0.3 1 0.3], "linewidth", 3) ;
      text(mean(REG.geo.(R{1})(1,:)), mean(REG.geo.(R{1})(2,:)), R{1}) ;

      PX = circshift([REG.geo.(R{2})(1,1:2) REG.geo.(R{2})(1,2:-1:1)]', 1-k) ;
      PY = circshift([REG.geo.(R{2})(2,1:2) REG.geo.(R{2})(2,2:-1:1)]', k) ;
      P = drawPolygon(PX, PY, "color", [0.3 1 0.3], "linewidth", 3) ;
      text(mean(REG.geo.(R{2})(1,:)), mean(REG.geo.(R{2})(2,:)), R{2}) ;

   endfor

endfunction
