## usage: res = selfields (s, VAR, IND)
##
##
function res = selfields (s, VAR, IND)

   res = s ;

   L = arrayfun(@(l) l == "1", IND) ;
   ind = rstructfun(@(u) cellfun(@(c) ismember(c, u.vars), VAR), s) ;

   for [mdl smdl] = s
      if ~isstruct(mdl) continue ; endif
      for [rea srea] = mdl
	 if any(ind.(smdl).(srea) < L)
	    res.(smdl) = rmfield(res.(smdl), srea) ;
	 else
	    I = arrayfun(@(a) ismember(a, VAR(L)), res.(smdl).(srea).vars) ;
	    res.(smdl).(srea).vars = res.(smdl).(srea).vars(I) ;
	    res.(smdl).(srea).x = res.(smdl).(srea).x(:,I,:,:) ;
	 endif
      endfor
   endfor

   for [mdl smdl] = res
      if sizeof(mdl) == 0
	 res = rmfield(res, smdl) ;
      endif
   endfor

##   order predictor fields
   if isfield(res, "vars")
      p = cellfun(@(c) find(strcmp(c, VAR(L))), res.vars) ;
      res.vars = res.vars(p) ;
      res.x = res.x(:,p,:,:) ;
   else
      for [mdl smdl] = res
	 for [rea srea] = mdl
	    p = cellfun(@(c) find(strcmp(c, VAR(L))), res.(smdl).(srea).vars) ;
	    res.(smdl).(srea).vars = res.(smdl).(srea).vars(p) ;
	    res.(smdl).(srea).x = res.(smdl).(srea).x(:,p,:,:) ;
	 endfor
      endfor
   endif

   if length(fieldnames (res)) < 1
      res = [] ;
   endif

endfunction

