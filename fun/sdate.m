function [I idn J] = sdate (id, varargin)

   %% select years from an index series
   %%
   %% usage:  [I idn J] = sdate(id, varargin)

   global VERBOSE

   id0 = varargin{1} ;
   if (nargin > 2)
      id1 = varargin{1} ;
      id2 = varargin{2} ;
   elseif (size(id0, 1) > 1)
      id1 = id0(1,:) ; id2 = id0(2,:) ;
   else
      id1 = id0 ;
      id2 = id0 ;
   end 

   if (size(id, 2) == 1)
      id = [id ones(rows(id), 2)] ;
   end
   if (size(id, 2) == 2)
      id = [id 15*ones(rows(id), 1)] ;
   end

   if (size(id1, 2) == 1)
      id1 = [id1 1 1 0 0 0] ;
   elseif (size(id1, 2) == 2)
      id1 = [id1 1 0 0 0] ;
   elseif (size(id1, 2) == 3)
      id1 = [id1 0 0 0] ;
   end

   if (size(id2, 2) == 1)
      id2 = [id2 12 31 23 59 59] ;
   elseif (size(id2, 2) == 2)
      id2 = [id2 31 23 59 59] ;
   elseif (size(id2, 2) == 3)
      id2 = [id2 23 59 59] ;
   end

   t = datenum(id) ; dt = diff(t) ; ut = unique(dt)' ;
   [~,j] = max(sum(dt == ut)) ; d = dt(j) ;
   tn = (datenum(id1) : d : datenum(id2))' ;
   idn = datevec(tn)(:,1:size(id,2)) ;

   if isempty(id) || any(isnan([id1(:); id2(:)]))
      I = all(id, 2) ;
   else
      if datenum(id1(1:4)) < datenum(id(1,1:4))
	 cid = num2cell(id(1,1:4)) ;
	 cid1 = num2cell(id1) ;
	 if VERBOSE && nargin < 4
	    warning('incompatible dates: [%04d %02d %02d %02d] < [%04d %02d %02d %02d]\n', cid1{1:4}, cid{:}) ;
	 endif
      end
      if datenum(id(end,1:4)) < datenum(id2(1:4))
	 cid = num2cell(id(end,1:4)) ;
	 cid2 = num2cell(id2) ;
	 if VERBOSE && nargin < 4
	    warning('incompatible dates: [%04d %02d %02d %02d] < [%04d %02d %02d %02d]\n', cid{:}, cid2{1:4})
	 endif
      end
      I1 = date_cmp(id(1,:), id1) ; I2 = date_cmp(id2, id(end,:)) ;
      if I1 & I2 # FIXME: should include outside case
         I = date_cmp(id1, id) & date_cmp(id, id2) ;
	 J = true(sum(I), 1) ;
	 idn = id(I,:) ;
      else
	 if ~I1
	    warning("using find_ind 1:\n%d %d %d %d < %d %d %d %d\n", datevec(datenum(id1))(1:4), datevec(datenum(id(1,:)))(1:4)) ;
	 endif
	 if ~I2
	    warning("using find_ind 2:\n%d %d %d %d > %d %d %d %d\n", datevec(datenum(id2))(1:4), datevec(datenum(id(end,:)))(1:4)) ;
	 endif
	 [I J] = find_ind(id, id1, id2, d) ;
      endif
   end

end
