## usage: [lon, lat] = geo2ll (geo)
##
##
function [lon, lat] = geo2ll (geo)

   lon = [min(structfun(@(r) min(r(1,:)), geo)) max(structfun(@(r) max(r(1,:)), geo))] ;
   lat = [min(structfun(@(r) min(r(2,:)), geo)) max(structfun(@(r) max(r(2,:)), geo))] ;

endfunction
