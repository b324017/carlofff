## usage: [res weights] = Deep (ptr, pdd, res, solverstate=[], SKL= {"GSS" "HSS"}, rnd = false)
##
## calibrate and apply caffe model
function [retval weights] = Deep (ptr, pdd, res, solverstate=[], SKL= {"GSS" "HSS"}, rnd = false)

   global IMB BATCH
   addpath ~/oct/nc/MLToolbox/MeteoLab/Validation

   [Dd Dn De] = fileparts(solverstate) ;
   Dn = strsplit(Dn, ".") ; Ds = strsplit(Dd, "/") ;
   proto = Dn{1} ; area = Ds{2} ; weights = "" ;
   Sa = sprintf("models/%s/%s", proto, area) ;
   if (ie = exist(Sa)) == 0 || ~S_ISLNK(lstat(Sa).mode)
      switch ie
	 case 2
	    [st msg] = unlink(Sa) ;
	 case 7
	    [st msg] = rmdir(Sa, "s") ;
	 otherwise
      endswitch
      cwd = pwd ;
      printf("%s --> %s\n", fullfile(cwd, Dd), Sa) ;
      cd(sprintf("models/%s", proto)) ;
      symlink(sprintf("../../data/%s/%s", area, Ds{3}), area) ;
      cd(cwd) ;
   endif

   if exist(sprintf("%s/CAL_lmdb", Dd), "dir") ~= 7 & 0
      str = fileread("tools/gen_lmdb.sh") ;
      str = strrep(str, "REG_tpl", REG) ;
      tt = tempname ;
      fid  = fopen(tt, 'wt') ;
      fprintf(fid, '%s', str) ;
      fclose(fid) ;
      system(sprintf("sh %s", tt)) ;
      unlink(tt) ;
   endif

   h5f = @(pddn, phs) sprintf("%s/%s.%s.%s.txt", Dd, ptr.ind, pddn, phs) ;

   yl = c2l(pdd.c) ; # make 1-hot classes
   yc = l2d(yl) ; # convert to 0, 1, 2,...
   img = arr2img(ptr.x, res) ;

   for phs = {"CAL" "VAL"}
      phs = phs{:} ;
      eval(sprintf("ptr.%s = sdate(ptr.id, ptr.Y%s) ;", phs, phs)) ;
      eval(sprintf("pdd.%s = sdate(pdd.id, ptr.Y%s) ;", phs, phs)) ;
      if ~isnewer(of = h5f(pdd.lname, phs), ptr.ofile) || ~isempty(IMB)
	 if strcmp(phs, "CAL")
	    ## oversampling
	    [images labels] = oversmpl(img(ptr.(phs), :, :, :), pdd.c(pdd.(phs)) , IMB) ;
	    labels = l2d(c2l(labels)) ;
	 else
	    labels = yc(pdd.(phs)) ;
            images = img(ptr.(phs), :, :, :) ;
	 endif
	 
	 if 0
	    ifile = sprintf('%s/%s-images-idx3-ubyte', Dd, phs) ;
	    lfile = sprintf('%s/%s-labels-idx1-ubyte', Dd, phs) ;
	    save_bin(images, ifile, labels, lfile) ;
	 endif
	 save_hdf5(of, images, labels, rnd) ;
      endif
   endfor

   [solver deploy] = proto_upd(:, ptr, pdd, res, proto, area, Dd) ;

   if strcmp(De, ".netonly")
      retval = struct("crossentropy", NaN, "th", NaN, "skl", NaN, "prob", NaN) ;
      weights = "" ;
      return ;
   endif

   ## train model
   Solver = caffe.Solver(solver) ;
   sfx = sprintf("%s/%s.%s.%s", Dd, proto, ptr.ind, pdd.lname) ;
   pat = sprintf("%s_iter_*.solverstate", sfx) ;
   if regexp(solverstate, "\\*") && ~isempty(lst = glob(solverstate))
      state = strtrim(ls("-1t", lst{:})(1,:)) ;
   elseif exist(solverstate, "file") == 2
      state = upd_solver(solverstate, ptr.ind, pdd.lname) ;
   endif

   tic ;
   if exist("state", "var") == 0
      Solver.solve() ;
   else
      Solver.restore(state) ;
      iter = Solver.iter ;
      printf("Solver at iteration: %d\n", iter) ;
      if ~isnewer(state, solver, deploy, h5f(pdd.lname, "CAL"))
	 if BATCH
	    n = iter ;
	 else
	    n = input("retrain model?\n[],0: do nothing\nn>0: n more iterations\n") ;
	 endif
	 if ~isempty(n) && n > 0
	    printf("<-- %s\n", state) ;
	    Solver.step(n) ;
	 endif
      endif
   endif
   printf('Execution time: %0.2f hours\n', toc/(60*60));
   pause(3) ;
   if isempty(glob(pat))
      retval = weights = NaN ;
      return ;
   endif
   state = strtrim(ls("-1t", pat)(1,:)) ;
   
   ## apply model
   weights = strrep(state, "solverstate", "caffemodel") ;
   if exist(deploy, "file") == 2
      printf("<-- %s\n", deploy) ;
   else
      error("file not found: %s", deploy)
   endif
   net = caffe.Net(deploy, weights, 'test') ;

   ## number of parameters
   count = compute_caffe_parameters(net) ;

   for phs = {"CAL" "VAL"}

      phs = phs{:} ;

      if strcmp(phs, "CAL")  th = [] ; endif # first CAL then VAL !

      if 0

	 [images labels] = load_hdf5(h5f(pdd.lname, phs)) ;
	 labels_h5 = labels ;
	 N = size(labels) ;
	 prb.(phs) = nan(N) ;
	 for i = 1 : n
	    data = squeeze(images(i,1,:,:))' ;
	    phat = net.forward({single(data)}) ;
	    prb.(phs)(i,:) = phat{1} ;
	 end

      else

	 labels = yc(pdd.(phs)) ;
	 prb.(phs) = apply_net(ptr, net, res, ptr.(phs)) ;

      endif

      mdl = strsplit(strsplit(solverstate, "."){2}, "/"){3} ;
      lc = c2l(pdd.c(pdd.(phs),:)) ;
      printf("%s %s grand corr: %.2f\n", mdl, phs, corr(double(lc)(:), prb.(phs)(:))) ;
      o = yc(pdd.(phs)) ; f = nthargout(2, @max, prb.(phs), [], 2) - 1 ;
      pc.(phs) = sum(f == o) / length(o) ;
      clear O P ;
      O(:,1,:) = o ;
      P(:,1,:) = prb.(phs) ;
      [~, rpss.(phs)] = validationRPS(O, P) ;
      [moc.(phs) th] = skl_est(prb.(phs)(:,:), yl(pdd.(phs),:), SKL, th) ;

   endfor

   t = [datenum(pdd.id(pdd.CAL,:)) ; datenum(pdd.id(pdd.VAL,:))] ;
   [~, Is] = sort(t) ;
   prob.id = datevec(t(Is)) ;
   prob.x = [prb.CAL ; prb.VAL](Is,:) ; # FIXME: CAL may come after VAL!

   retval = struct("pc", pc, "th", th, "moc", moc, "rpss", rpss, "prob", prob, "count", count) ;

endfunction
