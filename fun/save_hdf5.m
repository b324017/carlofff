## usage: save_hdf5 (of, images, labels, lrnd = false, HDF5OCT = true)
##
## save to hdf5
function save_hdf5 (of, images, labels, lrnd = false, HDF5OCT = true)

   file = strrep(of, ".txt", ".h5") ;
   
   fid = fopen(of, "wt") ;
   fdisp(fid, file) ;
   fclose(fid) ;

   [st msg] = unlink(file) ;

   if lrnd
      warning("using scrambled data") ;
      labels = labels(randperm(size(labels, 1)),:) ;
   endif
   
   labels = flipdim(labels) ;
   images = flipdim(images) ;

   N = size(images) ;
   if length(N) < 3
      N = [1 1 N] ;
   endif

   if HDF5OCT

      pkg load hdf5oct

      h5create(file, '/data', N, 'Datatype', 'double') ;
      h5write(file, '/data', images) ;

      N = size(labels) ;
      h5create(file,'/label', N, 'Datatype', 'double') ;
      h5write(file,'/label', labels) ;

   else

      save("-hdf5", file, "labels", "images") ;

   endif

   printf("--> %s\n", file) ;
   
endfunction
