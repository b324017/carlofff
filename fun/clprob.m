## usage: res = clprob (f, par, x, u = {1 2})
##
## convert to categorical prediction
function res = clprob (f, par, x, u = {1 2})

   u = cell2mat(u(:)') ;

   switch f
      case {@rfPredict @m5ppredict_new}
	 w = feval(f, par, x) ;
	 J = cell2mat(arrayfun(@(j) nthargout(2, @min, abs(w(:,j) - u), [], 2), 1 : size(w, 2), "UniformOutput", false)) ;
	 res = cell2mat(arrayfun(@(j) mean(u(:)(J) == u(j), 2), 1 : length(u), "UniformOutput", false)) ;
      case @sim
	 w = parfun(@(p) feval(f, p, x')', par, "UniformOutput", false) ;
	 w = reshape(cell2mat(w), size(x, 1), [], size(w, 2)) ;
	 res = min(max(squeeze(mean(w, 3)), 0), 1) ;
      otherwise
	 w = cell2mat(parfun(@(p) feval(f, p, x')', par, "UniformOutput", false)) ;
	 J = cell2mat(arrayfun(@(j) nthargout(2, @min, abs(w(:,j) - u), [], 2), 1 : size(w, 2), "UniformOutput", false)) ;
	 res = cell2mat(arrayfun(@(j) mean(u(:)(J) == u(j), 2), 1 : length(u), "UniformOutput", false)) ;
   endswitch

endfunction
