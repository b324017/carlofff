## usage: p = softmax (x, fun = @exp, dim = 2)
##
## calculate the softmax function
function p = softmax (x, fun = @exp, dim = 2)

   if size(x, 2) < 2
      x = [1 - x, x] ;
   endif

##   x = x - max(x, [], dim) ;
   x = fun(x) ;

   p = x ./ sum(x, dim) ;
   
endfunction
