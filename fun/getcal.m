## usage: [cal, y0, j] = getcal (nc)
##
## get calendar from netcdf
function [cal, y0, j] = getcal (nc)

      j = find(strcmp({nc.Variables.Name}, "time")) ;
      k = find(strcmp({nc.Variables(j).Attributes.Name}, "calendar")) ;
      cal = nc.Variables(j).Attributes(k).Value ;

      if nargout < 2
	 return
      endif

      k = find(strcmp({nc.Variables(j).Attributes.Name}, "units")) ;
      v = nc.Variables(j).Attributes(k).Value ;

      [~, ~, ~, dstr] = regexpi(v, "\(day\|hour\)s* since .*") ;
      dstr = dstr{:} ;
      if isempty(dstr)
	 error("getcal> could not determine time coordinate") ;
      endif

      wstr = strsplit(dstr, " ", STRIP_EMPTY=true) ;
      if ~strcmp(wstr(2){:}, "since")
	 warning("could not determine calendar")
	 return
      endif

      d0 = [sscanf(wstr(3){:}, "%d-%d-%d"); 0]' ;
      if isempty(strfind(wstr(3){:}, "T"))
	 d0 = [sscanf(wstr(3){:}, "%d-%d-%d"); 0]' ;
      else
	 d0 = sscanf(wstr(3){:}, "%d-%d-%dT%d")' ;
      endif

      y0 = date2cal(d0, cal) ;

endfunction
