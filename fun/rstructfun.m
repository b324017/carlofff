## usage: res = rstructfun (fun, varargin)
##
## recursive structfun
function res = rstructfun (fun, varargin)

   global VERBOSE

   i = 0 ;
   while i+1 < nargin && isstruct(w=varargin{i+1})
      i++ ;
      s{i} = w ;
      F{i} = fieldnames (s{i}) ;
   endwhile
   V = varargin(i+1:end) ;

   if isstruct(s{1}) && (~isfield(s{1}, "id") || ~isfield(s{1}, "x"))

      if i > 1 && ~isequal(F{1}, F{2})
	 D1 = setdiff(F{1}, F{2}) ;
	 D2 = setdiff(F{2}, F{1}) ;
	 if ~isempty(D1)
	    printf("missing for 2\n") ;
	    disp(D1) ;
	 endif
	 if ~isempty(D2)
	    printf("missing for 1\n") ;
	    disp(D2) ;
	    error("") ;
	 endif
      endif

      if i > 1
	 res = cellfun(@(f) rstructfun(fun, s{1}.(f), s{2}.(f), V{:}), F{1}, "UniformOutput", false) ;
      else
	 res = cellfun(@(f) rstructfun(fun, s{1}.(f), V{:}), F{1}, "UniformOutput", false) ;
      endif
      res = cell2struct(res, F{1}) ;

   else

      res = feval(fun, s{:}, V{:}) ;

   endif

endfunction
