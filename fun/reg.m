## usage: REG = reg (EXP, GLON, GLAT)
##
## create REG from EXP
function REG = reg (EXP, GLON, GLAT)

   switch EXP
      case "1R"
	 REG.name = {"A"} ;
	 REG.geo.A = [GLON(1) GLON(2) ; GLAT(1) GLAT(2)] ;
      case "NS"
	 REG.name = {"N" "S"} ;
	 GLAT = [GLAT(1) (GLAT(1) + GLAT(2))/2 GLAT(2)] ;
	 REG.geo.N = [GLON(1) GLON(2) ; GLAT(2) GLAT(3)] ;
	 REG.geo.S = [GLON(1) GLON(2) ; GLAT(1) GLAT(2)] ;
      case "EW"
	 REG.name = {"E" "W"} ;
	 GLON = [GLON(1) (GLON(1) + GLON(2))/2 GLON(2)] ;
	 REG.geo.E = [GLON(2) GLON(3) ; GLAT(1) GLAT(2)] ;
	 REG.geo.W = [GLON(1) GLON(2) ; GLAT(1) GLAT(2)] ;
      case {"4R" "DE"}
	 REG.name = {"NE" "SE" "SW" "NW"} ;
	 GLON = [GLON(1) (GLON(1) + GLON(2))/2 GLON(2)] ;
	 GLAT = [GLAT(1) (GLAT(1) + GLAT(2))/2 GLAT(2)] ;
	 REG.geo.NE = [GLON(2) GLON(3) ; GLAT(2) GLAT(3)] ;
	 REG.geo.SE = [GLON(2) GLON(3) ; GLAT(1) GLAT(2)] ;
	 REG.geo.SW = [GLON(1) GLON(2) ; GLAT(1) GLAT(2)] ;
	 REG.geo.NW = [GLON(1) GLON(2) ; GLAT(2) GLAT(3)] ;
      case "SW"
	 REG.name = {"SW"} ;
	 GLON = [GLON(1) (GLON(1) + GLON(2))/2 GLON(2)] ;
	 GLAT = [GLAT(1) (GLAT(1) + GLAT(2))/2 GLAT(2)] ;
	 REG.geo.SW = [GLON(1) GLON(2) ; GLAT(1) GLAT(2)] ;
      otherwise
	 REG.name = {EXP} ;
	 REG.geo.(REG.name{:}) = [GLON(1) GLON(2) ; GLAT(1) GLAT(2)] ;
   endswitch

endfunction
