function varargout = parfun (varargin)

   ## usage:  varargout = parfun (varargin)
   ##
   ## wrapper for parfun

   dbs = dbstack ;

   global NCPU PARLOOP VERBOSE CHUNKSPERPROC = 1

   ncpu = NCPU ;
   popts = {} ;

   c = cell(1, nargout) ;
   if isnumeric(w = varargin{1})
      ncpu = w ;
      varargin = varargin(2:end) ;
      nargin = nargin - 1 ;
   endif
   fun = varargin{1} ; sfun = func2str(fun) ;
   if isC = iscell(varargin{2})
      pfun = "cellfun" ;
   else
      pfun = "arrayfun" ;
   endif

   if PARLOOP

      for i = 2 : nargin
	 if ischar(varargin{i}) break ; endif
	 C{i-1} = varargin{i} ;
      endfor
      N = size(C{1}) ;
      for j = 1 : prod(N)
	 w = cell(1, nargout) ;
	 if isC
	    carg = arrayfun(@(i) C{i}(:){j}, 1 : length(C), "UniformOutput", false) ;
	 else
	    carg = arrayfun(@(i) C{i}(:)(j), 1 : length(C), "UniformOutput", false) ;
	 endif
      	 [w{:}] = feval(fun, carg{:}) ;
	 for k = 1:nargout
	    c{k}{1,j} = w{k} ;
	 endfor
	 [userdata, systemdata] = memory () ;
	 if VERBOSE
	    mem = 1 - systemdata.SystemMemory.Available / systemdata.SystemMemory.Total ;
	    ##	       mem = userdata.ram_used_octave / userdata.ram_available_all_arrays ;
	    printf("looping: %s(%d): %s), mem: %.2f%%\n", dbs(1).name, j, sfun, 100*mem) ;
	 endif
      endfor
      if isC
	 for i = 1 : nargout
	    if all(cellfun(@(e) length(e) == 1, c{i}))
	       c{i} = reshape(cell2mat(c{i}), N) ;
	    else
	       c{i} = reshape(c{i}, N) ;
	    endif
	 endfor
      endif

   elseif isempty(ncpu) || ncpu < 2

      if !isempty(j = find(strcmpi(varargin, "verboselevel")))
	 varargin = [varargin(1:j-1) varargin(j+2:end)] ;
      endif
      if VERBOSE
	 printf("%s: ncpu = %d: ", pfun, ncpu) ;
	 printf("< %s", sfun) ;
	 arrayfun(@(a) printf("< %s", a.name), dbs) ;
	 printf("\n") ;
      endif
      try
	 [c{:}] = feval(pfun, varargin{:}, "ErrorHandler", @errfun) ;
      catch
	 return ;
      end_try_catch
   else

      if any(strcmp({dbs(2:end).name}, "parfun"))
	 if !isempty(j = find(strcmpi(varargin, "verboselevel")))
	    varargin = [varargin(1:j-1) varargin(j+2:end)] ;
	 endif
	 if VERBOSE
	    printf("%s: ", pfun) ;
	    printf("< %s", sfun) ;
	    arrayfun(@(a) printf(" < %s", a.name), dbs) ;
	    printf("\n") ;
	 endif
      else
	 if min(numel(varargin{2}), nproc) > 1
	    pkg load parallel
	    varargin = [ncpu, varargin] ;
	    pfun = ["par" pfun] ;
	    popts = {"ChunksPerProc", CHUNKSPERPROC} ;
	 endif
	 if VERBOSE
	    printf("%s: ", pfun) ;
	    printf("< %s", sfun) ;
	    arrayfun(@(a) printf(" < %s", a.name), dbs) ;
	    printf("\n") ;
	 endif
      endif
      try
	 [c{:}] = feval(pfun, varargin{:}, "ErrorHandler", @errfun, popts{:}) ;
      catch
	 return ;
      end_try_catch

   endif

   varargout = c ;

endfunction


## usage: varargout = errfun (S, varargin)
##
##
function varargout = errfun (S, varargin)

   warning("%d: %s\n", S.index, S.message) ;
   varargout = mat2cell(nan(1, nargout), 1, ones(1, nargout)) ;

endfunction
