## usage: [l uc] = c2l (C)
##
##
function [l uc] = c2l (C)

   if islogical(C)
      l = C ;
   else
      switch class(C{1})
	 case "double"
	    uc = arrayfun(@(u) {num2str(u)}, unique(cell2mat(C)))' ;
	    C = cellfun(@(u) {num2str(u)}, C) ;
	 otherwise
	    uc = unique(C)' ;
      endswitch
      l = cell2mat(cellfun(@(c) strcmp(uc, c), C, "UniformOutput", false)) ;
   endif

endfunction
