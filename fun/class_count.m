## usage: [c Ilon Ilat] = class_count (x=[], lon, lat)
##
## class count of x from lon, lat
function [c Ilon Ilat] = class_count (x=[], lon, lat)

   global REG

   nREG = length(REG.name) ;

   for jREG = 1 : nREG
      r = REG.name{jREG} ;
      reg = REG.geo.(r) ;
      Ilon(:,jREG) = reg(1,1) <= lon & lon <= reg(1,2) ;
      Ilat(:,jREG) = reg(2,1) <= lat & lat <= reg(2,2) ;
      if ~isempty(x)
	 c(:,jREG) = sum(sum(x(:,Ilon(:,jREG),Ilat(:,jREG)) > 0, 2), 3) ;
      endif
   endfor

endfunction
