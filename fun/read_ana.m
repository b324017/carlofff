## usage: res = read_ana (edata, glon, glat, Y = [], svar = [])
##
##
function res = read_ana (edata, glon, glat, Y = [], svar = [])

   pkg load netcdf
   global JVAR MON NH

   V = trl_atm("atmvar.lst", 1) ;
   Vf = trl_atm("atmvar.lst", 3) ;

   res = [] ;
   for j = 1 : length(V)
      v = V{j} ;
      vf = Vf{j} ;
      if ~isempty(svar) && ~strcmp(vf, svar) continue ; endif
      res.(v).id = res.(v).x = [] ;
   endfor
   if isempty(res) return ; endif

   for y = Y

      if isnewer(file = sprintf("%s/ptr.%04d.ob", edata, y))

	 printf("<-- %s\n", file) ;
	 load(file) ;

      else

	 pyread("era5", [y 1 1 ; y 12 31], "cnvenv", "vort", "water", "rh") ;

	 for v = {"cape" "cin" "cp" "div" "vort" "tcrw"}
	    v = v{:} ;
	    resy.(v).id = resy.(v).x = [] ;
	 endfor

	 for m = MON

	    F = glob(sprintf("%s/pl/cnvenv/%4d-%02d*", edata, y, m))' ;
	    for ncf = F
	       ncf = ncf{:} ;
	       printf("<-- %s\n", ncf) ;

	       id = nctime(ncf) ;
	       lon = ncread(ncf, "longitude") ; lat = ncread(ncf, "latitude") ;
	       Ilon = find(glon(1) <= lon & lon <= glon(2)) ;
	       Ilat = find(glat(1) <= lat & lat <= glat(2)) ;
	       for v = {"level" "q" "t"}
		  v = v{:} ; V = toupper(v) ;
		  if strcmp(v, "level")
		     str = sprintf("%s = ncread(\"%s\", \"%s\") ;", V, ncf, v) ;
		     eval(str) ;
		  else
		     count = [length(Ilon) length(Ilat) length(LEVEL) length(id)] ;
		     str = sprintf("x = ncread(\"%s\", \"%s\", [%d %d %d %d], [%d %d %d %d]) ;", ncf, v, [Ilon(1) Ilat(1) 1 1], count) ;
		     eval(str) ;
		     N = size(x) ;
		     C = squeeze(mat2cell(x, ones(N(1), 1), ones(N(2), 1), N(3), ones(N(4), 1))) ;
		     eval(sprintf("%s = cellfun(@(q) squeeze(q), C, \"UniformOutput\", false) ;", V)) ;
		  endif
	       endfor
	       ##	       [mucape, mucin] = cellfun(@(t,q,z) compute_CAPE_AND_CIN(t,LEVEL,q,1,0,0,z,273.15,253.15), T, Q, Z, "UniformOutput", false) ;
	       [cape cin] = parfun(@(t, q) getcape(2, flip(LEVEL), flip(t)-273.15, flip(q)), T, Q, "UniformOutput", false) ;

	       cape = cell2mat(cape) ;
	       cin = cell2mat(cin) ;

	       ## 6-hourly
	       for v = {"cape" "cin"}
		  v = v{:} ;
		  eval(sprintf("x = permute(%s, [3 1 2]) ;", v)) ;
		  s = agg(struct("id", id, "x", x), NH) ;
		  resy.(v).id = cat(1, resy.(v).id, s.id) ;
		  resy.(v).x = cat(1, resy.(v).x, s.x) ;
		  resy.(v).lon = lon(Ilon) ;
		  resy.(v).lat = lat(Ilat) ;
	       endfor
	    endfor

	    for v = {"cp" "tcrw"}

	       v = v{:} ;
	       F = glob(sprintf("%s/sl/water/%d-%02d*", edata, y, m))' ;
	       for ncf = F
		  ncf = ncf{:} ;
		  printf("<-- %s\n", ncf) ;

		  id = nctime(ncf) ;
		  lon = ncread(ncf, "longitude") ; lat = ncread(ncf, "latitude") ;
		  Ilon = find(glon(1) <= lon & lon <= glon(2)) ;
		  Ilat = find(glat(1) <= lat & lat <= glat(2)) ;
		  resy.(v).lon = lon(Ilon) ; resy.(v).lat = lat(Ilat) ;
		  count = [length(Ilon) length(Ilat) length(id)] ;
		  str = sprintf("x = ncread(\"%s\", \"%s\", [%d %d %d], [%d %d %d]) ;", ncf, v, Ilon(1), Ilat(1), 1, count) ;
		  eval(str) ;
		  ## 6-hourly
		  x = permute(x, [3 1 2]) ;
		  s = agg(struct("id", id, "x", x), NH) ;
		  ##

		  resy.(v).id = cat(1, resy.(v).id, s.id) ;
		  resy.(v).x = cat(1, resy.(v).x, s.x) ;
		  resy.(v).lon = lon(Ilon) ; resy.(v).lat = lat(Ilat) ;
	       endfor
	    endfor

	    F = glob(sprintf("%s/pl/vort/%d-%02d*", edata, y, m))' ;
	    for ncf = F
	       ncf = ncf{:} ;
	       printf("<-- %s\n", ncf) ;

	       id = nctime(ncf) ;
	       lon = ncread(ncf, "longitude") ; lat = ncread(ncf, "latitude") ;
	       Ilon = find(glon(1) <= lon & lon <= glon(2)) ;
	       Ilat = find(glat(1) <= lat & lat <= glat(2)) ;

	       count = [length(Ilon) length(Ilat) length(resy.vort.lev) length(id)] ;
	       for v = {"d" "vo"}
		  v = v{:} ;
		  str = sprintf("x = ncread(\"%s\", \"%s\", [%d %d %d %d], [%d %d %d %d]) ;", ncf, v, Ilon(1), Ilat(1), 1, 1, count) ;
		  eval(str) ;
		  ## 6-hourly
		  x = permute(x, [4 1 2 3]) ;
		  s = agg(struct("id", id, "x", x), NH) ;
		  eval(sprintf("%s = s.x ;", v)) ;
		  ##
	       endfor

	       resy.div.id = cat(1, resy.div.id, s.id) ;
	       resy.div.x = cat(1, resy.div.x, d) ;
	       resy.vort.id = cat(1, resy.vort.id, s.id) ;
	       resy.vort.x = cat(1, resy.vort.x, vo) ;

	       resy.vort.lon = lon(Ilon) ;
	       resy.vort.lat = lat(Ilat) ;
	       resy.vort.lev = ncread(ncf, "level") ;
	       resy.div.lon = lon(Ilon) ;
	       resy.div.lat = lat(Ilat) ;
	       resy.div.lev = ncread(ncf, "level") ;

	    endfor

	 endfor

	 printf("--> %s\n", file) ;
	 save(file, "resy") ;

      endif

      for j = 1 : length(V)
	 v = V{j} ; vf = Vf{j} ;
	 if ~isempty(svar) && ~strcmp(vf, svar) continue ; endif
	 res.(v).id = cat(1, res.(v).id, resy.(v).id) ;
	 res.(v).x = cat(1, res.(v).x, resy.(v).x) ;
	 res.(v).lon = resy.(v).lon ;
	 res.(v).lat = resy.(v).lat ;
	 if isfield(resy.(v), "lev")
	    res.(v).lev = resy.(v).lev ;
	 endif
      endfor

   endfor

   ## regrid
   for j = 1 : length(V)
      v = V{j} ; vf = Vf{j} ;
      if ~isempty(svar) && ~strcmp(vf, svar) continue ; endif
      res.(v) = regrid(res.(v), glon, glat) ;
      res.(v).name = v ;
   endfor

endfunction
