%% usage: s = read_klamex (fin)
%%
%%
function s = read_klamex (fin)

   global isoctave MON REG EXP CNVDUR

   if isoctave(), pkg load io ; end

   if str2num(strsplit(fin, "_"){end-1}(end-3:end)) < 2022
      dlon = 17 ; dlat = 18 ;
      Jx = [9 10 23 : 29] ;
      dt0 = 5 ; dt1 = 6 ;
   else
      dlon = 16 ; dlat = 17 ;
      Jx = [8 9 23 : 29] ;
      dt0 = 4 ; dt1 = 5 ;
   endif

   fid = fopen(fin, "rt") ;
   str = fgetl(fid) ;
   s.vars = strsplit(str, ",")(Jx) ;
   fclose(fid) ;
   
   D = real(dlmread(fin))(2:end,:) ;
   Icnv = (D(:,Jx(1)) <= CNVDUR) ;
   D = D(Icnv,:) ;

   [lat lon] = polarstereo_inv(D(:,dlon), D(:,dlat), 6370040, 0, 60, 10) ;

##   [lat lon] = pyfun( D(:,dlon), D(:,dlat), 60, 6370040, 0) ;
##   pkg load pythonic
##   pyexec("import sys")
##   pyexec("sys.path.append(r'/home/gerd/oct/nc/polarstereo-lonlat-convert-py')")
##   pyexec("from polar_convert import polar_xy_to_lonlat")
##   pyexec("polar_xy_to_lonlat(D(:,dlon), D(:,dlat), 60, 0, \"NORTH\")") ;

   [LON LAT] = geo2ll(REG.geo) ;

   I = lookup(LON, lon) == 1 & lookup(LAT, lat) == 1 ;
   D = D(I,:) ; lon = lon(I) ; lat = lat(I) ;

   t1 = datenum(num2str(D(:,dt0), '%d'), 'yyyymmddHHMM') ;
   t2 = datenum(num2str(D(:,dt1), '%d'), 'yyyymmddHHMM') ;
   d = t2 - t1 ; t = (t1 + t2) / 2 ;

   if ~issorted(t)
      [t, Is] = sort(t) ;
      D = D(Is,:) ; lon = lon(Is) ; lat = lat(Is) ;
   endif

   id = datevec(t) ;
   II = ismember(id(:,2), MON) ;

   s.id = id(II,1:4) ;
   s.x = D(II,Jx) ; s.lon = lon(II) ; s.lat = lat(II) ;
   s.reg = EXP ;
   
endfunction


## usage: [lon, lat] = pyfun (x, y)
##
##
function [lon, lat] = pyfun (x, y, true_scale_lat, re, e, hemisphere = "NORTH")

   pkg load pythonic

   pyexec("import sys")
   pyexec("sys.path.append(r'/home/gerd/oct/nc/polarstereo-lonlat-convert-py')")
   pyexec("from polar_convert import polar_xy_to_lonlat")

   w = polar_xy_to_lonlat(x, y, true_scale_lat, re, e, hemisphere) ;
   lon = w(:,1) ;
   lat = w(:,2) ;

endfunction
