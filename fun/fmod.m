## usage: fmod (file, varargin)
##
## save x to file, preserving timestamp
function fmod (file, varargin)

   for i = 1 : nargin - 1
      xn{i} = inputname(i + 1) ;
      x = varargin{i} ;
      eval(sprintf("%s = x ;", xn{i})) ;
   endfor

   t = stat(file).mtime ;
   t = strftime("%Y%m%d%H%M", localtime(t)) ;

   fmt = [repmat("%s ", 1, nargin - 1) "--> %s\n"] ;
   printf(fmt, xn{:}, file) ;
   save(file, xn{:}) ;
   system(sprintf("touch -t %s %s", t, file)) ;

endfunction
