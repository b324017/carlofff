## usage: res = to_prob (par, x)
##
## convert to probs
function res = to_prob (par, x)

   Lfun = @(beta, x) beta(1,:) + x * beta(2:end,:) ;
   sigmoid = @(z) 1 ./ (1 + exp(-z)) ;

   res = sigmoid(Lfun(par, x)) ;

endfunction
