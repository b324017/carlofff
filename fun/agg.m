## usage: v = agg (u, nh, fun=@nanmean)
##
## aggregate u to time resolution nh hours
function v = agg (u, nh, fun=@nanmean)

   ## remove extra hours
   I = arrayfun(@(h) sum(u.id(:,4) == h), unique(u.id(:,4)), "UniformOutput", ~false) ;
   J = I < 0.1 * mean(I) ; h0 = unique(u.id(:,4))(J) ;
   I = ~ismember(u.id(:,4), h0) ;
   idx.type = "()" ;
   idx.subs = repmat({":"}, 1, ndims(u.x)) ;
   idx.subs{1} = I ;
   u.id = subsref(u.id, idx) ; u.x = subsref(u.x, idx) ;

   I = arrayfun(@(d) u.id(:,3) == d, unique(u.id(:,3)), "UniformOutput", false) ;
   ND = cellfun(@(i) length(unique(u.id(i,4))), I) ;
   nd = ND == unique(ND)' ; j = nthargout(2, @max, sum(nd)) ; nd = unique(ND)(j) ;
   lh = 24 / nd ;   # original hours

   v = u ;

   if nh > lh   # aggregate

      printf("agg> aggregating (%d > %d)\n", nh, lh) ;

      lag = (nh / lh) / 2 ;
      v = shiftstruct(struct("id", u.id, "x", u.x), -lag) ;

      nq = nh / lh ;

      N = size(v.x) ;

      if rem(N(1), nq) ~= 0
	 error("length %d not divisible by %d\n", N, nq) ;
      endif

      v.id = reshape(v.id, nq, N(1)/nq, []) ;
      i = nq / 2 + 1 ;
      v.id = squeeze(v.id(i,:,1:4)) ;
      v.x = reshape(v.x, [nq N(1)/nq N(2:end)]) ;

      v.x = squeeze(feval(fun, v.x)) ;
      v.x = reshape(v.x, [N(1)/nq N(2:end)]) ;

      for [w k] = u
	 if ismember(k, {"id" "x"}) continue ; endif
	 v.(k) = u.(k) ;
      endfor

   elseif lh > nh   # interpolate

      printf("agg> interpolating (%d < %d)\n", nh, lh) ;

      n = round(lh / nh) ;
      t = datenum(u.id) ;
      dt = rem(t(1), 1) ;
      t0 = floor(t(1)) ; t1 = ceil(t(end)) ;
      t1 = t1 + mod((t1 - t0)*n - 1, n) / n ;
      ti = [t0 : 1/n : t1]' ;

      N = size(u.x) ;

      C = mat2cell(u.x, N(1), ones(N(2), 1), ones(N(3), 1)) ;

      w = cell2mat(parfun(@(c) interp1(t, c, ti, "nearest"), C, "UniformOutput", false)) ;

      v.id = datevec(ti)(:,1:4) ;
      v.x = reshape(w, [rows(w) N(2:end)]) ;

      ## correction from interpolation
      q = mean(u.x, "omitnan") ./ mean(v.x, "omitnan") ;
      v.x = v.x .* q ;

   endif

endfunction
