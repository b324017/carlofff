function res = isnewer (file, varargin)

   ## usage:  res = isnewer (file, varargin)
   ##
   ## check if file is newer than files in varargin

   global BLD VERBOSE

   res = exist(file, "file") == 2 && (nargin < 2 || isempty(BLD) || BLD == 0) ;

   if nargin < 2 return ; endif

   if ~isempty(varargin{1}) && iscell(varargin{1})
      varargin = union(varargin{1}, varargin(2:end))(:)' ;
   endif

   b = birth(file) ;

   tick = 0 ;
   j = 0 ;
   for v = varargin
      [bi fi] = birth(v{:}) ;
      if (bi > b + tick) & ~strcmp(file, fi)
	 res = false ;
	 f{++j} = fi ;
      endif ;
   endfor

   if VERBOSE && ~res && j > 0
      ls("-ltd --time-style=+%Y%m%dT%T", file, f{1}) ;
      res = true ;
   endif

endfunction


function [res f] = birth (file)

   ## usage:  [res f] = birth (file)
   ##
   ## determine age of file (newest in case of directory)

   f = file ;

   if exist(file, "file") == 0
      if strncmp(file, "http", 4)
	 res = 0 ;
      else
	 res = Inf ;
      endif
      return ;
   endif

   if isfolder(file)

      if strcmp(file(end), ".")
	 res = -Inf ;
	 return ;
      endif

      D = readdir(file)(3:end) ;

      if isempty(D)
##	 res = time ;
	 res = stat(file).mtime ;
      else
	 [res j] = max(cellfun(@(a) birth(fullfile(file, a)), D)) ;
	 f = fullfile(file, D{j}) ;
      endif
      ##res = 0 ;
      ##for f = glob([file "/*"])'
      ##	 if islink(f{:}), continue ; endif
      ##	 res = max(res, birth(f{:})) ;
      ##endfor

   else

      res = stat(file).mtime ;

   endif

endfunction
