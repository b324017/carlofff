function [beta,fval,pen,Lgrad,flag] = maxll(model,beta,lambda,penalty,opts)
## MAXLL(model,beta,lambda,penalty,opts) maximizes the penalized
## likelihood
##
##   logL(model,beta)/nobs-sum(wt.*penalty(lambda,beta))

   nobs = property(model,'nobs');
   wt = lambda*opts.penaltywt;

   function f = maxll_ (beta)
      [logl m D V J] = scoring(model, beta) ;
      pen  = sum(wt .* call_penalty(penalty, '', beta, lambda, opts)) ;
      f = logl/nobs - pen ;
   endfunction

   switch opts.solver
      case "adsmax"
	 stopit = [1e-2 Inf Inf 0 0] ;
	 beta = adsmax(@maxll_, beta, stopit) ;
	 flag  = 'maxll';
      case "powell"
	 o = optimset("MaxIter", 100, "MaxFunEvals", 1000, "Display", "none") ;
	 [beta, obj_value, convergence] = powell(@maxll_, beta, o) ;
	 flag  = sprintf("convergence = %d", convergence) ;
   endswitch

   [logl m D V J] = scoring(model, beta) ;
   wt = lambda * opts.penaltywt ;
   pen  = sum(wt .* call_penalty(penalty, '', beta, lambda, opts)) ;
   fval = logl/nobs - pen ;
   Lgrad = J' * (D .* m/nobs) ; 

endfunction
