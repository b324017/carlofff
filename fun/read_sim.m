## usage: res = read_sim (D, sim, fld, glon, glat, qres = 1.0)
##
## read fld from simulations sim from directory d
function res = read_sim (D, sim, fld, glon, glat, qres = 1.0)

   pkg load io

   switch fld
      case {"cnvenv"}
	 D = sprintf("%s/HighRes", D) ;
	 d = sprintf("%s/%s*%s*.ob", D, sim, fld) ;
      case {"vort"}
	 D = sprintf("%s/HighRes", D) ;
	 d = sprintf("%s/%s*%s*.ob", D, sim, fld) ;
      otherwise
	 D = sprintf("%s/Day", D) ;
	 d = sprintf("%s/%s*%s*.ob", D, sim, fld) ;
   endswitch
   G = {"nc" "lat" "lon" "id" "cal"} ;

   F = glob(d)' ;
   ## limit to r[1-5] realizations
   I = cellfun(@(c) ~isempty(regexp(c, ".*r[1-5]i1.*")), F) ;
   F = F(I) ;
   printf("--> %s\n", cfile = sprintf("%s/%s_%s.lst", D, sim, fld)) ;
   cell2csv(cfile, F') ;

   for f = F

      f = f{:} ;

      [cold cnew ncf] = get_res(f, glon, glat) ;
      if cold / cnew > qres
	 warning("resolution %.2f >> %.2f for %s\n", cold, cnew, ncf) ;
	 continue ;
      endif
      if isnan(cold)
	 warning("file not found: %s\n", ncf) ;
	 continue ;
      endif

      Fs = strsplit(f, ".") ;
      [Da fn] = fileparts(f) ;
      [~] = mkdir(Da = [Da "/grd"]) ;
      s_id = strrep(Fs{2}, "-", "_") ;
      v_id = Fs{3} ;
      if isempty(regexp(v_id, "r[1-5]i*"))
	 continue ;
      endif

      if isnewer(fn = sprintf("%s/%s.ob", Da, fn), f)

	 printf("<-- %s\n", fn) ;
	 load(fn) ;

      else

	 if ~isnewer(f)
	    warning("file not found: %s\n", f) ;
	    continue ;
	 endif
	 printf("<-- %s\n", f) ;
	 load(f) ;
	 if isfield(s, "lev")
	    idx.type = "()" ; idx.subs = {":",":",":",":"} ;
	    if issorted(s.lev)
	       idx.subs{2} = "end" ;
	    else
	       idx.subs{2} = 1 ;
	    endif
	    s = rmfield(s, "lev") ;
	    save(f, "s") ;
	 endif

	 for [v k] = s
	    if ~ismember(k, G)
	       sgrd.(k).id = s.id ;

	       ## select lowest level
	       if ndims(s.(k)) > 3
		  s.(k) = squeeze(subsref(s.(k), idx)) ;
	       endif

	       sgrd.(k).x = s.(k) ;
	       sgrd.(k).lon = s.lon ;
	       sgrd.(k).lat = s.lat ;
	       sgrd.(k) = regrid(sgrd.(k), glon, glat, qres) ;
	       if ~isstruct(sgrd.(k))
		  sgrd = rmfield(sgrd, k) ;
		  continue ;
	       endif
	       sgrd.(k).name = k ;
	       sgrd.(k).nc = s.nc ;
	       sgrd.(k).cal = getcal(sgrd.(k).nc) ;

	       sgrd.(k) = sort_id(sgrd.(k)) ;
	       sgrd.(k) = fill_date(sgrd.(k)) ;

	    endif
	 endfor

	 printf("--> %s\n", fn) ;
	 save(fn, "sgrd") ; 

      endif

      res.(s_id).(v_id) = sgrd ;

   endfor

endfunction


## usage: v = sort_id (u)
##
##
function v = sort_id (u)

   v = u ;

   if iscell(u.nc.Filename)
      f = u.nc.Filename{1} ;
   else
      f = u.nc.Filename ;
   endif

   idx.type = "()" ;
   idx.subs = repmat({":"}, 1, ndims(v.x)) ;

   [~, Is] = sort(datenum(v.id)) ;

   if any(diff(Is) < 0)
      warning("not sorted: %s ...\n", f) ;
      v.id = v.id(Is,:) ;
      idx.subs{1} = Is ;
      v.x = subsref(v.x, idx) ;
   endif

   [~, J] = unique(datenum(v.id)) ;
   if length(J) ~= size(v.id, 1)
      warning("double entry: %s ...\n", f) ;
      idx.subs{1} = J ;
      v.id = v.id(J,:) ;
      v.x = subsref(v.x, idx) ;
   endif

endfunction
