function t = selper (s, varargin)

   ## usage:  t = selper (s, varargin)
   ##
   ## select period of structure s

   t = s ;

   if isstruct(s)
      if isfield(s, "id")
	 if nargin > 2
	    [I id J] = sdate(s.id, varargin{:}) ;
	 else
	    I = varargin{1} ;
	    J = true(sum(I), 1) ;
	    id = s.id(I,:) ;
	 endif
      endif
      for [v k] = s
	 if isstruct(v) && isfield(v, "id") && isfield(v, "x")
	    t.(k) = selper(v, varargin{:}) ;
	 else
	    switch k
	       case "id"
		  t.(k) = id ;
	       case "x"
		  t.(k) = selper(v, I, id, J) ;
	       otherwise
		  t.(k) = v ;
	    endswitch
	 endif
      endfor
      return ;
   endif

   if nargin > 3
      I = varargin{1} ;
      id = varargin{2} ;
      J = varargin{3} ;
      N = size(s) ;
      s = reshape(s, N(1), prod(N(2:end))) ;
      t = nan(length(id), prod(N(2:end))) ;
      t(J,:) = s(I,:) ;
      t = reshape(t, [size(t,1), N(2:end)]) ;
   endif

endfunction
