## usage: pdd = selpdd (PDD, lon, lat, jVAR, Q0)
##
## select predictands
function pdd = selpdd (PDD, lon, lat, jVAR, Q0)

   global MON
   
   switch PDD
      case {"cape" "cp"}
	 pdd = ptr2pdd(s) ;
      case "regnie"
	 R0 = 10 ; # from climate explorer
	 rfile = "https://opendata.dwd.de/climate_environment/CDC/grids_germany/daily/regnie" ;
	 pdd = regnie(rfile, 2001, 2020, R0) ;
      case "CatRaRE"
	 rfile = "nc/StaedteDWD/CatRaRE_2001_2022_W3_Eta_v2023_01.csv" ;
	 pdd = read_klamex(rfile) ;
	 ##      pdd.x = pdd.x(:,3) ; # use RRmean
	 ##      pdd.x = pdd.x(:,5) ; # use Eta
	 ##      pdd.x = pdd.x(:,6) ; # use RRmax
      case {"WEI" "xWEI"}
	 rfile = "data/hpe-catalog/HPEs_2001_2022_xwei_v1.0.csv" ;
	 pdd = read_WEI(rfile) ;
      otherwise
	 error("unknown predictand: %s", PDD) ;
   endswitch
   printf("<-- %s\n", rfile) ;

   x0 = quantile(pdd.x(:,jVAR), Q0) ;
   I = pdd.x(:,jVAR) > x0 ;
   pdd.id = pdd.id(I,:) ; pdd.x = pdd.x(I,:) ;
   pdd.lon = pdd.lon(I) ; pdd.lat = pdd.lat(I) ;

   pdd.name = PDD ;
   org = pdd ;
   pdd = togrid(pdd, lon, lat) ;
   pdd = unifid(pdd, MON) ;
   pdd.ts = org ;
   
   ## plot pdd statistics
   if 0
      for jVAR = JVAR
	 plot_pdd(pdd, jVAR) ;
      endfor
   endif

   pdd.x(isinf(pdd.x)) = 0 ;
   if 0
      plot_hrs(pdd) ;
   endif

endfunction
