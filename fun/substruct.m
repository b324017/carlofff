function v = substruct(u, keys)

   if isstruct(u)

      K = fieldnames(u)' ; 
      KI = intersect(K, keys, "stable") ;
      KD = setdiff(K, KI, "stable") ;

      if numel(KI) > 0
	 for k = KI
	    k = k{:} ;
	    v.(k) = substruct(u.(k), keys) ;
	 endfor
      else
	 for k = K
	    k = k{:} ;
	    v.(k) = substruct(u.(k), keys) ;
	 endfor
      endif

   else

      v = u ;

   endif

endfunction
