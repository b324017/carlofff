## usage: v = shiftstruct (u, lag)
##
## shift stucture by lag
function v = shiftstruct (u, lag)

   if lag == 0
      v = u ;
      return
   endif

   N = size(u.x) ;
   idx.type = "()" ; idx.subs = repmat({":"}, 1, length(N)) ;

   t = datenum(u.id) ;
   dt = mean(diff(t)) ;

   if lag < 0
      tw = t(1) + dt * (lag : 1 : -1) ;
      t = [tw' ; t(1:N(1)+lag)] ;
      xw = nan([-lag N(2:end)]) ;
      idx.subs{1} = 1 : N(1)+lag ;
      x = subsref(u.x, idx) ;
      v.x = cat(1, xw, x) ;
   else
      tw = t(end) + dt * (1 : lag) ;
      t = [t(lag+1:end) ; tw'] ;
      xw = nan([lag N(2:end)]) ;
      idx.subs{1} = 1 : N(1)-lag ;
      x = subsref(u.x, idx) ;
      v.x = cat(1, x, xw) ;
   endif

   v.id = datevec(t)(:,1:4) ;

endfunction
