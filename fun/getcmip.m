## usage: [res fail] = getcmip (del = "", ncf, GLON, GLAT, GLEV, CX)
##
## get timeseries
function [res fail] = getcmip (del = "", ncf, GLON, GLAT, GLEV, CX)

   global MOIST

   [fd, fn] = fileparts(ncf) ;
   rvar = strsplit(fn, "_"){1} ;
   fail = true ;

   switch rvar
      case "hus"
	 tGLEV = [0 1] ;
	 if strcmp(MOIST, "pw")
	    res = struct("nc", NaN, "lon", NaN, "lat", NaN, "id", NaN, "pw", NaN) ;
	 else
	    res = struct("nc", NaN, "lon", NaN, "lat", NaN, "id", NaN, "cape", NaN, "cin", NaN) ;
	 endif
      case "ua"
	 tGLEV = GLEV ;
	 res = struct("nc", NaN, "lon", NaN, "lat", NaN, "lev", NaN, "id", NaN, "div", NaN, "vort", NaN) ;
      otherwise
	 tGLEV = GLEV ;
	 res = struct("nc", NaN, "lon", NaN, "lat", NaN, "id", NaN, rvar, NaN) ;
   endswitch

   y = str2num(char(strsplit(strsplit(fn, "_"){end}, "-"))(:,1:4)) ;
   if y(2) < 1960 | y(1) > 2100
      return ;
   endif

   [~] = mkdir(tdir = sprintf("data/tmp/%s", rvar)) ;
   fd = strsplit(fd, "/"){end} ;

   if strcmp(rvar, "hus") && strcmp(MOIST, "pw")
      tfile = [tdir "/" fn ".pw_" fd ".ob"] ;
   else
      tfile = [tdir "/" fn "_" fd ".ob"] ;
   endif
   if isnewer(tfile)
      if strcmp(del, "del")
	 delete(tfile) ;
      else
	 printf("<-- %s\n", tfile) ;
	 load(tfile) ;
	 fail = false ;
	 return
      endif
   endif

   [st out] = system(sprintf("curl --max-time 5 %s 2>/dev/null", [ncf ".dds"])) ;
   if st
      warning("no connection: %s\n", ncf) ;
      return ;
   endif
##   out = webread([ncf ".dds"], "Timeout", "5") ;
   if ~(length(out) > 6 && strncmp(out, "Dataset", 7)) && 0
      warning("getcmip> problem 1: %s\n", ncf) ;
      return ;
   endif
   try
      nc = ncinfo(ncf) ;
   catch
      warning("getcmip> problem 2: %s\n", ncf) ;
      return ;
   end_try_catch

   jv = find(cellfun(@(c) ~isempty(c), regexp({nc.Variables.Name}, rvar))) ;
   if isempty(jv)
      warning("mismatch: %s in %s\n", rvar, ncf) ;
      return ;
   endif
   if ismember(rvar, {"hus" "ua"}) && length(nc.Variables(jv).Size) < 4 # zonal wind
      warning("zonal wind: %s\n", ncf) ;
      return ;
   endif
   J = cellfun(@(c) isempty(c), regexp({nc.Variables(jv).Dimensions.Name}, "(lon|lat|time)"), "UniformOutput", false) ;
   j = find(cell2mat(J)) ;
   vn = nc.Variables(jv).Name ;
   if ismember(vn, {"ta" "hus"}) && nc.Variables(j).Size < 20
      warning("too few levels: %s\n", ncf) ;
      return
   endif

   [res fail] = getcmip_(nc, jv, GLON, GLAT, tGLEV, CX) ;

   if ~fail
      printf("--> %s\n", tfile) ;
      save(tfile, "res") ;
   endif

endfunction


## usage: [s fail] = getcmip_ (nc, jv, GLON, GLAT, GLEV, CX = Inf)
##
## read cmip nc's
function [s fail] = getcmip_ (nc, jv, GLON, GLAT, GLEV, CX = Inf)

   global VERBOSE DEBUG MOIST
   pkg load netcdf

   N = nc.Variables(jv).Size ;
   vn = nc.Variables(jv).Name ;
   fail = true ;

   switch vn
      case "hus"
	 if strcmp(MOIST, "pw")
	    s = struct("nc", NaN, "lon", NaN, "lat", NaN, "id", NaN, "pw", NaN) ;
	 else
	    s = struct("nc", NaN, "lon", NaN, "lat", NaN, "id", NaN, "cape", NaN, "cin", NaN) ;
	 endif
      case "ua"
	 s = struct("nc", NaN, "lon", NaN, "lat", NaN, "lev", NaN, "id", NaN, "div", NaN, "vort", NaN) ;
      otherwise
	 s = struct("nc", NaN, "lon", NaN, "lat", NaN, "id", NaN) ;
   endswitch

   if DEBUG
      ncf = sprintf("%s#log&show=fetch", nc.Filename) ;
   else
      ncf = nc.Filename ;
   endif

   [count stride Ilon Ilat Ilev lon lat lev] = get_cs(nc, jv, GLON, GLAT, GLEV) ;
   if isempty(Ilon) || isempty(Ilat)
      warning("Ilon or Ilat empty: %s\n", ncf) ;
      return ;
   endif
   pause(3) ;

   count = [count N(end)] ;
   stride = [stride 1] ;

   nx = sizeof(1) * prod(count) ;
   c = floor(50000000 / (sizeof(1) * prod(count(1:end-1)))) ;
   c = min(c, CX) ;

   v = nan(count) ;
   try
      id = nctime(nc) ;
   catch
      printstruct(s, "nlevel", 4)
      printf("getcmip_> problem 3:\n%s\n", ncf) ;
      return ;
   end_try_catch

   printf("<-- %s\n", ncf) ;

   switch vn
      case "hus"
	 if strcmp(MOIST, "pw")
	    [s fail] = pw_(vn, nc, N, Ilon, Ilat, count, stride, c) ;
	 else
	    [s fail] = cnvenv_(vn, nc, N, Ilon, Ilat, count, stride, c) ;
	 endif
      case "ua"
	 [s fail] = vort_(vn, nc, N, Ilon, Ilat, Ilev, count, stride, c) ;
	 s.lev = lev ;
      otherwise
	 [s fail] = def_(vn, nc, N, Ilon, Ilat, count, stride, c) ;
   endswitch
   s.lon = lon ; s.lat = lat ; s.id = id ;

endfunction


## usage: [s fail] = cnvenv_ (vn, nc, N, Ilon, Ilat, count, stride, c)
##
##
function [s fail] = cnvenv_ (vn, nc, N, Ilon, Ilat, count, stride, c)

   global VERBOSE DEBUG
   fail = true ;

   if DEBUG
      ncf = sprintf("%s#log&show=fetch", nc.Filename) ;
   else
      ncf = nc.Filename ;
   endif

   s.cape = s.cin = NaN ; s.nc = nc ;

   plev = extract_plev(vn, nc, Ilon, Ilat, count, stride) ;

   cape = cin = [] ; k = 1 ;
   while k < N(end)

      c = min(c, N(end) - k + 1) ;
      str = @(vn, ncf) sprintf("%s = ncread(\"%s\", \"%s\", [%d %d %d %d], [%d %d %d %d], [%d %d %d %d]) ;", vn, ncf, vn, Ilon(1), Ilat(1), 1, k, count(1:3), c, stride) ;
      [fd, fn] = fileparts(ncf) ;
      if exist(tfile = sprintf("data/tmp/%s/%s.%d.nc", vn, fn, k)) == 2
	 load(tfile) ;
      else
	 if VERBOSE
	    printf("%s\n", str(vn, ncf)) ;
	 endif
	 try
	    eval(str(vn, ncf)) ;
	    TC = true ;
	 catch
	    TC = false ;
	 end_try_catch
	 if ~TC
	    warning("file not found:\n%s\n", str(vn, ncf)) ;
	    return
	 endif
	 save(tfile, vn) ;
      endif
      if strcmp(vn, "hus") va = "ta" ; endif
      if strcmp(vn, "ta") va = "hus" ; endif
      [~] = mkdir(tdir = sprintf("data/tmp/%s", va)) ;
      if exist(tfile = sprintf("data/tmp/%s/%s.%d.nc", va, fn, k)) == 2
	 load(tfile) ;
      else
	 try
	    eval(str(va, strrep(ncf, vn, va))) ;
	    TC = true ;
	 catch
	    TC = false ;
	 end_try_catch
	 if ~TC
	    warning("file not found:\n%s\n", str(va, strrep(ncf, vn, va))) ;
	    return
	 endif
	 save(tfile, va) ;
      endif

      if all(isnan(hus(:))) || all(isnan(ta(:)))
	 warning("all data undefined:\n%s\n", ncf) ;
	 return ;
      endif

      n = size(ta) ;
      Q = mat2cell(hus, ones(n(1),1), ones(n(2),1), n(3), ones(n(4),1)) ;
      T = mat2cell(ta - 273.15, ones(n(1),1), ones(n(2),1), n(3), ones(n(4),1)) ;

      if ndims(plev) > 2 # from sigma levels
	 P = plev(:, :, :, k:k+c-1) ;
	 P = mat2cell(P, ones(n(1),1), ones(n(2),1), n(3), ones(n(4),1)) ;
	 [wcape wcin] = cellfun(@(p, t, q) getcape(2, p, t, q), P, T, Q, "UniformOutput", false) ;
      else
	 [wcape wcin] = cellfun(@(t, q) getcape(2, plev, t, q), T, Q, "UniformOutput", false) ;
      endif
      wcape = squeeze(cell2mat(wcape)) ; wcin = squeeze(cell2mat(wcin)) ;
      cape = cat(length(N)-1, cape, wcape) ;
      cin = cat(length(N)-1, cin, wcin) ;
      k += c ;
      pause(3) ;

   endwhile

   s.cape = flipdim(cape) ; s.cin = flipdim(cin) ;
   fail = false ;

endfunction


## usage: [s fail] = pw_ (vn, nc, N, Ilon, Ilat, count, stride, c)
##
##
function [s fail] = pw_ (vn, nc, N, Ilon, Ilat, count, stride, c)

   global VERBOSE DEBUG
   fail = true ;

   if DEBUG
      ncf = sprintf("%s#log&show=fetch", nc.Filename) ;
   else
      ncf = nc.Filename ;
   endif

   s.pw = NaN ; s.nc = nc ;

   plev = extract_plev(vn, nc, Ilon, Ilat, count, stride) ;

   pw = [] ; k = 1 ;
   while k < N(end)

      c = min(c, N(end) - k + 1) ;
      str = @(vn, ncf) sprintf("%s = ncread(\"%s\", \"%s\", [%d %d %d %d], [%d %d %d %d], [%d %d %d %d]) ;", vn, ncf, vn, Ilon(1), Ilat(1), 1, k, count(1:3), c, stride) ;
      [fd, fn] = fileparts(ncf) ;
      if exist(tfile = sprintf("data/tmp/%s/%s.%d.nc", vn, fn, k)) == 2
	 load(tfile) ;
      else
	 if VERBOSE
	    printf("%s\n", str(vn, ncf))
	 endif
	 try
	    eval(str(vn, ncf)) ;
	    TC = true ;
	 catch
	    TC = false ;
	 end_try_catch
	 if ~TC
	    warning("file not found:\n%s\n", str(vn, ncf)) ;
	    return
	 endif
	 save(tfile, vn) ;
      endif

      if all(isnan(hus(:)))
	 warning("all data undefined:\n%s\n", ncf) ;
	 return ;
      endif

      n = size(hus) ;
      Q = mat2cell(hus, ones(n(1),1), ones(n(2),1), n(3), ones(n(4),1)) ;

      wpw = cellfun(@(q) sum(q), Q, "UniformOutput", false) ;
      wpw = squeeze(cell2mat(wpw)) ;
      pw = cat(length(N)-1, pw, wpw) ;
      k += c ;
      pause(3) ;

   endwhile

   s.pw = flipdim(pw) ;
   fail = false ;

endfunction


## usage: [s fail] = vort_ (vn, nc, N, Ilon, Ilat, Ilev, count, stride, c)
##
##
function [s fail] = vort_ (vn, nc, N, Ilon, Ilat, Ilev, count, stride, c)

   global VERBOSE DEBUG
   fail = true ;

   if DEBUG
      ncf = sprintf("%s#log&show=fetch", nc.Filename) ;
   else
      ncf = nc.Filename ;
   endif

   s.div = s.vort = NaN ; s.nc = nc ;

   div = vort = [] ; k = 1 ;
   while k < N(end)
      c = min(c, N(end) - k + 1) ;
      str = sprintf("%s = ncread(\"%s\", \"%s\", [%d %d %d %d], [%d %d %d %d], [%d %d %d %d]) ;", vn, ncf, vn, Ilon(1), Ilat(1), Ilev(1), k, count(1:3), c, stride) ;
      if VERBOSE
	 printf("%s\n", str)
      endif
      try
	 eval(str) ;
	 if strcmp(vn, "ua") va = "va" ; endif
	 if strcmp(vn, "va") va = "ua" ; endif
	 str = strrep(str, vn, va) ;
	 eval(str) ;
	 TC = true ;
      catch
	 TC = false ;
      end_try_catch
      if ~TC
	 warning("vort_> problem 1:\n%s\n", str) ;
	 return
      endif
      n = size(ua) ;
      U = squeeze(mat2cell(ua, n(1), n(2), ones(n(3), 1), ones(n(4), 1))) ;
      V = squeeze(mat2cell(va, n(1), n(2), ones(n(3), 1), ones(n(4), 1))) ;

      wdiv = cellfun(@(u,v) divergence(u,v), U, V, "UniformOutput", false) ;
      wdiv = cell2mat(wdiv) ;
      wdiv = reshape(wdiv, n([1 3 2 4])) ;
      wdiv = permute(wdiv, [1 3 2 4]) ;
      div = cat(length(N), div, wdiv) ;

      wvort = cellfun(@(u,v) curl(u,v), U, V, "UniformOutput", false) ;
      wvort = cell2mat(wvort) ;
      wvort = reshape(wvort, n([1 3 2 4])) ;
      wvort = permute(wvort, [1 3 2 4]) ;
      vort = cat(length(N), vort, wvort) ;

      k += c ;
      pause(3) ;
   endwhile

   s.div = flipdim(div) ; s.vort = flipdim(vort) ;
   fail = false ;

endfunction


## usage: [s fail] = def_ (vn, nc, N, Ilon, Ilat, count, stride, c)
##
##
function [s fail] = def_ (vn, nc, N, Ilon, Ilat, count, stride, c)

   global VERBOSE DEBUG
   fail = true ;

   if DEBUG
      ncf = sprintf("%s#log&show=fetch", nc.Filename) ;
   else
      ncf = nc.Filename ;
   endif

   s.(vn) = NaN ; s.nc = nc ;

   v = [] ; k = 1 ;
   while k < N(end)
      c = min(c, N(end) - k + 1) ;
      str = sprintf("wv = ncread(\"%s\", \"%s\", [%d %d %d], [%d %d %d], [%d %d %d]) ;", ncf, vn, Ilon(1), Ilat(1), k, count(1:end-1), c, stride) ;
      if VERBOSE
	 printf("%s\n", str)
      endif
      try
	 eval(str) ;
	 TC = true ;
      catch
	 TC = false ;
      end_try_catch
      if ~TC
	 warning("def_> problem 1:\n%s\n", str) ;
	 return
      endif
      if all(isnan(wv(:)))
	 warning("def_> all data undefined:\n%s\n", str) ;
	 return ;
      endif

      v = cat(length(N), v, wv) ;
      k += c ;
      pause(3) ;
   endwhile

   s.(vn) = flipdim(v) ;
   fail = false ;

endfunction


## usage: plev = extract_plev (nc, vn, Ilon, Ilat, count, stride)
##
## extract pressure from model levels
function plev = extract_plev (vn, nc, Ilon, Ilat, count, stride)

   ncf = nc.Filename ;

   jv = find(cellfun(@(c) strcmp(c, vn), {nc.Variables.Name})) ;
   vlev = nc.Variables(jv).Dimensions(3).Name ;
   jl = find(cellfun(@(c) strcmp(c, vlev), {nc.Variables.Name})) ;

   if strcmp(pu = keyfind(nc.Variables(jl).Attributes, "units"), "1")  # sigma levels

      formula = keyfind(nc.Variables(jl).Attributes, "formula") ;
      formula = strsplit(formula, "="){2} ;
      formula_terms = keyfind(nc.Variables(jl).Attributes, "formula_terms") ;
      if isempty(formula_terms)
	 formula_terms = keyfind(nc.Variables(jl).Attributes, "formula_term") ;
      endif
      formula_terms = strsplit(formula_terms, "[ :]", "delimitertype", "regularexpression") ;
      formula_terms = reshape(formula_terms, 2, []) ;
      for j = 1 : size(formula_terms, 2)
	 formula = strrep(formula, formula_terms{1,j}, formula_terms{2,j}) ;
      endfor
      formula = strrep(formula, "*", ".*") ;

      for v = formula_terms(2,:)
	 j = eval(sprintf("find(cellfun(@(c) strcmp(c, \"%s\"), {nc.Variables.Name})) ;", v{:})) ;
	 if length(nc.Variables(j).Size) > 2
	    eval(sprintf("%s(1,:,:,:) = ncread(ncf, \"%s\", [Ilon(1) Ilat(1) 1], count([1 2 4]), stride([1 2 4])) ;", v{:}, v{:})) ;
	 else
	    eval(sprintf("%s = ncread(ncf, \"%s\") ;", v{:}, v{:})) ;
	 endif
      endfor
      plev = eval(formula) ;
      plev = permute(plev, [2 3 1 4]) ;


   else

      plev = ncread(ncf, vlev) ;

   endif

   if ~strcmp(pu, "hPa") plev = plev / 100 ; endif ; # convert to hPa

endfunction
