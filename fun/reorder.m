## usage: p = reorder (C, names)
##
##
function p = reorder (C, names)

   if numel(C) < 3
      p = 1 : 2 ;
      return ;
   endif

   D = [{"0"} names {"1"}] ;
   p = cellfun(@(d) find(strcmp(d, C)), D) ;

endfunction
