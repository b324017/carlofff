## usage: val = keyfind (S, key)
##
## find value to key in S
function val = keyfind (S, key)

   j = find(w = arrayfun(@(s) strcmp(s.Name, key), S)) ;

   if isempty(j)
      val = [] ;
   else
      val = S(j).Value ;
   endif

endfunction
