## usage: c = l2c (L)
##
## transform 1-hot to classes
function c = l2c (L)

   N = size(L) ;

   I0 = all(~L, 2) ;
   L = mat2cell(L, ones(N(1), 1), N(2)) ;
   c = cellfun(@(l) find(l), L, "UniformOutput", false) ;
   c(I0) = 0 ;

endfunction
